/***********company section***********/
$(".alert-success").fadeOut(3000);
var csrf_token = $('[name="_token"]').val();
var id = $('#hid').val()?$('#hid').val():'';
console.log(csrf_token);
$("#cmp_section").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

  rules: {    
    name: {
        required: true, 
        minlength:3,
        remote: baseUrl+'/superadmin/comp_check_isexist?id='+id,
        },  
    ctype: {
        required: true,
        minlength: 6,
        },
    email: {
        required: true,
        email: true,
        remote: baseUrl+'/superadmin/comp_check_isexist?id='+id,
        },
    phone:{
        required:true,
        minlength:8,
        maxlength:15,
        number: true
    },
  },
  messages: {
    name: {
            required: "Company Name field is required.",                
            remote: "Company Name is already exist.",
        },    
    ctype: {
            required : "Company Type field is required.",
            minlength : "Please enter at least 6 characters.",

        },
    email: {
            required: "Email field is required.",
            email: "Please enter a valid email address.",
            remote: "This Email already exist.",
        }
  }
});

$("#chck1").click(function(){
      if($(this). prop("checked") == true){
        $("#buld_add1").val($("#bus_add1").val());
        $("#buld_add2").val($("#bus_add2").val());
        $("#buld_suburb").val($("#bus_suburb").val());
        $("#buld_state").val($("#bus_state").val());
        $("#buld_post").val($("#bus_post").val());
        $("#buld_country").val($("#bus_country").val());
      }
      else{
        $("#buld_add1").val("");
        $("#buld_add2").val("");
        $("#buld_suburb").val("");
        $("#buld_state").val("");
        $("#buld_post").val("");
        $("#buld_country").val("");
      }
  })

$(document).ready(function() {
    $('#sample_1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": baseUrl+"/superadmin/cmp_list",
        columns: [            
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'action', name: 'action', orderable: false, searchable: false}           
        ]
    } );

    $('#assoc_user_list').DataTable( {
        "processing": true,
        "serverSide": true,
        'bFilter': false,
        'paging': true,
        "bLengthChange" : false,
        "ajax": baseUrl+"/superadmin/getCompanyUserData/"+id,
        columns: [            
            {data: 'full_name', name: 'full_name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
        ]
    } );

    $('#assoc_project_list').DataTable( {
        "processing": true,
        "serverSide": true,
        'bFilter': false,
        'paging': true,
        "bLengthChange" : false,
        "ajax": baseUrl+"/superadmin/getCompanyProjectData/"+id,
        columns: [            
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
            {data: 'action', name: 'action', orderable: false, searchable: false}          
        ]
    } );

} );

