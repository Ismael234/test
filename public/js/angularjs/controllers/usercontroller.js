var user = angular.module('userModule', []);
    user.constant('API_URL', baseUrl+'/superadmin/');

user.controller('userController', function ($scope, $http, API_URL) {

    $(".alert-success").fadeOut(3000);
    $scope.user = {};
    //show modal form
    $scope.showModelFrm = function (id,userform) {
        console.log(id,userform);
        switch (userform) {
            case 'profile':
                var url = API_URL + "edit-user/" + id + '/profile';
                //console.log(url);
                $http({method: 'GET', url: url}).
                    then(function(response) {
                      //$scope.addressInfo = {};                         
                      $scope.user = response.data;
                      //console.log($scope.user.username);
                      $('#profileModal').modal('show');
                    }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                break;
            case 'address':
                var url = API_URL + "edit-user/" + id + '/address';
                $http({method: 'GET', url: url}).
                    then(function(response) {
                      //$scope.userInfo = {};
                      $scope.user = response.data;
                      $('#addressModal').modal('show');
                    }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                break;
            case 'companies':
                $('#companiesModal').modal('show');
                // var url = API_URL + "edit-user/" + id + '/companies';
                // $http({method: 'GET', url: url}).
                //     then(function(response) {
                //       $scope.user = response.data;
                //       $('#companiesModal').modal('show');
                //     }, function(response) {
                //       $scope.data = response.data || 'Request failed';
                //       $scope.status = response.status;
                // });
                break;
            default:
                alert("Please select any form");
                break;
        }            
    };

    //save new record / update existing record
    $scope.submitForm = function (id,userform,username='') {
        
        $scope.showLoader = true;
        var url = API_URL + "update_user";
        $scope.user.id = id;
        $scope.user.userform = userform;

        switch (userform) {
            case 'profile':
            if ($scope.userInfo.$valid) {
                console.log('user form is in scope');
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param($scope.user)
                }).then(function successCallback(response) {
                    //console.log(response);
                    $scope.showLoader = false;
                    if(response.status == 200)
                    {
                        window.location.href= baseUrl+"/superadmin/user-details/"+id;
                    }
                    }, function errorCallback(response) {
                        $scope.showLoader = false;
                        alert('This is embarassing. An error has occured. Please check your internet connections');
                });
            } else {
                console.log('userform is not in scope');
            }
            break;
            case 'address':
            if ($scope.addressInfo.$valid) {
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param($scope.user)
                }).then(function successCallback(response) {
                    //console.log(response);
                    $scope.showLoader = false;
                    if(response.status == 200)
                    {
                        window.location.href= baseUrl+"/superadmin/user-details/"+id;
                    }
                    }, function errorCallback(response) {
                        $scope.showLoader = false;
                        alert('This is embarassing. An error has occured. Please check your internet connections');
                });
            } else {
                console.log('address is not in scope');
            }
            break;
            case 'companies':
                var companies = $("#company").val();
                $scope.user.companies = companies;
                console.log($scope.user);
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param($scope.user)
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    $scope.showLoader = false;
                    if(response.status == 200)
                    {
                        window.location.href= baseUrl+"/superadmin/user-details/"+id;
                    }
                 }, function errorCallback(response) {
                    $scope.showLoader = false;
                    alert('This is embarassing. An error has occured. Please check your internet connections');
                });
                break;
            case 'logo':
                var formdata = new FormData(document.getElementById("userLogoFrm"));
                formdata.append('userform' , userform);
                formdata.append('id' , id);
                formdata.append('username' , username);
                console.log(formdata);
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': undefined},
                    data: formdata
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    $scope.showLoader = false;
                    if(response.status == 200)
                    {
                        window.location.href= baseUrl+"/superadmin/user-details/"+id;
                    }
                 }, function errorCallback(response) {
                    $scope.showLoader = false;
                    alert('This is embarassing. An error has occured. Please check your internet connections');
                });
                break;
            default:
                alert("Nothing to update");
            break;
        }
    };

    $scope.check_company_email = function(id){         
                  
        var email = $scope.user.email;                    
        var url=API_URL+"user_check_isexist?id="+id+'&email='+email;
        $http({method: 'GET', url: url}).
            then(function successCallback(response) {
                console.log(response.data);                    
                if(response.data == "false"){  
                    $('#error_msg_email').show();                        
                    $('#profile_form').attr('disabled',true);

                }else{
                    $('#error_msg_email').hide();
                    $('#profile_form').attr('disabled',false);
                }                                        
            }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    function readLogoImage(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();        
            reader.onload = function (e) {                           
                $('#logo_image').attr('src', e.target.result);
                $('#upload_btn').show();
            }              
            reader.readAsDataURL(input.files[0]);
        }
    };

    $("#userImg").change(function(){
        readLogoImage(this);    
    });


    $scope.cancel = function (formid) {
        $('#'+formid).modal('hide');
    };

});




