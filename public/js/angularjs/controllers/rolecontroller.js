var role = angular.module('roleModule', []);
    role.constant('API_URL', baseUrl+'/superadmin/');

role.controller('roleController', function($scope, $http, API_URL) {
    
    //show modal form
    $scope.id='';
    $scope.toggle = function(modalstate, id, name) {        
        $scope.modalstate = modalstate;
        switch (modalstate) {
            case 'add':
                $scope.form_title = "Edit New Role";
                break;
            case 'edit':
                $scope.form_title = "Role Detail";
                $scope.id = id;                                          
                $scope.name = name;
                $('#showModal').modal('show');                
                break;
            default:
                break;
        }        
    }


    //save new record / update existing record
    $scope.save = function(id) {
        var url = API_URL + "update-role";
        var permission = $('#treeview-checkbox-demo').treeview('selectedValues');
        permission = permission.slice(1);                  
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/json'},
            data: { permission: permission,'id':id,'name':$scope.name}, //forms user object
        }).then(function(response) {                                    
           if(response.status == 200)
                {
                    window.location.href= baseUrl+"/superadmin/role?i=1";
                }
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check your internet connections');
        });

    }

    var permission = '';
    if($('#perm_hidevalue').text()){        
        permission = $('#perm_hidevalue').text();
        permission = JSON.parse(permission); 
        if(permission !='')
        {            
            $('#treeview-checkbox-demo').treeview({
                debug : true,
                data : permission,
            });       
        }else{
            $('#treeview-checkbox-demo').treeview({
                debug : true,
                data : ['selected'],
            });   
        }        
    }


    // $('#treeview-checkbox-demo').treeview({
    //     debug : true,
    //     data : permission?permission:['selected'],
    // });

    $scope.check_role = function(){  

        if(typeof $scope.name == "undefined")
        {            
            $('#error_msg').hide();
            $('.angfrm').attr('disabled',true);
        }else{
            var id = $scope.id;
            var url=baseUrl+"/superadmin/check-role?id="+id+'&name='+$scope.name;
            $http({method: 'GET', url: url}).
                then(function(response) {
                    console.log(response.data);
                    if(response.data == "false"){  
                        $('#error_msg').show();
                        $('.angfrm').attr('disabled',true);
                    }else{
                        $('#error_msg').hide();
                        $('.angfrm').attr('disabled',false);
                    }
                  //$scope.user = response.data;        
                }, function(response) {
                  $scope.data = response.data || 'Request failed';
                  $scope.status = response.status;
            });
        }            
    }

});

