var project = angular.module('projectModule', []);
    project.constant('API_URL', baseUrl+'/superadmin/');

project.controller('projectController', function ($scope, $http, API_URL) {

    $(".alert-success").fadeOut(3000);
    $scope.project = {};
    //show modal form
    $scope.show_details = function (id,projectform) {
        console.log(id,projectform);
        switch (projectform) {
            case 'details':
                var url = API_URL + "edit-project/" + id + '/details';
                //console.log(url);
                $http({method: 'GET', url: url}).
                    then(function(response) {
                      // console.log(response);
                      $scope.project = response.data;
                      $('#detailsModal').modal('show');
                    }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                break;
            case 'address':
                var url = API_URL + "edit-project/" + id + '/address';
                $http({method: 'GET', url: url}).
                    then(function(response) {
                      $scope.project = response.data;
                      $('#addressModal').modal('show');
                    }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                break;
            case 'companies':
                var url = API_URL + "edit-project/" + id + '/companies';
                $http({method: 'GET', url: url}).
                    then(function(response) {
                      $scope.project = response.data;
                      $('#companiesModal').modal('show');
                    }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                //alert('test');
                break;
            default:
                alert("Please select any form");
                break;
        }            
    };

    //save new record / update existing record
    $scope.update_details = function (id,projectform) {
        var url = API_URL + "update_project";
        $scope.project.id = id;
        $scope.project.projectform = projectform;
        console.log($scope.project);
        switch (projectform) {
            case 'details':
                if ($scope.projectInfo.$valid) {
                    console.log('project form is in scope');
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.project)
                    }).then(function(response) {
                        if(response.status == 200)
                        {
                            window.location.href= baseUrl+"/superadmin/project-details/"+id+"?i=smsg";
                        }
                    }).error(function(response) {
                        console.log(response);
                        alert('This is embarassing. An error has occured. Please check your internet connections');
                    });
                } else {
                    console.log('projectform is not in scope');
                }                
                break;
            case 'address':
                if ($scope.projectAddress.$valid) {
                    console.log('project form is in scope');
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.project)
                    }).then(function(response) {
                        if(response.status == 200)
                        {
                            window.location.href= baseUrl+"/superadmin/project-details/"+id+"?i=smsg";
                        }
                    }).error(function(response) {
                        console.log(response);
                        alert('This is embarassing. An error has occured. Please check your internet connections');
                    });
                } else {
                    console.log('projectform is not in scope');
                }
                break;
            case 'companies':
                var companies = $("#company").val();
                $scope.project.companies = companies;
                console.log($scope.project);
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param($scope.project)
                }).then(function successCallback(response) {
                    console.log(response.data);
                    if(response.status == 200)
                    {
                        window.location.href= baseUrl+"/superadmin/project-details/"+id+"?i="+response.data;
                    }
                 }, function errorCallback(response) {
                    console.log(response);
                    alert('This is embarassing. An error has occured. Please check your internet connections');
                });
                break;
            default: 
                alert("Please select any form");
                break;
        }           
    };
    
    $scope.cancel = function () {
        $modal.dismiss('cancel');
   };

});
