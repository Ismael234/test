app.controller('issuetoController', function($scope, $http, API_URL) {
    
    //show modal form
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Issueto";
                break;
            case 'edit':
                $scope.form_title = "Issueto Detail";
                $scope.id = id;
                $http.get(API_URL + 'edit-issueto-details/' + id)
                        .success(function(response) {
                            console.log(response);
                            $scope.employee = response;
                        });
                break;
            default:
                break;
        }
        console.log(id);
        $('#editUserModel').modal('show');
    }


    //save new record / update existing record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "save_issueto";
        
        //append users id to the URL if the form is in edit mode
        if (modalstate === 'edit'){
            url += "/" + id;
        }
        
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.issueto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

});