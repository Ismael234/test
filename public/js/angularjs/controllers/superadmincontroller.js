var superadmin = angular.module('adminModule', []);
    superadmin.constant('API_URL', baseUrl+'/superadmin/');

superadmin.controller('superadminController', function ($scope, $http, API_URL) {

    $scope.adminLoader = false;
    $(".alert-success").fadeOut(3000);
    $scope.admin = {};
    //show modal form
    $scope.showModal = function (id) {
        var url = API_URL + "edit-admin-profile/" + id + '/profile';
        $http({method: 'GET', url: url}).
            then(function(response) {
              console.log(response.data);                
              $scope.admin = response.data;
              $('#adminModelFrm').modal('show');
            }, function(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    //save new record / update existing record
    $scope.submitForm = function (id,adminform) {
        $scope.adminLoader = true;
        var url = API_URL + "update_admin";
        $scope.admin.id = id;
        $scope.admin.adminform = adminform;

        switch (adminform) {
            case 'profile':
                if ($scope.adminInfo.$valid) {
                    console.log('admin form is in scope');
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.admin)
                    }).then(function successCallback(response) {
                        $scope.adminLoader = false;
                        if(response.status == 200)
                        {
                            window.location.href= baseUrl+"/superadmin/profile"; //+response.data;
                        }
                        }, function errorCallback(response) {
                            console.log(response);
                            alert('This is embarassing. An error has occured. Please check your internet connections');
                    });
                } else {
                    console.log('userform is not in scope');
                }
            break;
            case 'logo':
                var formdata = new FormData(document.getElementById("adminLogoFrm"));
                formdata.append('adminform' , adminform);
                formdata.append('id' , id);
                console.log(formdata);
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': undefined},
                    data: formdata
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    $scope.adminLoader = false;
                    if(response.status == 200)
                    {
                        window.location.href= baseUrl+"/superadmin/profile";
                    }
                 }, function errorCallback(response) {
                    $scope.adminLoader = false;
                    alert('This is embarassing. An error has occured. Please check your internet connections');
                });
            break;
            default:
                alert("Nothing to update");
            break;
        }
    };

    function readLogoImage(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();        
            reader.onload = function (e) {                           
                $('#logo_image').attr('src', e.target.result);
                $('#upload_btn').show();
            }              
            reader.readAsDataURL(input.files[0]);
        }
    };

    $("#userImg").change(function(){
        readLogoImage(this);    
    });

    $scope.cancel = function (formid) {
        $('#'+formid).modal('hide');
    };

});


