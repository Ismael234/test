var company = angular.module('companyModule', []);
    company.constant('API_URL', baseUrl+'/superadmin/');

company.directive('ngFiles', ['$parse', function ($parse) {

    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });
    };

    return {
        link: fn_link
    }
} ]);

company.controller('companyController', function ($scope,$log, $http, API_URL) {

    $(".alert-success").fadeOut(3000);
    $scope.cmp = {};    

    //show modal form
    $scope.showModelFrm = function (id,userform) {
        console.log(id,userform);
        switch (userform) {
            case 'company':
            var url = API_URL + "edit-company-details/" + id + '/company';
            $http({method: 'GET', url: url}).
            then(function(response) {
               console.log(response);         
                       //alert(response.data.cmpname);
                       $scope.cmp = response.data;
                      //alert($scope.cmpname);
                      $('#companyModal').modal('show');
                  }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                  });
            break;
            case 'address':
                //$('#addressModal').modal('show');
                $scope.cmpdetails = {};
                var url = API_URL + "edit-company-details/" + id + '/address';
                $http({method: 'GET', url: url}).
                then(function(response) {                        
                  $scope.cmpdetails = response.data;                      
                  $('#addressModal').modal('show');
              }, function(response) {
                  $scope.data = response.data || 'Request failed';
                  $scope.status = response.status;
              });
                break;
                default:
                alert("Please select any form");
                break;
            }            
        };

    $scope.cancel = function (formid) {
        $('#'+formid).modal('hide');
    };


    $scope.check_company_name = function(id){         
        if(typeof $scope.cmp.cmpname == "undefined")
        {            
            $('#error_msg').hide();
            $('.angfrm').attr('disabled',true);
        }else{            
            var cmpname = $scope.cmp.cmpname;            
            var url=baseUrl+"/superadmin/comp_check_isexist?id="+id+'&name='+cmpname;
            $http({method: 'GET', url: url}).
            then(function(response) {
                console.log(response.data);                    
                if(response.data == "false"){  
                    $('#error_msg').show();                        
                    $('#company_form').attr('disabled',true);

                }else{
                    $('#error_msg').hide();
                    $('#company_form').attr('disabled',false);
                }                                        
                  //$scope.user = response.data;        
              }, function(response) {
                  $scope.data = response.data || 'Request failed';
                  $scope.status = response.status;
              });
        }            
    }

    $scope.check_company_email = function(id){         
        if(typeof $scope.cmp.email == "undefined")
        {            
            $('#error_msg').hide();
            $('.angfrm').attr('disabled',true);
        }else{            
            var email = $scope.cmp.email;                    
            var url=baseUrl+"/superadmin/comp_check_isexist?id="+id+'&email='+email;
            $http({method: 'GET', url: url}).
            then(function(response) {
                console.log(response.data);                    
                if(response.data == "false"){  
                    $('#error_msg_email').show();                        
                    $('#company_form').attr('disabled',true);

                }else{
                    $('#error_msg_email').hide();
                    $('#company_form').attr('disabled',false);
                }                                        
                  //$scope.user = response.data;        
              }, function(response) {
                  $scope.data = response.data || 'Request failed';
                  $scope.status = response.status;
              });
        }            
    }

    $scope.saveComapny = function(id){
        var cmpName = $scope.cmp;        
        var url = API_URL + "update_company";                        
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/json'},
            data: { cmpName,'company_form':'company','id':id }, //forms user object
        }).then(function(response) {                                    
           if(response.status == 200)
           {
            window.location.href= baseUrl+"/superadmin/company-details/"+id;
        }
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check your internet connections');
        });
    }

    $scope.saveCompanyDetail = function(){
        var cmpName = $scope.cmpdetails;        
        var url = API_URL + "update_company";                        
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/json'},
                data: { cmpName,'company_form':'address' }, //forms user object
            }).then(function(response) {                                    
               if(response.status == 200)
               {
                window.location.href= baseUrl+"/superadmin/company-details/"+id;
            }
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check your internet connections');
        });
    }

    var formdata = new FormData();
    $scope.getTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formdata.append(key, value);
        });
    };



    $scope.uploadLogo = function(id,logo,company_name){
        $scope.showLoader = true;       
        var url = API_URL + "update_company";       
        formdata.append('company_form' , logo);
        formdata.append('cmp_id' , id);
        formdata.append('company_name' , company_name);
        //formdata.append(key, value);        
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': undefined},
            data: formdata, //forms user object
            }).then(function successCallback(response) {
                $scope.showLoader = false;
                //$('#upload_btn').hide();
                 window.location.href= baseUrl+"/superadmin/company-details/"+id;
                // when the response is available
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
        });
    }


    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();        
            reader.onload = function (e) {                           
                $('#logo_image').attr('src', e.target.result);
                $('#upload_btn').show();
            }              
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);    
    });

});