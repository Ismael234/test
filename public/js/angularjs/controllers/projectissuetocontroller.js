var projectIssueto = angular.module('projectIssuetoModule', []);
    projectIssueto.constant('API_URL', baseUrl+'/superadmin/');

projectIssueto.controller('projectIssuetoController', function($scope, $http, API_URL) {
    //show modal form
    $scope.issueto = {};
    $scope.issuetoUser = {};
    $scope.showIssueto = function(modalstate='', id='') {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Issueto";

                $('#issuetoModal').modal('show');
                break;
            /*case 'edit':
                $('#issuetoModal').modal('show');
                $scope.form_title = "Issueto Detail";
                $scope.id = id;
                $scope.project_id;
                var url = API_URL + 'project/issueto/edit-issueto/' + project_id + '/' + id;
                $http({
                    method: 'POST',
                    url: url,
                    //data: $.param($scope.issueto),
                    //headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function successCallback(response) {
                    console.log(response);
                    $scope.issueto = response.data;
                });  
                break;*/
            default:
                break;
        }
        console.log(id);
        //$('#editUserModel').modal('show');
    }

    

    $scope.showIssuetoUser = function(modalstate, id='', issuedto_id='') {
        
        $scope.issuetoUser.modalstate = modalstate;
        console.log('test');
        switch (modalstate) {
            case 'add':
                $scope.issuetoUser.form_title = "Add New Issueto";
                $scope.issuetoUser = {};
                console.log($scope.issuetoUser);
                $('#issuetoUserModal').modal('show');
                break;
            case 'edit':
                $('#issuetoUserModal').modal('show');
                $scope.form_title = "Issueto Detail";
                $scope.id = id;
                $scope.project_id;
                var url = API_URL + 'project/issueto/edit-issueto-user/'+ id;
                $http({
                    method: 'POST',
                    url: url,
                    //data: $.param($scope.issueto),
                    //headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function successCallback(response) {
                    console.log(response);
                    $scope.issuetoUser = response.data;
                });  
                break;
            default:
                break;
        }
        //$('#editUserModel').modal('show');
    }

    

    $(document).on('click', '.edit_ius' , function(){
        var anc = angular.element("editT");
        var id = $(this).attr('IsUID');
        anc.bind('click', $scope.showIssuetoUser('edit', id));
    });


    //save new record / update existing record
    $scope.saveIssueto = function(modalstate, id=0) {

        $scope.issueto.id = id;
        $scope.issueto.modalstate = modalstate;
        var url = API_URL + "project/save_issueto";
        console.log('yes');
        //append users id to the URL if the form is in edit mode
        if (modalstate === 'edit'){
            url += "/" + id;
        }
        
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.issueto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            console.log(response);
            location.reload();
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.saveIssuetoUser = function(modalstate='', id='') {

        //$scope.issuetoUser.id = id;
        //console.log($scope.issuetoUser); return false;
        $scope.issuetoUser.modalstate = modalstate;
        var url = API_URL + "project/save_issueto_user";
        
        // //append users id to the URL if the form is in edit mode
        // if (modalstate === 'edit'){
        //     url += "/" + $scope.issuetoUser.id;
        // }
        
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.issuetoUser),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            $scope.showLoader = false;
            if(response.status == 200)
            {
                window.location.href= baseUrl+"/superadmin/project/issueto-details/"+$scope.issuetoUser.issueto_id+"?i="+response.data;
            }
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.cancel = function (formid) {
        $('#'+formid).modal('hide');
    };
    
});