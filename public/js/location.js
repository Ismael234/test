var csrf_token = $('[name="_token"]').val();
var id = $('#hid').val()?parseInt($('#hid').val()):'';
$(".alert-success").fadeOut(3000);
$(document).ready( function(){    
    $.contextMenu({
    selector: '.location', 
    build: function($trigger, e) {
        return {
            callback: function(key, options) {
                var operationID = $($trigger)[0];
                console.log(key , $(operationID).attr('id'));
                switch(key){
                    case "add":
                        addLocation($(operationID).attr('name'),$(operationID).attr('id'));
                    break;

                    case "edit":
                        //deleteHotspot(operationID);
                    break;

                    case "delete":
                        //rotatationActivate(operationID);
                    break;

                } 
            },
            items: {
                "add": {name: "Add", icon: "add"},
                "edit":{name: "Edit", icon: "edit"},
                "delete": {name: "Delete", icon: "delete"},
                }
            };
        }
    });

    function addLocation(name,id){
        if(id == undefined)
        {
            $('#addModal').modal('show');
        }else{
            $('#parent').val(name);
            $('#parent_id').val(id);            
            $('#childModal').modal('show');
        }
    }    

$("#location").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
    rules: {
      level1: {
            required: true,
            minlength: 3,
        }     
    },
    // Specify validation error messages
    messages: {
      level1: {
        required: "The location name field is required.",        
      }      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {            
        var level1 = $('#level1').val();
        var data = {'level1':level1,'_token':csrf_token,'case':'parent'}     
        $.ajax({
        url: baseUrl+'/superadmin/project/save-location',
        type: "post",
        data: data,
        success: function(response) {            
            var res = JSON.parse(response);
            console.log(res);
            $('#browser').append(res.html);            
            $('#level1').val('');
            $('#loc_msg').removeClass('hide');
            $('#loc_msg').text(res.msg);
            $(".alert-success").fadeOut(3000);                                    
        }            
        });
        return false;
        //console.log($('#treeview-checkbox-demo').treeview('selectedValues'));        
    }

    
  });

$("#sublocation").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },
    // Specify validation rules
    rules: {
      sublocation_name: {
            required: true,
            minlength: 3,
        }     
    },
    // Specify validation error messages
    messages: {
      sublocation_name: {
        required: "The sub location name field is required.",        
      }      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {       
        var level1 = $('#sublocation_name').val();
        var parent_id = $('#parent_id').val();        
        var data = {'level1':level1,'parent_id':parent_id,'_token':csrf_token,'case':'child'}     
        $.ajax({
        url: baseUrl+'/superadmin/project/save-location',
        type: "post",
        data: data,
        success: function(response) {
            var res = JSON.parse(response);
            console.log(res);
            if ($('#'+parent_id+' > ul').length) {               
               $('#'+parent_id).removeClass('last expandable lastExpandable').addClass('closed collapsable lastCollapsable');
               if (!($('#'+parent_id+' > div').length)) {
                    $('#'+parent_id).append('<div></div>');
               }
                $('#'+parent_id+' > div').removeClass('hitarea location-hitarea tree-view-hitarea closed-hitarea expandable-hitarea lastExpandable-hitarea').addClass('hitarea location-hitarea tree-view-hitarea closed-hitarea collapsable-hitarea lastCollapsable-hitarea');
                $('#'+parent_id+' > ul').append(res.html);
                $('#'+parent_id).find('ul').show();
            }else{                
                $('#'+parent_id).removeClass('last expandable lastExpandable').addClass('closed collapsable lastCollapsable');
                if (!($('#'+parent_id+' > div').length)) {
                    $('#'+parent_id).append('<div></div>');
                   }
                $('#'+parent_id+' > div').removeClass('hitarea location-hitarea tree-view-hitarea closed-hitarea expandable-hitarea lastExpandable-hitarea').addClass('hitarea location-hitarea tree-view-hitarea closed-hitarea collapsable-hitarea lastCollapsable-hitarea');
                $('#'+parent_id).append('<ul style="display:block">'+res.html+'</ul>');
                $('#'+parent_id).find('ul').show();                
            }
            $('#sublocation_name').val('');
            $('#loc_msg1').removeClass('hide');
            $('#loc_msg1').text(res.msg);
            $(".alert-success").fadeOut(3000);                        
            $('#childModal').modal('hide');
            window.location.href= baseUrl+"/superadmin/project/location";
        }            
        });
        return false;        
    }
});
    
});