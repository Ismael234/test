$(document).ready(function() {
    $('#sample_1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": baseUrl+"/superadmin/role_list",
        columns: [            
            {data: 'name', name: 'name'},                        
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    } );
} );
