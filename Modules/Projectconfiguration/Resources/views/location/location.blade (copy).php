@extends('superadmin::layouts.master')

<!-- BEGIN CONTAINER -->
	
@section('content')
	
	
	<div class="page-content-wrapper" ng-controller="LocationController">
		<div  class="page-content" style="min-height:1029px">
			
			<!-- BEGIN PAGE HEADER-->			
			@include('superadmin::partials.breadcrumb')
			<h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>-->	</h3>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN CONTENT -->
			
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Add Location</span>
								<span class="caption-helper"></span>
							</div>							
						</div>
						
						
							<!-- BEGIN FORM-->							
							
										    
							    @include('projectconfiguration::location.form')							    
							        <!-- <pre id="values"></pre> -->							   
  							
							<!-- END FORM-->
						
					</div>	
				</div>
			
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
			
		</div>
	</div>
@stop
<!-- END CONTAINER -->