<link href="{{url('/')}}/css/jquery.treeview.css" rel="stylesheet" type="text/css"/>
<div class="row">
    <div class="col-lg-8 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption col-md-9">
                    <i class="icon-bubbles font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Locations</span>
                </div>
  
                <div class="caption col-md-3">
                    <i class="icon-bubbles font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Project : {{$project[0]->name}}</span>
                </div>                                       
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="portlet_comments_1">
                        <!-- BEGIN: Comments -->
                        <div class="mt-comments">
                            <div class="mt-comment">
                                <!-- Tree -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">                                
                                            <button class="location btn green" pid="{{$project[0]->id}}" pname="{{$project[0]->name}}">AddLocation</button>
                                        </div>
                                    </div>
                                </div>    
                                <div class="parent_tree">                            
                                    {!! $tree !!}  
                                </div>                              
                                <!-- End Tree -->                                
                            </div>           
                        </div>
                        <!-- END: Comments -->
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-xs-12 col-sm-12" ng-app="notebooks" ng-controller="NotebookListCtrl">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <!-- <div class="caption">
                    <i class=" icon-social-twitter font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase"></span>
                </div>            -->                             
                <div class="caption">
                    <i class="icon font-purple-plum"></i>
                    <span class="caption-subject bold font-purple-plum">
                    Project List </span>                    
                </div>
                <div class="inputs">
                    <div class="portlet-input input-inline input-small">
                        <div class="input-icon right">                            
                            <i class="icon-magnifier"></i>                            
                            <input type="text" id="query" class="form-control input-circle" ng-model="query" onfocus="pxtrack.emit('counter', '1')" placeholder="search..."/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div id="notebooks">                       
                        <ul id="notebook_ul" class="project_list">
                            <li ng-repeat="notebook in notebooks | filter:query | orderBy: orderList">
                            @{{notebook.name}}<br/>                                                            
                            </li>
                        </ul>                        
                    </div>                                           
                </div>
            </div>

        </div>
    </div>
</div>
@include('projectconfiguration::location.popup.add')