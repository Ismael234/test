<div class="modal fade" id="addModal" close="cancel()">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="location" name="location" class="horizontal-form" novalidate="">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Add Location</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}					
					<div class="form-body">
							<!--/row for buisness-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Level Name</label>
									<input type="text" id="level1" required="required" name="level1" class="form-control" ng-model="cmpdetails.bus_add1" placeholder="">
								</div>
							</div>							
						</div>
					<!--/row-->
					</div>
				<div class="form-actions right">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</div>
		</form>
	</div>
</div>
</div>