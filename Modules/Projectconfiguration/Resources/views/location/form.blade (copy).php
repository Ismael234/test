<div class="form-body">	
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-md-3"><strong>Selected Project:</strong></label>
				<div class="prj">
				@if(!empty($project[0]) && isset($project[0]->name))
					{{$project[0]->name}}
				@endif
				</div>
			</div>
		</div>
	</div>
	<!--/row-->	
	@if(!empty($project[0]) && isset($project[0]->name))
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">								
				<button class="location btn green" data-pid="">Location</button>
			</div>
		</div>
	</div>
	<div id="treeView">
	@if(!empty($location))	
		@foreach($location as $key=>$loc)				
          	<ul id="tree_0">
          		<li class="location" id="{{$loc[$key]['id']}}" data-pid="" title="test">
          			<a href="#" class="" id="list_{{$loc[$key]['id']}}">{{$loc[$key]['name']}}</a>
          			<div id="treeBox_{{$loc[$key]['id']}}"></div>
          		</li>
          	</ul>          	
		@endforeach	
	@endif
	</div>
	@endif
	<!--/row-->												
	@include('projectconfiguration::location.popup.add')
</div>
