@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper" ng-app="projectIssuetoModule">
		<div class="page-content" ng-controller="projectIssuetoController">
		
			<!-- BEGIN PAGE HEADER-->
			@include('superadmin::partials.breadcrumb')
			<!-- {!! $breadcrumbs !!} -->
			<h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>--> </h3>
			<!-- END PAGE HEADER-->
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Issuedto Details
							</div>
						</div>
						<div class="portlet-body form">
							@if(isset($_REQUEST['i']) && !empty($_REQUEST['i']) && empty($j))							 	
		                        <div class="alert alert-success alert-dismissable">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           		@php echo $_REQUEST['i']; @endphp;
		                        </div>
                       		@endif
							
							<div class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Issuedto Info</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"><strong>Name :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$issueto->name}}
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"><strong>Company :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$companies[$issueto->company_id]}}
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"><strong>Email :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$issueto->email}}
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"><strong>Phone :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$issueto->phone}}
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"><strong>Activity :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$issueto->activity}}
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="edit_btn">								
								<div class="text-right">
									<div class="row">
										<div class="col-md-12">
											<a href="javascript:;" id="" ng-click="showIssuetoUser('add')" class="btn green"><i class="fa fa-plus"></i> Add New User</a>
										</div>
									</div>
								</div>								
							</div>
							@include('projectconfiguration::issueto.forms.issueto_user') <!-- Add User to Issueto Form -->
							<div class="form-horizontal">
								<div class="form-body">
									<h3 class="form-section">Issuedto Users</h3>
									<!-- <dtable options="options" rows="data" class="material"></dtable> -->
									<table class="table table-striped table-bordered table-hover" id="sample_2">
										<thead>
											<tr>
												<th> Name </th>
												<th> Email </th>
												<th> Phone </th>
												<th> Activity </th>
												<th> Action </th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>	
				</div>															
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
<!-- END CONTAINER -->
@stop