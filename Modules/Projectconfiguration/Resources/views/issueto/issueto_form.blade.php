<div class="modal fade" id="issuetoModal" close="cancel()">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="issuetoInfo" name="issuetoInfo" class="horizontal-form" ng-submit="saveIssueto('add')">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Add Issuedto</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" id="issueto_id" name="issueto_id" value="" />
					<input type="hidden" id="project_id" name="project_id" ng-model="issueto.project_id" ng-init="issueto.project_id={{$project_id}}" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name</label>
									<input type="text" id="name" name="name" class="form-control" placeholder="" ng-model="issueto.name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Email</label>
									<input type="text" id="email" name="email" class="form-control" placeholder="" ng-model="issueto.email">
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Company</label>
									<input type="text" id="company" name="company" class="form-control" placeholder="" ng-model="issueto.company">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Phone</label>
									<input type="text" id="phone" name="phone" class="form-control" placeholder="" ng-model="issueto.phone" >
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Activity</label>
								<input type="text" id="activity" name="activity" class="form-control" placeholder="" ng-model="issueto.activity" >
								</div>
							</div>
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel('issuetoInfo')">Cancel</button>
					<button type="submit" class="btn btn-primary" id="details_form">Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
