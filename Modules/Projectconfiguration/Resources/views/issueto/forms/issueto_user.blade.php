<div class="modal fade" id="issuetoUserModal" close="cancel('issuetoUserFrm')">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="issuetoUserFrm" name="issuetoUserFrm" class="horizontal-form" method="post" ng-submit="saveIssuetoUser()" novalidate>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Add New User</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" id="issueto_id" name="issueto_id" ng-model="issuetoUser.issueto_id" ng-init="issuetoUser.issueto_id={{$issueto_id}}" value="{{$issueto_id}}"/>
					<input type="hidden" id="issueto_user_id" name="issueto_user_id" ng-model="issuetoUser.id" ng-init="issuetoUser.id=issuetoUser.id" value="issuetoUser.id"/>
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name</label>
									<input type="text" id="name" name="name" class="form-control" placeholder="" ng-model="issuetoUser.name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Email</label>
									<input type="text" id="email" name="email" class="form-control" placeholder="" ng-model="issuetoUser.email">
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Phone</label>
									<input type="text" id="phone" name="phone" class="form-control" placeholder="" ng-model="issuetoUser.phone" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Activity</label>
								<input type="text" id="activity" name="activity" class="form-control" placeholder="" ng-model="issuetoUser.activity" >
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel('issuetoUserFrm')">Cancel</button>
					<button type="submit" class="btn btn-primary" id="details_form">Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
