<div class="modal fade" id="issuetoModal" close="cancel()">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form action="{{ url('superadmin/save_issueto_user') }}" id="issueto_user_form" class="horizontal-form" method="post" style="display:none;">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-equalizer font-blue-hoki"></i>
							<span class="caption-subject font-blue-hoki bold uppercase">Add Issueto User</span>
							<span class="caption-helper"></span>
						</div>
					</div>
					<input type="hidden" id="issueto_id" name="issueto_id" value="" />
					<input type="hidden" id="project_id" name="project_id" ng-model="issueto.project_id" ng-init="issueto.project_id={{$project_id}}" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name</label>
									<input type="text" id="name" name="name" class="form-control" placeholder="" ng-model="issueto.name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Email</label>
									<input type="text" id="email" name="email" class="form-control" placeholder="" ng-model="issueto.email">
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Company</label>
									<input type="text" id="company" name="company" class="form-control" placeholder="" ng-model="issueto.company">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Phone</label>
									<input type="text" id="phone" name="phone" class="form-control" placeholder="" ng-model="issueto.phone" >
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Activity</label>
								<input type="text" id="activity" name="activity" class="form-control" placeholder="" ng-model="issueto.activity" >
								</div>
							</div>
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel('issuetoInfo')">Cancel</button>
					<button type="submit" class="btn btn-primary" id="details_form">Update</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
