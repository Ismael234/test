<?php
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['web','auth','ProjectConfig'], 'prefix' => 'superadmin/projectConfiguration', 'namespace' => 'Modules\Projectconfiguration\Http\Controllers'], function()
{       
    //Project Configuration Issueto    
    Route::get('issueto', 'ProjectIssuetoController@index');
    Route::post('save_issueto', 'ProjectIssuetoController@updateIssueto');
    Route::get('issueto_list/{project_id}', 'ProjectIssuetoController@getIssuetoData');
    Route::get('issueto_users_list/{issueto_id}', 'ProjectIssuetoController@getIssuetoUserData');
    Route::get('issueto-details/{issueto_id}', 'ProjectIssuetoController@show');    
    Route::post('issueto/edit-issueto-user/{user_id}', 'ProjectIssuetoController@editIssuetoUser');
    Route::post('save_issueto_user', 'ProjectIssuetoController@updateIssuetoUser');    

    //Location
    Route::get('location', 'LocationController@index');
    Route::post('save-location', 'LocationController@store');
    Route::get('getAllProjectData', 'LocationController@getAllProjectData');
    Route::get('projectConfigId/{id}', function(){
    	
    });
});


