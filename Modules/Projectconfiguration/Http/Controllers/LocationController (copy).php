<?php

namespace Modules\Projectconfiguration\Http\Controllers;

use App\Location;
use App\Project;
use App\CompanyProjectMapping;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use Auth;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $id = \Session::get('projectConfig');        
        $data['scripts'] = array('jquery-ui','jquery.ui-contextmenu','location');
        $project = Project::select('id','name')->where('is_deleted','=',0)->where('id', $id)->get();                        
        $company = array(""=>"please select company");
        $loc_data = array();
        if(!empty($id)){
            //$companyProjectMapping = CompanyProjectMapping::where('is_deleted','=',0)->where('project_id','=',$id)->get();
        $project_comps = Helper::getCompanyProjectMapping($id);
        $project_company1 = Company::select([ 'id','name', 'type'])->where('is_deleted', '=', 0)->whereIn('id', $project_comps)->get();            
        foreach ($project_company1 as $key => $value) {
            $company[$value->id] = $value->name;                
        }             
        $location = Location::where('is_deleted','=',0)->where('project_id','=',$id)->get();
               
        if(!empty($location))
        {
            $loc_data = array();
            $parent_data = array();
            $i=0;
            $j=0;
            foreach ($location as $key => $loc) {
                $loc_data[$loc->parent_id][$i]['id'] = $loc->id;
                $loc_data[$loc->parent_id][$i]['name'] = $loc->label;
                $loc_data[$loc->parent_id][$i]['parent_id'] = $loc->parent_id;
                if($loc->parent_id == 0){
                    $parent_data[$j]['id'] = $loc->id;
                    $parent_data[$j]['name'] = $loc->label;
                    $parent_data[$j]['parent_id'] = $loc->parent_id;
                    $j++;
                }
                $i++;
            }    

            foreach ($parent_data as $key => $value) {
                $tree[] = $this->createTree($loc_data, array($value));
            }            
        }        
    }            
    echo "<pre>";
    // print_r($loc_data);
    // print_r($parent_data);
    print_r($tree);
    die;
    Breadcrumbs::addBreadcrumb('Project Location',  url('superadmin/project/location'));        
    $page_data = array('breadcrumbs' => Breadcrumbs::generate());        
    return view('projectconfiguration::location.location',compact('data'))->with($page_data)->with('project',$project)->with('company',$company)->with('location',$loc_data);
    }


    public function createTree(&$list, $parent){
        $tree = array();
       // echo "<pre>";
        foreach ($parent as $k=>$l){
            //print_r($l);die;
            if(isset($list[$l['id']])){
                $l['children'] = $this->createTree($list, $list[$l['id']]);
            }
            $tree[] = $l;
        }
        return $tree;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        
        $input = Input::except('_token');
        $id = \Session::get('projectConfig'); 
        $cmp_id = \Session::get('cmp_id');
        $insertInLocation = array(
            "label"  =>  $input['level1'],
            "company_id"  =>  $cmp_id,
            "project_id" =>  $id,  
            "parent_id" => 0,
            "order_id" => 0,
            "is_deleted" => 0,
            "created_at" => date('Y-m-d h:i:s'),
            "created_by" => Auth::user()->id,
            "modified_by" => Auth::user()->id,
            "resource_type" => 'Webserver',            

        );        
        $save = Location::create($insertInLocation);
        if($save){
            echo "Location saved successfully";
        }else{
            echo "Location save failed";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        //
    }
}
