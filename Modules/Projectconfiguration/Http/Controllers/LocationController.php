<?php

namespace Modules\Projectconfiguration\Http\Controllers;

use App\Location;
use App\Project;
use App\CompanyProjectMapping;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use Auth;
use Yajra\Datatables\Facades\Datatables as Datatables;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $id = \Session::get('projectConfig');             
        $data['scripts'] = array('jquery.contextMenu','jquery.ui.position','location','jquery-treeview','tree','angularjs/angular.min','angularjs/controllers/projectlistcontroller');
        $project = Project::select('id','name')->where('is_deleted','=',0)->where('id', $id)->get();                    
        $allProject = $this->getAllProjectData();        
        $loc_data = array();
        if (!empty($id)) {
            $Categorys = Location::where('is_deleted','=',0)->where('project_id','=',$id)->where('parent_id','=',0)->get();
            if (!empty($Categorys)) {             
                $tree='<ul id="browser" class="filetree"><li class="location tree-view"></li>';
                foreach ($Categorys as $Category) {                   
                    $tree .='<li name="'.$Category->label.'"  id='.$Category->id.' class="location tree-view closed"><a class="tree-name">'.$Category->label.'</a>';
                    if (count($Category->childs)) {
                    $tree .=$this->childView($Category);
                    } else {
                        $tree .="</li>";
                    }
                }
                $tree .='<ul>';
            }               
        }
        Breadcrumbs::addBreadcrumb('Project Location',  url('superadmin/project/location'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());        
        return view('projectconfiguration::location.location',compact('data','tree','allProject'))->with($page_data)->with('project',$project)->with('location',$tree);
    }

    /**
     * Fetch Child location of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function childView($Category){
        $html ='<ul>';
        foreach ($Category->childs as $arr) {
            if (count($arr->childs)) {
                $html .='<li  name="'.$arr->label.'" id='.$arr->id.' class="location tree-view closed"><a class="tree-name">'.$arr->label.'</a>';                  
                $html.= $this->childView($arr);
            } else {
                $html .='<li name="'.$arr->label.'"  id='.$arr->id.' class="location tree-view"><a class="tree-name">'.$arr->label.'</a>';                                 
                $html .="</li>";
            }                                   
        }
        $html .="</ul>";
        return $html;
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::except('_token');        
        $pid = \Session::get('projectConfig'); 
        $cmp_id = \Session::get('cmp_id');        
        if ($input['case'] == "parent") {
            $insertInLocation = array(
                "label"  =>  $input['level1'],
                "company_id"  =>  $cmp_id,
                "project_id" =>  $pid,  
                "parent_id" => 0,
                "order_id" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d h:i:s'),
                "created_by" => Auth::user()->id,
                "modified_by" => Auth::user()->id,
                "resource_type" => 'Webserver',            

            );        
            $save = Location::create($insertInLocation);
            if ($save) {       
                $id = $save->id;        
                $html = '<li name="'.$input['level1'].'" id="'.$id.'" class="location tree-view closed"><a class="tree-name">'.$input['level1'].'</a></li>';
                $msg = "Location saved successfully";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);
            } else {
                $html = "";
                $msg = "Location save failed";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);
            }
        } else if($input['case'] == "child") {            
            $insertInLocation = array(
                "label"  =>  $input['level1'],
                "company_id"  =>  $cmp_id,
                "project_id" =>  $pid,  
                "parent_id" => $input['parent_id'],
                "order_id" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d h:i:s'),
                "created_by" => Auth::user()->id,
                "modified_by" => Auth::user()->id,
                "resource_type" => 'Webserver',            
            );        
            $save = Location::create($insertInLocation);
            if ($save) {
                $id = $save->id;        
                $html = '<li name="'.$input['level1'].'" id="'.$id.'" class="location tree-view closed"><a class="tree-name">'.$input['level1'].'</a></li>';
                $msg = "Location saved successfully";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);                
            } else {
                $html = "";
                $msg = "Location save failed";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        //
    }

    /**
    * List view by Data table for Assciated project Details
    */
    public function getAllProjectData()
    {         
        $cmp_id = \Session::get('cmp_id');
        $comp_projects = Helper::getCompanyProject($cmp_id);        
        $project1 = Project::select([ 'id','name'])->where('is_deleted', '=', 0)->whereIn('id', $comp_projects)->get();
        return $project1;        
        exit;        
    }
}
