<?php

namespace Modules\Projectconfiguration\Http\Controllers;

use App\Project;
use App\ProjectIssueto;
use App\ProjectIssuetoUsers;
use App\Company;
use App\CompanyIssuetoMapping;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreIssuetoPost;
use Modules\Superadmin\Http\Requests\StoreIssuetoUsersPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;


class ProjectIssuetoController extends Controller
{
    /**
     * Display dynamic Issueto list associated wit a Project
     */
    public function index()
    {
        
        $page_title     = 'Project Issueto'; 
        $page_action    = 'Project Issueto Details';
        $viewPage       = 'project-issueto';
        $viewPage1       = '';

        Breadcrumbs::addBreadcrumb('Project',  url('superadmin/project'));
        Breadcrumbs::addBreadcrumb('Issueto List',  '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $page_data['obj_companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['issueto_comps'] = array();
        $page_data['cur_it'] = '';
        $page_data['project_id'] = '1';
        $project = Project::select('id','name')->where('is_deleted','=',0)->get();    
        $pro = array(""=>"Please select one");
        foreach ($project as $key => $value) {
           $pro[$value->id] = $value->name;
        }           
        $page_data['project'] = $pro;

        //$issueto = ProjectIssueto::where('is_deleted', '=', 0)->where('project_id', '=', $project_id)->paginate(10);
        $data['scripts'] = array('angularjs/angular.min','angularjs/controllers/projectissuetocontroller', 'project-issueto');
        return view('projectconfiguration::issueto.issueto_list',compact('data','page_title','page_action','viewPage','viewPage1'))->with($page_data); 
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display specified issueto of a project.
     *
     */
    public function show(ProjectIssueto $issueto, $id)
    {
        Breadcrumbs::addBreadcrumb('Project',  url('superadmin/project'));
        Breadcrumbs::addBreadcrumb('Issueto List',  url('superadmin/project/issueto'));
        Breadcrumbs::addBreadcrumb('Issueto Details', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        //Get specific user details according to id
        $page_title     = 'Issueto'; 
        $page_action    = '';
        $viewPage       = 'issueto';
        $viewPage1      = '';
        $page_data['project_id'] = '1';
        $page_data['issueto_comps'] = array();
        $page_data['obj_companies'] =  Company::select('id', 'name')->where('is_deleted', '=', 0)->orderBy('name', 'asc')->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['issueto'] = $issueto->where('is_deleted','=',0)->find($id);
        $page_data['issueto_id'] = $id;
        $page_data['option_activity'] = array(
                        "Brickwork"=>"Brickwork",
                        "Carpentry"=>"Carpentry",
                        "Cleaning"=>"Cleaning",
                        "Concreting"=>"Concreting",
                        "Electrical"=>"Electrical",
                        "Fire services"=>"Fire services",
                        "Joinery"=>"Joinery",
                        "Plasterboard"=>"Plasterboard",
                        "Painting"=>"Painting",
                        "Plumbing"=>"Plumbing",
                        "Metalwork"=>"Metalwork",
                        "Structural steel"=>"structural steel",
                        "Waterproofing"=>"Waterproofing",
                        "Other"=>"Other",
                    );
        if(!empty($page_data['issueto'])){
            //$page_data['issueto_users'] = IssuetoUsers::where('issueto_id', '=', $id)->first();
            $data['scripts'] = array('angularjs/angular.min','angularjs/dataTable.min','angularjs/controllers/projectissuetocontroller', 'project-issueto');
            return view('projectconfiguration::issueto.show_issueto',compact('data','page_title','page_action','viewPage','viewPage1'))->with($page_data);
           
        }else{
            return redirect('superadmin/project');
        }
    }

    /**
     * Response the details to edit specified Issueto.
     *
     */
    

    public function editIssueto($id)
    {
        $issueto = ProjectIssueto::find($id);
        if(!empty($issueto)){
            $data = array();
            $data['name'] = $issueto->name;
            $data['email'] = $issueto->email; 
            $data['phone'] = (int)$issueto->phone; 
            $data['activity'] = $issueto->activity;
            // print_r($userdata); die;
            return json_encode($data);
        }
    }

    public function editIssuetoUser($id)
    {
        $issuetoUser = ProjectIssuetoUsers::find($id);
        if(!empty($issuetoUser)){
            $data = array();
            $data['id'] = $issuetoUser->id;
            $data['name'] = $issuetoUser->name;
            $data['issueto_id'] = $issuetoUser->issueto_id;
            $data['email'] = $issuetoUser->email; 
            $data['phone'] = (int)$issuetoUser->phone; 
            $data['activity'] = $issuetoUser->activity;
            // print_r($userdata); die;
            return json_encode($data);
        }
    }

    /**
     * Add new Issueto or update specified Issueto details.
     *
     */
    public function updateIssueto()
    {   
        $id = Input::get('issueto_id');
        $project_id = Input::get('project_id');
        //$modalstate = Input::get('modalstate');
        
        //switch ($modalstate) {
        //    case 'add':
                $issueto = ProjectIssueto::firstOrNew(array('id' => $id));
               // $issueto->id = '1';
                $issueto->project_id = $project_id;
                $issueto->name = Input::get('name');
                $issueto->company_id = Input::get('company');
                $issueto->email = Input::get('email'); 
                $issueto->phone = Input::get('phone');
                $issueto->activity = Input::get('activity'); 
                $issueto->created_at = date('Y-m-d h:i:s');
                $issueto->created_by = Auth::user()->id;
                $success = $issueto->save();
                return "Issueto created successfully";
        //        break;
        //    default:
        //        return redirect('superadmin/project')->with('flash_alert_notice', 'There was nothing to update.');    
        //        break;
        //}
    }

    /**
     * Add new Issueto or update specified Issueto details.
     *
     */
    public function updateIssuetoUser()
    {   
        $id = (Input::get('id'))? Input::get('id') : '0';
        $issueto_id = Input::get('issueto_id');
        //$modalstate = Input::get('modalstate');
        
        //switch ($modalstate) {
          //  case 'add':
                $issuetoUser = ProjectIssuetoUsers::firstOrNew(array('id' => $id));
               // $issueto->id = '1';
                $issuetoUser->issueto_id = $issueto_id;
                $issuetoUser->name = Input::get('name');
                $issuetoUser->email = Input::get('email'); 
                $issuetoUser->phone = Input::get('phone');
                $issuetoUser->activity = Input::get('activity'); 
                $issuetoUser->trade = '1234';
                $issuetoUser->created_at = date('Y-m-d h:i:s');
                $issuetoUser->created_by = Auth::user()->id;
                $success = $issuetoUser->save();
                return "Issueto User created successfully";
                //break;
          //  default:
            //    return redirect('superadmin/project')->with('flash_alert_notice', 'There was nothing to update.');    
            //    break;
        //}
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get Issueto list for specified project.
     *
     */
    public function getIssuetoData($project_id){
        $issueto1 = ProjectIssueto::select(['id', 'name', 'company_id', 'email', 'phone', 'activity'])->where('is_deleted', '=', 0)->where('project_id','=', $project_id)->get(); 
        $issueto = Datatables::of($issueto1)->addColumn('company', function ($issueto1) {   
                $company = Company::select(['name'])->where('id','=',$issueto1->company_id)->get();
                return !empty($company[0]->name)?$company[0]->name:'';
            })->addColumn('action', function ($ist) {
                return '<a href="'.url('superadmin/project').'/issueto-details/'.$ist->id.'" class="btn btn-xs default"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;<a href="delete-issueto/'.$ist->id.'" title="Delete" class="btn btn-xs default" onclick="return confirm('."'Are you sure you want to delete this Issueto?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $issueto;
        exit;        
    }

    public function getIssuetoUserData($issueto_id){
        
        $issuetoUser1 = ProjectIssuetoUsers::select(['id', 'name', 'email', 'phone', 'activity'])->where('is_deleted', '=', 0)->where('issueto_id','=', $issueto_id)->get(); 
        $issuetoUsers = Datatables::of($issuetoUser1)->addColumn('action', function ($itu) {
                $datatable_function = "showIssuetoUser('add')";
                 return '<a id="editT" class="btn btn-xs default edit_ius" IsUID="'.$itu->id.'"><i class="fa fa-pencil" title="Edit"></i></a>&nbsp;<a href="delete-issueto-user/'.$itu->id.'" title="Delete" class="btn btn-xs default" onclick="return confirm('."'Are you sure you want to delete this User?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $issuetoUsers;
        exit;        
    }
}
