<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'superadmin', 'namespace' => 'Modules\Superadmin\Http\Controllers'], function()
{
	// Home
	Breadcrumbs::register('home', function($breadcrumbs)
	{
	    $breadcrumbs->push('Home', route('home'));
	});

	// Home > About
	Breadcrumbs::register('add-company', function($breadcrumbs)
	{
	    $breadcrumbs->parent('company');
	    $breadcrumbs->push('add-company', route('/add-company'));
	});
});