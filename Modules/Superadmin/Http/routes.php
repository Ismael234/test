<?php
use Illuminate\Support\Facades\Route;
Route::group(['middleware' => ['web','auth','SuperadminCheck'], 'prefix' => 'superadmin', 'namespace' => 'Modules\Superadmin\Http\Controllers'], function()
{
    #Auth::routes();
    Route::get('/', 'SuperadminController@index');
    Route::get('profile', 'SuperadminController@profile');
    Route::get('edit-admin-profile/{id}/{adminform}', 'SuperadminController@edit');
    Route::post('update_admin', 'SuperadminController@update');
    
    /************ User ************/
    Route::resource('user', 'UserController');
    Route::get('add-user', 'UserController@create');
    Route::post('save_user', 'UserController@store');
    Route::get('edit-user/{id}/{userform}', 'UserController@edit');
    Route::post('update_user', 'UserController@update');
    Route::get('user-details/{id}', 'UserController@show');
    Route::get('delete-user/{id}', 'UserController@destroy');
    Route::get('user_check_isexist', 'UserController@check_isexist');
    Route::get('usr_list', 'UserController@getData');
    Route::get('getUserCompanyData/{id}', 'UserController@getUserCompanyData');


    /************ Company ************/
    Route::resource('company', 'CompanyController');
    Route::get('add-company', 'CompanyController@create');
    Route::post('save_company', 'CompanyController@store');
    Route::get('edit-company-details/{id}/{userform}', 'CompanyController@edit');
    Route::post('update_company', 'CompanyController@update');
    Route::get('company-details/{id}/', 'CompanyController@show');
    Route::get('comp_check_isexist', 'CompanyController@check_isexist');    
    Route::get('delete-company/{id}', 'CompanyController@destroy');
    Route::get('cmp_list', 'CompanyController@getData');
    Route::get('getCompanyUserData/{id}', 'CompanyController@getCompanyUserData');
    Route::get('getCompanyProjectData/{id}', 'CompanyController@getCompanyProjectData');
    
    /************ Authentication ************/
    Route::resource('permission', 'AuthenticationController');
    Route::get('add-permission', 'AuthenticationController@create');

    Route::post('save-permission', 'AuthenticationController@store');
    Route::get('check-permission', 'AuthenticationController@check_isexist');
    Route::get('perm_list', 'AuthenticationController@getData');
    Route::get('edit-permission/{id}', 'AuthenticationController@edit');
    Route::post('update-permission', 'AuthenticationController@update');

    /***************Role****************/
    Route::resource('role', 'RoleController');
    Route::get('add-role', 'RoleController@create');
    Route::post('save-role', 'RoleController@store');    
    //Route::get('perm_list', 'RoleController@getData');
    Route::get('show-role/{id}', 'RoleController@show');
    Route::post('update-role', 'RoleController@update');
    Route::get('check-role', 'RoleController@checkRole');
    Route::get('role_list', 'RoleController@getData');
    Route::get('delete-role/{id}', 'RoleController@destroy');

    /************ Issueto ************/
    Route::resource('issueto', 'IssuetoController');
    Route::post('save_issueto', 'IssuetoController@store_issueto');
    Route::post('update_issueto', 'IssuetoController@update');
    Route::get('issueto-details/{id}', 'IssuetoController@show');
    Route::get('issueto_check_isexist', 'IssuetoController@issueto_check_isexist'); 
    Route::get('delete-issueto/{id}', 'IssuetoController@destroy');
    Route::get('issueto_list', 'IssuetoController@getIssuetoData');
    Route::get('issueto_user_list/{issueto_id}', 'IssuetoController@getIssuetoUsersData');
    Route::post('save_issueto_user', 'IssuetoController@store_issueto_user');
    Route::post('update_issueto_user', 'IssuetoController@update_issueto_user');
    Route::get('delete-issueto-user/{id}', 'IssuetoController@destroy_issueto_user');
    Route::get('issueto_user_check_isexist', 'IssuetoController@issueto_user_check_isexist'); 
    Route::post('issueto_user_details', 'IssuetoController@get_issueto_user_details');

    /************ Quality Checklist ************/
    Route::resource('quality-checklist', 'QualityChecklistController');
    Route::post('save_quality_checklist', 'QualityChecklistController@store_checklist');
    Route::get('get_quality_checklist', 'QualityChecklistController@getChecklistData');
    Route::get('quality-checklist-details/{id}', 'QualityChecklistController@show');
    Route::post('update_quality_checklist', 'QualityChecklistController@update');
    Route::post('save_checklist_task', 'QualityChecklistController@store_checklist_task');
    Route::get('get_quality_checklist_tasks/{checklist_id}', 'QualityChecklistController@getChecklistTaskData');
    Route::post('checklist_task_details', 'QualityChecklistController@get_checklist_task_details');
    Route::post('update_checklist_task', 'QualityChecklistController@update_checklist_task');
    Route::get('delete-quality-checklist/{id}', 'QualityChecklistController@destroy');
    Route::get('delete-quality-checklist-task/{id}', 'QualityChecklistController@destroy_checklist_task');
    Route::get('quality_checklist_isexist', 'QualityChecklistController@checklist_isexist');  
    Route::get('quality_checklist_task_isexist', 'QualityChecklistController@checklist_task_isexist');

    /************ Project ************/
    Route::get('project', 'ProjectController@index');
    Route::get('add-project', 'ProjectController@create');
    Route::post('save_project', 'ProjectController@store');
    Route::get('edit-project/{id}/{projectform}', 'ProjectController@edit');
    Route::get('project_list', 'ProjectController@getData');
    Route::get('project-details/{id}', 'ProjectController@show');
    Route::get('delete-project/{id}', 'ProjectController@destroy');
    Route::post('update_project', 'ProjectController@update');
    Route::get('getProjectCompanyData/{id}', 'ProjectController@getProjectCompanyData');    
    /* -----------------------
    Route::get('edit-user/{id}/{userform}', 'ProjectController@edit');
    Route::get('user_check_isexist', 'ProjectController@check_isexist');
    */

});