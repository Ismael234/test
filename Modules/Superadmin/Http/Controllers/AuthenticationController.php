<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Authentication;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Modules\Superadmin\Http\Requests\AuthenticationPost;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use App\Http\Requests;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;


class AuthenticationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        Breadcrumbs::addBreadcrumb('Permission List',  url('superadmin/permission'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('permission');
        return view('superadmin::permission.permission_list',compact('data'))->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Breadcrumbs::addBreadcrumb('Permission List',  url('superadmin/permission'));        
        Breadcrumbs::addBreadcrumb('Add Permission', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('permission');        
        $module_name = Authentication::select(['id','name'])->where('is_deleted',"=",0)->where('module_id',"=",0)->get();
        $module=array();        
        foreach ($module_name as $key => $value) {
            $module[$value->id] = $value->name;
        }     
        $data['module_list'] = $module;        
        return view('superadmin::permission.add_permission',compact('data'))->with($page_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthenticationPost $request)
    {
        $module_name = 0;
        if (Input::get("type") != 'Menu') {
            $module_name = Input::get("module_name");
        }
        $insertInPermission = array(
            "name"  =>  Input::get("name"),
            "type"  =>  Input::get("type"),
            "description" =>  Input::get("description"),            
            "module_id"  =>  $module_name,
            "created_by"=> Auth::user()->id,
            "created_at"=> date('Y-m-d'),
            "modified_by"=> Auth::user()->id,          
        );        
        $save = Authentication::create($insertInPermission);
        return redirect('superadmin/permission')->with('flash_alert_notice', 'Permission created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function show(Authentication $authentication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function edit(Authentication $authentication,$id=0)
    {        
        if(!empty($id))
        {
            $data['scripts'] = array('permission');
            $permission = $authentication->where('is_deleted','=',0)->find($id);                        
            Breadcrumbs::addBreadcrumb('Permission List', url('superadmin/permission'));
            Breadcrumbs::addBreadcrumb('Edit Permission', '');
            $page_data['breadcrumbs'] = Breadcrumbs::generate();

            $module_name = Authentication::select(['id','name'])->where('is_deleted',"=",0)->where('id',"!=",$id)->where('module_id',"=",0)->get();
            $module=array();
            $module[]="Select Module";
            foreach ($module_name as $key => $value) {
                $module[$value->id] = $value->name;
            }     
            $data['module_list']=$module;

            return view('superadmin::permission.edit_permission',compact('data'))->with('permission', $permission)->with($page_data);
        } else {
            return redirect('superadmin/permission');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function update(AuthenticationPost $request, Authentication $authentication)
    {
        $module_name = 0;
        if (Input::get("type") != 'Menu') {
            $module_name = Input::get("module_name");
        }
        $authentication = Authentication::firstOrNew(array('id' => Input::get('hid')));
        $authentication->name = Input::get('name');
        $authentication->type = Input::get('type');
        $authentication->description = Input::get('description');
        $authentication->module_id = $module_name;
        $authentication->modified_by = Auth::user()->id;
        $authentication->modified_at = date('Y-m-d h:i:s');
        $authentication->save();
        return redirect('superadmin/permission')->with('flash_alert_notice', 'Permission Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Authentication $authentication,$id)
    {
        $authentication = Authentication::where('id','=',$id)->first();
        $authentication->is_deleted = 1;
        $authentication->save();
        return redirect('superadmin/permission')->with('flash_alert_notice', 'Permission deleted successfully!');
    }

    /**
    * Check the specified permission name is exist
    * And return as true and false
    */
    public function check_isexist()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {    
            $name = Input::get('name');           
            $id = Input::get('id');
            if (isset($name) && !empty($name)) {
                if (!empty($id)) {
                    $is_current_name = Authentication::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   

                    if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $name) {
                        echo "true";
                        die;
                    }
                }            
                $is_name = Authentication::where('name',"=",$name)->where('is_deleted',"=",0)->get();                  
                if(isset($is_name[0]) && !empty($is_name[0])) {                    
                    echo 'false';
                } else {
                    echo 'true';
                }
            }            
            die;
        } else {
            die('Access Denied.');
        }
    }

    /**
    * List view by Data table for Permission Details
    */
    public function getData(){
        $perm1 = Authentication::select(['id', 'name', 'type', 'description', 'module_id'])->where('is_deleted', '=', 0)->get();        
        $perm = Datatables::of($perm1)->addColumn('module_name', function ($perm) {   
                    $module_name = Authentication::select(['name'])->where('id','=',$perm->module_id)->get();
                    if(isset($module_name[0]) && !empty($module_name[0])) {
                        return  $module_name[0]->name;
                    } else{
                        return "-";
                    }                    
                })->addColumn('action', function ($cmp) {
                return '<a href="edit-permission/'.$cmp->id.'" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="delete-permission/'.$cmp->id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure you want to delete this user?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $perm;die;        
    }
}
