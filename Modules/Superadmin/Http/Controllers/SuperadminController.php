<?php

namespace Modules\Superadmin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use App\User;
use App\Company;
use App\Issueto;
use App\Project;
use App\UserDetails;
use File;
use Image;


class SuperadminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {        
        $companies = Company::where('is_deleted', '=', 0)->count();
        $users = User::where('is_deleted', '=', 0)->where('role_id', '!=', 0)->count();
        $users = User::where('is_deleted', '=', 0)->where('role_id', '!=', 0)->count();       
        $total_issueto = Issueto::where('is_deleted', '=', 0)->count();
        $project = Project::where('is_deleted', '=', 0)->count();
        $page_data = array('breadcrumbs' => Breadcrumbs::generate(), 'total_companies' => $companies, 'total_users' => $users, 'project' => $project, 'total_issueto' => $total_issueto);
        return view('superadmin::dashboard')->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('superadmin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    public function profile()
    {
        $id = Auth::user()->id;
        $admin = User::find($id);
        Breadcrumbs::addBreadcrumb('My Profile', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());

        //Get superadmin details according to id
        $page_data['admin'] = $admin;
        $user_det = User::where('id','=',$id)->find($id);
        $page_data['udetail'] = $user_det->UserDetails;               
        $page_data['option_state'] = array(
            ""=>'Select One',
            "NSW"=>'NSW',
            "QLD"=>'QLD',
            "SA"=>'SA',
            "VIC"=>'VIC',
            "WA"=>"WA");           
        if (!empty($page_data['admin'])) {
            $data['scripts'] = array('angularjs/angular.min','angularjs/controllers/superadmincontroller');
            return view('superadmin::user.admin-profile',compact('data'))->with($page_data);
        } else {
            return redirect('superadmin');
        } 
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id,$userform)
    {
        if ($id == 0) {
            die("error message");
        }

        switch ($userform) {
            case 'profile':
                $admin = User::find($id);
                $admindata = array();
                $admindata['username'] = $admin->name;
                $admindata['fullname'] = $admin->full_name;
                $admindata['password'] = $admin->plain_password; 
                $admindata['email'] = $admin->email; 
                $admindata['phone'] = $admin->phone;                
                return json_encode($admindata);
                break;

            case 'logoimage':
                $admin = UserDetails::where('id','=',$id)->get();                
                break;

            default:
                die("error message");
            break;
        }
        
       // return view('superadmin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,User $admin)
    {        
        $user_id = Input::get('id');
        $adminform = Input::get('adminform');
        switch ($adminform) {
            case 'profile': 
                $admin = User::where('id','=',$user_id)->first();
                $admin->name = Input::get('username');
                $admin->full_name = Input::get('fullname');
                $admin->password = bcrypt(Input::get('password')); 
                $admin->email = Input::get('email'); 
                $admin->phone = Input::get('phone'); 
                $admin->plain_password = Input::get('password'); 
                $success = $admin->save();
                if($success){
                    $success = \Session::flash('flash_alert_notice', 'Profile data updated successfully !');
                    return $success;
                }else{
                    return "there was some server error";
                }
            break;

            case 'logo':
                $username = Input::get('username');
                $file = Input::file('logo'); 
                
                // Make directory if not exist
                $temp_path = public_path('uploads/user/temp/');
                if(!File::exists($temp_path)) {
                    File::makeDirectory($temp_path, 0777, true, true);
                }
                $path_200 = public_path('uploads/user/thumbnail_200x200/');
                if(!File::exists($path_200)) {
                    File::makeDirectory($path_200, 0777, true, true);
                }
                $path_29 = public_path('uploads/user/thumbnail_29x29/');
                if(!File::exists($path_29)) {
                    File::makeDirectory($path_29, 0777, true, true);
                }
                
                if ($file) {
                    $image_name = $file->getClientOriginalName();
                    $img_path = public_path('uploads/user/');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $fileName = $user_id . '_' .$username.".". $extension;
                    $uploaded_at = $file->move($temp_path, $fileName);
                    // Main Image
                    $img = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path.$fileName)) {
                            unlink($img_path.$fileName);
                    }                
                    $save_thumb = $img->save($img_path.$fileName);

                    // Resize image for 200x200
                    $img_thumb1 = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path. "thumbnail_200x200/" .$fileName)) {
                            unlink($img_path. "thumbnail_200x200/" .$fileName);
                    }                
                    $save_thumb = $img_thumb1->save($img_path. "thumbnail_200x200/" .$fileName);
                    
                    // Resize image for 138x30
                    $img_thumb2 = Image::make($temp_path.$fileName)->resize(29,29);
                    if (file_exists($img_path. "thumbnail_29x29/" .$fileName)) {
                            unlink($img_path. "thumbnail_29x29/" .$fileName);
                        }
                    $save_thumb = $img_thumb2->save($img_path . "thumbnail_29x29/" .$fileName);            
                    
                    // Remove temperory file
                    if (file_exists($temp_path.$fileName)) {
                        unlink($temp_path.$fileName);
                    }
                    
                } else {
                    $fileName = '';
                } 

                //update user_details table data
                $user_details = UserDetails::firstOrNew(array('user_id' => $user_id));
                $user_details->user_id = $user_id;
                $user_details->logo_image = $fileName;
                $success = $user_details->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Logo has been updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;

            default:
                return redirect('superadmin/user')->with('flash_alert_notice', 'There was nothing to update.');    
            break;
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
