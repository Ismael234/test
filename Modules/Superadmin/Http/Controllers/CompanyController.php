<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Company;
use App\CompanyDetails;
use App\CompanyUserMapping;
use App\User;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreComponyPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Image;
use File;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        Breadcrumbs::addBreadcrumb('Company List',  url('superadmin/company'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('company');
        $company = Company::where('is_deleted', '=', 0)->paginate(10);       
        return view('superadmin::company.company_list',compact('data'))->with($page_data);
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {            
        $data['scripts'] = array('company');
        $option_state = array(
            ""=>'Select One',
            "NSW"=>'NSW',
            "QLD"=>'QLD',
            "SA"=>'SA',
            "VIC"=>'VIC',
            "WA"=>"WA");  
        Breadcrumbs::addBreadcrumb('Company List',  url('superadmin/company'));
        Breadcrumbs::addBreadcrumb('Add Company', '');
        $page_data['breadcrumbs'] = Breadcrumbs::generate();
        return view('superadmin::company.add_company',compact('data'))->with('option_state',$option_state)->with($page_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComponyPost $request)
    {
        $input = Input::except('_token');
        //Store value for Company Details
        $insertInCompany = array(
            "name"  =>  strtolower(Input::get("name")),
            "type"  =>  Input::get("ctype"),
            "registration_number" =>  Input::get("cregis"),
            "code" =>  rand(),
            "email"  =>  Input::get("email"),
            "phone"  =>  Input::get("phone"),
            "modif ied_by"=> Auth::user()->id,
            "created_at"=> date('Y-m-d'),
        );        
        $save = Company::create($insertInCompany);
        $cmp_id = $save->id;
        $bus_add1 = Input::get("bus_add1");
        $bus_add2 = Input::get("bus_add2");
        
        //Store value for Company Address
        $insertInCompanyDetails = array(
            "company_id"=>  "$cmp_id",
            "busi_address1" =>  "$bus_add1",
            "busi_address2" =>  "$bus_add2",
            "busi_suburb" =>  Input::get("bus_suburb"),
            "busi_state" =>  Input::get("bus_state"),
            "busi_post" =>  Input::get("bus_post"),
            "busi_country" =>  Input::get("bus_country"),
            "bill_address1" =>  Input::get("buld_add1"),
            "bill_address2" =>  Input::get("buld_add2"),
            "bill_suburb" =>  Input::get("buld_suburb"),
            "buld_state" =>  Input::get("buld_state"),
            "bill_post" =>  Input::get("buld_post"),
            "bill_country" =>  Input::get("buld_country")
        );                        
        $company_details = CompanyDetails::create($insertInCompanyDetails);        
        
        // Send Email Notif ication
        $template = "superadmin::email.notif ication";
        $link = url('/');
        $email = Input::get('email');
        $subject = "GendefId Company Registeration";
        $data = array('name'=>Input::get('name'),'link'=>$link,'email'=>$email,'subject'=>$subject);
        Helper::sendMessage($template,$data);
        return redirect('superadmin/company')->with('flash_alert_notice', 'Company created successfully!');
    }

    /**
     * Display the specif ied resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company,$id=0)
    {
        //Get company details according to id
        if (!empty($id)) {
            $cmp = $company->where('is_deleted','=',0)->find($id);    
            $cmp_details = $cmp->CompanyDetails;            
            $data['scripts'] = array('company','angularjs/angular.min','angularjs/controllers/companycontroller');
            Breadcrumbs::addBreadcrumb('Company List',  url('superadmin/company'));
            Breadcrumbs::addBreadcrumb('Company Details', '');
            $page_data = array('breadcrumbs' => Breadcrumbs::generate());
            $option_state = array(
            ""=>'Select One',
            "NSW"=>'NSW',
            "QLD"=>'QLD',
            "SA"=>'SA',
            "VIC"=>'VIC',
            "WA"=>"WA");  
            return view('superadmin::company.show_company',compact('data','option_state'))->with('cmp_details',$cmp_details)->with('cmp',$cmp)->with($page_data);
        } else {
            return redirect('superadmin/company');
        }
    }

    /**
     * Show the form for editing the specif ied resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company,$id=0,$companyform)
    {                  
        //Get company data by cases
        if ($id == 0) {
            die("error message");
        }
        $company = Company::find($id);
        switch ($companyform) {            
            case 'company':           
                $companydata = array();            
                $companydata['cmpname'] = $company->name;
                $companydata['cregis'] = $company->registration_number;
                $companydata['ctype'] = $company->type; 
                $companydata['email'] = $company->email; 
                $companydata['phone'] = (int)$company->phone;                
                return json_encode($companydata);
            break;

            case 'address':
                $cmp_details = $company->CompanyDetails;
                $companydata = array();
                if (!empty($cmp_details[0])){
                    $companydata['company_id'] = $cmp_details[0]->company_id;
                    $companydata['bus_add1'] = $cmp_details[0]->busi_address1;
                    $companydata['bus_add2'] = $cmp_details[0]->busi_address2;
                    $companydata['bus_suburb'] = $cmp_details[0]->busi_suburb;
                    $companydata['bus_state'] = $cmp_details[0]->busi_state;
                    $companydata['bus_post'] = $cmp_details[0]->busi_post;
                    $companydata['bus_country'] = $cmp_details[0]->busi_country;
                    $companydata['billing_add1'] = $cmp_details[0]->bill_address1;
                    $companydata['billing_add2'] = $cmp_details[0]->bill_address2;
                    $companydata['billing_suburb'] = $cmp_details[0]->bill_suburb;
                    $companydata['billing_state'] = $cmp_details[0]->bill_state;
                    $companydata['billing_post'] = $cmp_details[0]->bill_post;
                    $companydata['billing_country'] = $cmp_details[0]->bill_country;      
                }
                return json_encode($companydata);
            break;

            default:
                die("Oops!! Something went wrong....");
            break;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Company $company)
    {
        //Update Company details according to cases
        $company_id = Input::get('id');
        $company_form = Input::get('company_form');        
        $input = Input::all();       
        switch ($company_form) {

            case 'company':
            $company = Company::firstOrNew(array('id' => $company_id));
            $company->name = strtolower($input['cmpName']['cmpname']);
            $company->type = $input['cmpName']['ctype'];
            $company->registration_number = $input['cmpName']['cregis'];
            $company->email = $input['cmpName']['email'];
            $company->phone = $input['cmpName']['phone'];
            $success = $company->save();
            if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Company detials updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;
            
            case 'address':
            $CompanyDetails = CompanyDetails::firstOrNew(array('company_id' => $input['cmpName']['company_id']));
            $CompanyDetails->busi_address1 = $input['cmpName']['bus_add1'];
            $CompanyDetails->busi_address2 = $input['cmpName']['bus_add2'];
            $CompanyDetails->busi_suburb = $input['cmpName']['bus_suburb'];
            $CompanyDetails->busi_state = $input['cmpName']['bus_state'];
            $CompanyDetails->busi_post = $input['cmpName']['bus_post'];
            $CompanyDetails->busi_country = $input['cmpName']['bus_country'];
            $CompanyDetails->bill_address1 = $input['cmpName']['billing_add1'];
            $CompanyDetails->bill_address2 = $input['cmpName']['billing_add2'];
            $CompanyDetails->bill_suburb = $input['cmpName']['billing_suburb'];
            $CompanyDetails->bill_state = $input['cmpName']['billing_state'];
            $CompanyDetails->bill_post = $input['cmpName']['billing_post'];
            $CompanyDetails->bill_country = $input['cmpName']['billing_country'];
            $success = $CompanyDetails->save();
            if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Address detials updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;
            
            case 'logo':                
                $cmp_id = Input::get('cmp_id');
                $company_name = Input::get('company_name');
                // Make directory if  not exist
                $temp_path = public_path('uploads/company/temp/');
                if (!File::exists($temp_path)) {
                    File::makeDirectory($temp_path, 0777, true, true);
                }
                $path_138 = public_path('uploads/company/thumbnail_138x30/');
                if (!File::exists($path_138)) {
                    File::makeDirectory($path_138, 0777, true, true);
                }
                
                if  (Input::file()[0]) {
                    $file = Input::file()[0];
                    $image_name = $file->getClientOriginalName();
                    $img_path = public_path('uploads/company/');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $fileName = $cmp_id . '_' .$company_name.".". $extension;
                    $uploaded_at = $file->move($temp_path, $fileName);
                    
                    // Resize image for 200x200
                    $img = Image::make($temp_path.$fileName)->resize(200,200);
                    if  (file_exists($img_path.$fileName)) {
                            unlink($img_path.$fileName);
                    }                
                    $save_thumb = $img->save($img_path.$fileName);
                    
                    // Resize image for 138x30
                    $img_thumb1 = Image::make($temp_path.$fileName)->resize(138,30);
                    if  (file_exists($img_path. "thumbnail_138x30/" .$fileName)) {
                            unlink($img_path. "thumbnail_138x30/" .$fileName);
                        }
                    $save_thumb = $img_thumb1->save($img_path . "thumbnail_138x30/" .$fileName);            
                    
                    // Remove temperory file
                    if  (file_exists($temp_path.$fileName)) {
                        unlink($temp_path.$fileName);
                    }
                    $insertInCompanyDetails['logo_image'] = $fileName;
                } else {
                    $fileName = '';
                } 
                                   
                $CompanyDetails = CompanyDetails::firstOrNew(array('company_id' => $cmp_id));
                $CompanyDetails->logo_image = $fileName;
                $success = $CompanyDetails->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Logo has been updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;

            default:            
                return redirect('superadmin/company')->with('flash_alert_notice', 'There was nothing to update.');
            break;
        }        
    }

    /**
     * Remove the specif ied resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company,$id)
    {        
        $company = Company::where('id','=',$id)->first();
        $company->is_deleted = 1;
        $company->save();

        $companyDetails = CompanyDetails::where('company_id','=',$id)->first();
        if (!empty($companyDetails))
        {
            $companyDetails->is_deleted = 1;
            $companyDetails->save();
        }

        $companyUserMapping = CompanyUserMapping::where('company_id','=',$id);
        if (!empty($companyUserMapping))
        {
            $companyUserMapping->update(array('is_deleted' => 1));
        }
        return redirect('superadmin/company')->with('flash_alert_notice', 'Company deleted successfully!');
    }

    /**
    * Check the specified name and email is exist
    * And return as true and false
    */
    public function check_isexist()
    {        
        $cname = ucfirst(Input::get('name'));
        $email = Input::get('email');
        $id = Input::get('id');
        if (isset($cname) && !empty($cname)) {
            if (!empty($id)) {
                $is_current_name = Company::where('name',"=",strtolower($cname))->where('is_deleted',"=",0)->where('id',"=",$id)->get();                      
                if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == strtolower($cname)) {
                    return "true";                    
                }
            }            
            $is_cname = Company::where('name',"=",strtolower($cname))->where('is_deleted',"=",0)->get();
            if (isset($is_cname[0]) && !empty($is_cname[0])) {
                return 'false';
            } else {
                return "true";   
            }
        }
        if (isset($email) && !empty($email)) {
            if (!empty($id)) {
                $is_current_email = Company::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if (isset($is_current_email[0]) && !empty($is_current_email[0]) && $is_current_email[0]->email == $email) {
                    return "true";                    
                }
            }
            $is_email = Company::where('email',"=",$email)->where('is_deleted',"=",0)->get();
            if (isset($is_email[0]) && !empty($is_email[0])) {
                return 'false';                
            }else{
                return 'true';                
            }
        }            
    }

    /**
    * List view by Data table for company Details
    */
    public function getData()
    {
        $company1 = Company::select(['id', 'name', 'type', 'email', 'phone'])->where('is_deleted', '=', 0)->get();        
        $company = Datatables::of($company1)->addColumn('action', function ($cmp) {
                return '<a href="company-details/'.$cmp->id.'" class="btn btn-warning btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="delete-company/'.$cmp->id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure you want to delete this Company?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $company;die;
    }

    /**
    * List view by Data table for Assciated user Details
    */
    public function getCompanyUserData($id)
    {
        $comp_users = Helper::getCompanyUsers($id);
        $company_users = User::select([ 'full_name', 'email', 'phone'])->where('is_deleted', '=', 0)->whereIn('id', $comp_users)->get();
        $users = Datatables::of($company_users)->make(true);
        return $users;
        exit;        
    }

    /**
    * List view by Data table for Assciated project Details
    */
    public function getCompanyProjectData(Request $request, $id)
    {         
        \Session::set('cmp_id', $id);
        $comp_projects = Helper::getCompanyProject($id);        
        $project1 = Project::select([ 'id','name', 'type'])->where('is_deleted', '=', 0)->whereIn('id', $comp_projects)->get();
        $projects = Datatables::of($project1)->addColumn('action', function ($prj) {
                return '<a   data-toggle="tooltip" title="Project Configuration"  href='.url('/').'/superadmin/projectConfiguration/projectConfigId/'.$prj->id.' class="btn btn-success btn-xs"><i class="fa fa-star-o"></i></a></a>';
            })->make(true);        
        return $projects;
        exit;        
    }

}