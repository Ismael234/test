<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Role;
use App\Authentication;
use App\RoleAuthMap;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Auth;
class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        Breadcrumbs::addBreadcrumb('Role List',  url('superadmin/role'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('role_list');
        return view('superadmin::role.role_list',compact('data'))->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $authentication = Authentication::where('is_deleted','=',0)->get();        
        $parent = array();
        $child = array();
        foreach ($authentication as $key => $value) {
            if ($value->module_id == 0) {
                $parent[$value->id] = $value->name;
            } else {
                $child[$value->module_id][$value->id] = $value->name;
            }
        }
        $data_tree = array();
        foreach ($parent as $key => $p_value) {
            if (!empty($child[$key])) {
                $data_tree[$key] = array("parent"=>$p_value,'child'=>$child[$key]);
            } else {
                $data_tree[$key] = array("parent"=>$p_value);
            }
        }        
        Breadcrumbs::addBreadcrumb('Role List',  url('superadmin/role'));        
        Breadcrumbs::addBreadcrumb('Add Role', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('logger','treeview','role');
        return view('superadmin::role.add_role',compact('data','data_tree'))->with($page_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $input = Input::except('_token');
        $authentication = Authentication::where('is_deleted','=',0)->get();        
        $insertInRole = array(
            "name"  =>  Input::get("name"),
            "created_by" => Auth::user()->id,
            "created_at" => date('Y-m-d h:i:s')
            );
        $save = Role::create($insertInRole);  
        $permission = Input::get("permission");        
        foreach ($authentication as $key => $value) {            
            $insertInRoleAuthMap['role_id'] = $save->id;
            $insertInRoleAuthMap["authorisation_id"] = $value->id;
            if (in_array($value->id, $permission)) {
                $insertInRoleAuthMap["status"] = 0;
            } else {
                $insertInRoleAuthMap["status"] = 1;
            }
            $company_details = RoleAuthMap::create($insertInRoleAuthMap);            
            $insertInRoleAuthMap = array();
        }
        $arr = array('msg'=>'Permission creatd succesfully.','status'=>0);
        echo json_encode($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role,$id)
    {        
        $role = Role::where('is_deleted','=',0)->where('id','=',$id)->first();
        $roles = $role->roleAuthDetails->where('status','=',0); 
        $auth_id = array();
        foreach($roles as $r) {
            $auth_id[] = $r->authorisation_id;
        }
        $auth_permission = Authentication::whereIn('id',$auth_id)->get();        
        $parent = array();
        $child = array();
        $allPermission = array();
        $type = array();
        foreach ($auth_permission as $key => $value) {
            $allPermission[] = $value->id;
            $type[$value->id] = $value->type;
            if ($value->module_id == 0) {
                $parent[$value->id] = $value->name;
            } else {
                $child[$value->module_id][$value->id] = $value->name;
            }            
        }               
        
        Breadcrumbs::addBreadcrumb('Role List',  url('superadmin/role'));        
        Breadcrumbs::addBreadcrumb('Show Role', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $authentication = Authentication::where('is_deleted','=',0)->get();        
        $allparent = array();
        $allchild = array();
        $allchild = array();
        foreach ($authentication as $key => $value) {
            if ($value->module_id == 0) {
                $allparent[$value->id] = $value->name;
            } else {
                $allchild[$value->module_id][$value->id] = $value->name;
            }            
        }
        $data_tree = array();
        foreach ($allparent as $key => $p_value) {
            if (!empty($allchild[$key])) {
                $data_tree[$key] = array("parent"=>$p_value,'child'=>$allchild[$key]);
            } else {
                $data_tree[$key] = array("parent"=>$p_value);
            }
        }
        $data['scripts'] = array('logger','treeview','angularjs/angular.min','angularjs/controllers/rolecontroller');
        return view('superadmin::role.show_role',compact('data','data_tree','parent','child','role','allPermission','type'))->with($page_data);        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {        
        $id = Input::get('id');
        $roles = Role::firstOrNew(array("id"=>$id));
        $roles->name = Input::get('name');
        $roles->save();        
        $authentication = Authentication::where('is_deleted','=',0)->get();  
        $permission = Input::get("permission");
        foreach ($authentication as $key => $value) {
            $auth_role = RoleAuthMap::firstOrNew(array('role_id'=>$id,"authorisation_id"=>$value->id));            
            if (in_array($value->id, $permission)) {
                $auth_role->status = 0;
            } else {
                $auth_role->status = 1;
            }
            $auth_role->save();
        }
        $arr = array('msg'=>'Permission updated succesfully.','status'=>0);
        echo json_encode($arr);
        die;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role,$id)
    {
        $role = Role::where('id','=',$id)->first();
        $role->is_deleted = 1;
        $role->save();
        return redirect('superadmin/role')->with('flash_alert_notice', 'Role deleted successfully!');
    }
    /**
    * Check the specified Role is exist
    * And return as true and false
    */
    public function checkRole()
    {    
        $cname = Input::get('name');            
        $id = Input::get('id');
        if (isset($cname) && !empty($cname)) {
            if (!empty($id)) {
                $is_current_name = Role::where('name',"=",$cname)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $cname)
                {
                    return "true";
                    die;
                }
            }            
            $is_cname = Role::where('name',"=",$cname)->where('is_deleted',"=",0)->get();            
            if (isset($is_cname[0]) && !empty($is_cname[0])) {
                return 'false';
            } else {
                return 'true';
            }
        }        
    }

    /**
    * List view by Data table for Role Details
    */  
    public function getData(){
        $role = Role::select(['id', 'name'])->where('is_deleted', '=', 0)->where('is_deleted', '=', 0)->get();                          
        $company = Datatables::of($role)->addColumn('action', function ($role) {
            return '<a href="show-role/'.$role->id.'" class="btn btn-warning btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="delete-role/'.$role->id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure you want to delete this user?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
        })->make(true);
        return $company;die;           
    }
}
