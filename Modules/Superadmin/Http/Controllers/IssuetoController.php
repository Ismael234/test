<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Issueto;
use App\IssuetoUsers;
use App\Company;
use App\CompanyIssuetoMapping;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreProjectIssuetoPost;
use Modules\Superadmin\Http\Requests\StoreProjectIssuetoUsersPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;



class IssuetoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                 
        Breadcrumbs::addBreadcrumb('Issueto List',  url('superadmin/issueto'));
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('issueto');
        $page_data['obj_companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['issueto_comps'] = array();
        $page_data['cur_it'] = '';
        $issueto = Issueto::where('is_deleted', '=', 0)->paginate(10);
        return view('superadmin::issueto.issueto_list',compact('data'))->with($page_data);
    }    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    
        
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_issueto(StoreIssuetoPost $request)
    {
        $issueto = new Issueto;
        $issueto->name = Input::get('name');
        $issueto->description = Input::get('description');
        $insert_issueto = $issueto->save();
        if (!empty($issueto->id)) {
            $comps = Input::get('company');
            foreach($comps as $comp){
                $arr = array('issueto_id' => $issueto->id, 'company_id' => $comp);
                CompanyIssuetoMapping::insert($arr);
            }
            return redirect('superadmin/issueto')->with('flash_alert_notice', 'Issueto created succesfully.');
        } else {
            return redirect('superadmin/user');
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Issueto $issueto,$id=0)
    {
        Breadcrumbs::addBreadcrumb('Issueto List',  url('superadmin/issueto'));
        Breadcrumbs::addBreadcrumb('Issueto Details', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());

        //Get specific user details according to id        
        $data['scripts'] = array('issueto');
        $page_data['issueto_comps'] =  Helper::getCompanyIssuetoMapping($id);
        $page_data['obj_companies'] =  Company::select('id', 'name')->where('is_deleted', '=', 0)->orderBy('name', 'asc')->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['issueto'] = $issueto->where('is_deleted','=',0)->find($id);
        $page_data['issueto_id'] = $id;
        $page_data['option_activity'] = array(
            "Brickwork"=>"Brickwork",
            "Carpentry"=>"Carpentry",
            "Cleaning"=>"Cleaning",
            "Concreting"=>"Concreting",
            "Electrical"=>"Electrical",
            "Fire services"=>"Fire services",
            "Joinery"=>"Joinery",
            "Plasterboard"=>"Plasterboard",
            "Painting"=>"Painting",
            "Plumbing"=>"Plumbing",
            "Metalwork"=>"Metalwork",
            "Structural steel"=>"structural steel",
            "Waterproofing"=>"Waterproofing",
            "Other"=>"Other",
        );
        
        if (!empty($page_data['issueto'])) {
            return view('superadmin::issueto.show_issueto',compact('data'))->with($page_data);
        } else {
            return redirect('superadmin/issueto');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Issueto $isueto,$id=0)
    {                  
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(StoreIssuetoPost $request)
    {
        $issueto_id = Input::get('issueto_id');
        $issueto = Issueto::firstOrNew(array('id' => $issueto_id));
        $issueto->name = Input::get('name');
        $issueto->description = Input::get('description');
        $success = $issueto->save();
        if ($success) {
            $ci_map = new CompanyIssuetoMapping;
            $obj_issueto_comps = $ci_map->select('company_id')->where('issueto_id', '=', $issueto_id)->where('is_deleted', '=', 0)->get();
            $issueto_comps = array();
            foreach($obj_issueto_comps as $comp) {
                $issueto_comps[] = $comp->company_id;
            }
            $obj_issueto_comps_del = $ci_map->select("company_id")->where('issueto_id', '=', $issueto_id)->where('is_deleted', '=', 1)->get();
            $issueto_comps_del = array();
            foreach($obj_issueto_comps_del as $comp) {
                $issueto_comps_del[] = $comp->company_id;
            }
            $comps = Input::get('company');
            $delete_issueto_comps = array_diff($issueto_comps,$comps);
            $to_del_comps = $ci_map->where('issueto_id', '=', $issueto_id)->whereIn('company_id', $delete_issueto_comps);
            if (!empty($to_del_comps)) {
                $to_del_comps->update(array('is_deleted'=>1));
            }
            $add_issueto_comps = array_diff($comps,$issueto_comps);
            foreach($add_issueto_comps as $comp) {
                if (in_array($comp, $issueto_comps_del)) {
                    
                    $to_add_comps = $ci_map->where('issueto_id', '=', $issueto_id)->where('company_id', '=', $comp);
                    if (!empty($to_add_comps)) {
                        $to_add_comps->update(array('is_deleted'=>0));
                    }
                } else {
                    $ci_map->issueto_id = $issueto_id;
                    $ci_map->company_id = $comp;
                    $ci_map->save();
                }
            }            
            return redirect("superadmin/issueto-details/$issueto_id")->with('flash_alert_notice', 'Issueto details updated successfully.');
        } else {
            return redirect('superadmin/issueto');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Issueto $issueto,$id)
    {
        if (!empty($id)) {
            $cur_issueto = $issueto->where('id', $id);
            if (!empty($cur_issueto)) {
                $cur_issueto->update(array('is_deleted' => 1));
                $companyIssuetoMapping = CompanyIssuetoMapping::where('issueto_id','=',$id);
                if (!empty($companyIssuetoMapping)) {
                    $companyIssuetoMapping->update(array('is_deleted' => 1));
                }
                $issuetoUsers = IssuetoUsers::where('issueto_id','=',$id);
                if (!empty($issuetoUsers)) {
                    $issuetoUsers->update(array('is_deleted' => 1));
                }
            }
        }
        return redirect('superadmin/issueto')->with('flash_alert_notice', 'Issueto deleted successfully.');
    }

    /**
    * Check the specified issue to is exist
    * And return as true and false
    */
    public function issueto_check_isexist()
    {
        $name = Input::get('name');
        $id = Input::get('id');
        if (isset($name) && !empty($name)) {
            if (!empty($id)) {
                $is_current_name = Issueto::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if(isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $name)
                {
                    echo "true";
                    die;
                }
            }            
            $is_name = Issueto::where('name',"=",$name)->where('is_deleted',"=",0)->get();            
            if (isset($is_name[0]) && !empty($is_name[0])) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
        die;
    }
    /**
    * Check the specified issue to user is exist
    * And return as true and false
    */
    public function issueto_user_check_isexist()
    {        
        $issueto_id = Input::get('issueto_id');
        $email = Input::get('email');
        $id = Input::get('id');     
        if(isset($email) && !empty($email)){
            if(!empty($id)){
                $is_current_email = IssuetoUsers::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"=",$id)->where('issueto_id',"=",$issueto_id)->get();   
                if(isset($is_current_email[0]) && !empty($is_current_email[0]) && $is_current_email[0]->email == $email)
                {
                    echo "true";
                    die;
                }
            }
            $is_email = IssuetoUsers::where('email',"=",$email)->where('is_deleted',"=",0)->where('issueto_id',"=",$issueto_id)->get();
            if(isset($is_email[0]) && !empty($is_email[0])){
                echo 'false';
            }else{
                echo 'true';
            }
        }
        die;
    }

    /**
    * List view by Data table for issue to Details
    */
    public function getIssuetoData()
    {
        $issueto1 = Issueto::select(['id', 'name', 'description'])->where('is_deleted', '=', 0)->get();        
        $issueto = Datatables::of($issueto1)->addColumn('action', function ($it) {
                return '<a href="issueto-details/'.$it->id.'" class="btn btn-warning btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="'.url('/').'/superadmin/delete-issueto/'.$it->id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure you want to delete this Issueto?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $issueto;die;        
    }

    /**
    * Store a newly created isssue to user resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store_issueto_user(StoreIssuetoUsersPost $request)
    {
        $issueto_id = Input::get('issueto_id');
        $issuetoUser = new IssuetoUsers;
        $issuetoUser->name = Input::get('name');
        $issuetoUser->issueto_id = $issueto_id;
        $issuetoUser->activity = Input::get('activity');
        $issuetoUser->phone = Input::get('phone');
        $issuetoUser->email = Input::get('email');
        $insert_issueto_user = $issuetoUser->save();

        if(!empty($issuetoUser->id)){
            
            return redirect("superadmin/issueto-details/$issueto_id")->with('flash_alert_notice','Issueto user created succesfully.');
        }else{
            return redirect('superadmin/issueto');
        }
        
    }

    /**
    * Update isssue to user resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function update_issueto_user(StoreIssuetoUsersPost $request)
    {
        $issueto_id = Input::get('issueto_id');
        $issueto_user_id = Input::get('issueto_user_id');
        $issuetoUser = IssuetoUsers::firstOrNew(array('id' => $issueto_user_id));
        $issuetoUser->name = Input::get('name');
        $issuetoUser->issueto_id = $issueto_id;
        $issuetoUser->activity = Input::get('activity');
        $issuetoUser->phone = Input::get('phone');
        $issuetoUser->email = Input::get('email');
        $success = $issuetoUser->save();
        if ($success) {   
            return redirect("superadmin/issueto-details/$issueto_id")->with('flash_alert_notice','Issueto user created succesfully.');      
        } else {
            return redirect('superadmin/issueto');
        }
    }

    /**
    * Remove isssue to user resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function destroy_issueto_user(IssuetoUsers $issuetoUsers, $id)
    {
       if (!empty($id)) {
            
            $cur_user = $issuetoUsers->where('id', $id);
            if(!empty($cur_user)){
                $cur_user->update(array('is_deleted' => 1));
            }
        }
        return redirect('superadmin/issueto')->with('flash_alert_notice', 'Issueto user deleted successfully.');
    }

    /**
    * List view by Data table for issue to user data
    */
    public function getIssuetoUsersData($issueto_id)
    {
        $issuetoUsers1 = IssuetoUsers::select(['id', 'name', 'email', 'phone', 'activity'])->where('is_deleted', '=', 0)->where('issueto_id', '=', $issueto_id)->get();        
        $issuetoUsers = Datatables::of($issuetoUsers1)->addColumn('action', function ($itu) {
                return '<a href="javascript:;" class="btn btn-xs default" onclick="edit_issueto_user('.$itu->id.');"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="'.url('/').'/superadmin/delete-issueto-user/'.$itu->id.'" title="Delete" class="btn btn-xs default"  onclick="return confirm('."'Are you sure you want to delete this Issueto?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $issuetoUsers;die;        
    }

    /**
    * Fetch isssue to user resource from storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function get_issueto_user_details()
    {
        $id = Input::get('id');
        if (!empty($id)) {
            $issuetoUsers = IssuetoUsers::select(['id', 'issueto_id', 'name', 'email', 'phone', 'activity'])->where('is_deleted', '=', 0)->where('id', '=', $id)->get();
        }
        echo $issuetoUsers;
        exit();
    }
}