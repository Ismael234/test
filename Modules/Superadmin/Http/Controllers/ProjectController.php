<?php

namespace Modules\Superadmin\Http\Controllers;

use App\User;
use App\Project;
use App\ProjectUserRole;
use App\CompanyProjectMapping;
use App\Role;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreProjectPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use Lang;
use DB;
use Helpers as Helper;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;


class ProjectController extends Controller
{
    /**
     * Display a listing of the Projects.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {         
        $data['scripts'] = array('project');
        Breadcrumbs::addBreadcrumb('Project List',  url('superadmin/project'));
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        return view('superadmin::project.project_list',compact('data'))->with($page_data);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $data['scripts'] = array('project');
        $page_data = array();
        $page_data['option_type'] = array(
            ""              =>  'Select Project Type',
            "Educational"   =>  'Educational',
            "Industrial"    =>  'Industrial',
            "Offices"       =>  'Offices',
            "Other"         =>  'Other',
            "Residential"   =>  'Residential',
            "Retail"        =>  'Retail',
            "Test"          =>  'Test'); 

        $page_data['option_state'] = array(
            ""      =>  'Select One',
            "NSW"   =>  'NSW',
            "QLD"   =>  'QLD',
            "SA"    =>  'SA',
            "VIC"   =>  'VIC',
            "WA"    =>  "WA");

        $page_data['option_country'] = array(
            ""          =>  'Select One',
            "Australia" =>  'Australia');
        $page_data['companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['cur_project'] = '';
        $page_data['proj_comps'] = array();
        Breadcrumbs::addBreadcrumb('Project List',  url('superadmin/project'));
        Breadcrumbs::addBreadcrumb('Add Project', '');
        $page_data['breadcrumbs'] = Breadcrumbs::generate();        
        return view('superadmin::project.add_project',compact('data'))->with($page_data);
    }

    /**
     * Save new Project and associate with all mapping tables.
     *
     */
    public function store(StoreProjectPost $request)
    {   
        $project = new Project;
        $project->name = Input::get('name');
        $project->code = Input::get('code');
        $project->type = Input::get('type');
        $project->description = Input::get('description'); 
        $project->address1 = Input::get('address1'); 
        $project->address2 = Input::get('address2');
        $project->geo_code = Input::get('geo_code'); 
        $project->suburb = Input::get('suburb'); 
        $project->state = Input::get('state'); 
        $project->country = Input::get('country');
        $project->postal_code = Input::get('postal_code');
        $project->created_by = Auth::user()->id; 
        $insert_project = $project->save();
        $image = Input::file('project_logo');
        if (!empty($image)) {
            $filename  = $project->id.'_'.$project->name. '.' . $image->getClientOriginalExtension();
            $path = "uploads/project";
            $move_logo = $image->move($path,$filename);
            $project->logo  = $filename;
            $project->image = $filename;
            $project->save();
        }
        
        if (!empty($project->id)) {
            $comps = Input::get('company');
            foreach($comps as $comp) {
                $arr = array('project_id' => $project->id, 'company_id'=> $comp);
                CompanyProjectMapping::insert($arr);
            }
            return redirect('superadmin/project')->with('flash_alert_notice', 'Project created succesfully.');        
        } else {
            return redirect('superadmin/project');
        }
    }

    /**
     * Display specified Project details.
     *
     */
    public function show(Project $project,$id)
    {
        Breadcrumbs::addBreadcrumb('Project List',  url('superadmin/project'));
        Breadcrumbs::addBreadcrumb('Project Details', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());

        //Get specific user details according to id
        $page_data['project_companies'] =  Helper::getCompanyProjectMapping($id);
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['project'] = $project->where('is_deleted','=',0)->find($id);

        $comp_detail = CompanyProjectMapping::where('is_deleted','=',0);                    
        if (empty($comp_detail)) {
            return redirect(url('superadmin/project'));
        } else {
            $page_data['logo_image'] = 'default-profile.png';
            $page_data['option_type'] = array(
                "Educational"   =>  'Educational',
                "Industrial"    =>  'Industrial',
                "Offices"       =>  'Offices',
                "Other"         =>  'Other',
                "Residential"   =>  'Residential',
                "Retail"        =>  'Retail',
                "Test"          =>  'Test'); 

            $page_data['option_state'] = array(
                "NSW"   =>  'NSW',
                "QLD"   =>  'QLD',
                "SA"    =>  'SA',
                "VIC"   =>  'VIC',
                "WA"    =>  "WA");

            $page_data['option_country'] = array(
                "Australia" =>  'Australia');

            $page_data['companies'] = Company::select('id', 'name')->where('is_deleted', '=', 0)->orderBy('name', 'asc')->get();
            $proj_comps = array();
            foreach($comp_detail as $comp){
                $proj_comps[] = $comp->company_id;
            }
            $page_data['comp_detail'] = $comp_detail;
            $page_data['proj_comps'] = $proj_comps;
        }           
        if (!empty($page_data['project'])) {
            $data['scripts'] = array('angularjs/angular.min','angularjs/controllers/projectcontroller', 'project');
            return view('superadmin::project.show_project',compact('data'))->with($page_data);
        } else {
            return redirect('superadmin/project');
        }
    }

    /**
     * Edit specified Project details.
     *
     */
    public function edit($id,$projectform)
    {
        if ($id == 0) {
            die("error message");
        }
        switch ($projectform) {

            case 'details':
                $project = Project::find($id);
                $projectdata = array();
                $projectdata['name'] = $project->name;
                $projectdata['code'] = $project->code;
                $projectdata['type'] = $project->type; 
                $projectdata['description'] = $project->description;                 
                return json_encode($projectdata);
            break;

             case 'address':
                $project = Project::find($id);
                $addressData = array();
                $addressData['address1'] = $project->address1;
                $addressData['address2'] = $project->address2;
                $addressData['suburb'] = $project->suburb; 
                $addressData['state'] = $project->state; 
                $addressData['postal_code'] = $project->postal_code; 
                $addressData['country'] = $project->country;
                $addressData['geo_code'] = $project->geo_code;
                return json_encode($addressData);
            break;

            case 'logo':
                $project = Project::find($id);
                print_r($project); die;
            break;

            case 'companies':
                $companyData['proj_comps'] = 'true';                
                return json_encode($companyData);
            break;
            
            default:
                die("error message");
            break;
        }
        
    }

    /**
     * Update the specified Project details.
     *
     */
    public function update()
    {
        $project_id = Input::get('id');
        $project_form = Input::get('projectform');
        switch ($project_form) {
            case 'details':
                $project = Project::firstOrNew(array('id' => $project_id));
                $project->name = Input::get('name');
                $project->code = Input::get('code');
                $project->type = Input::get('type'); 
                $project->description = Input::get('description'); 
                $success = $project->save();
                return "project info has been updated";
            break;

            case 'address':
                $project = Project::firstOrNew(array('id' => $project_id));
                $project->address1 = Input::get('address1');
                $project->address2 = Input::get('address2');
                $project->suburb = Input::get('suburb');
                $project->state = Input::get('state');
                $project->postal_code = Input::get('postal_code');
                $project->country = Input::get('country');
                $project->geo_code = Input::get('geo_code');
                $success = $project->save();
                return "adress has been updated";
            break;

            case 'logoimage':
                $project = Project::firstOrNew(array('id' => $project_id));
                $image = Input::file('logo');
                if (!empty($image)) {
                    $filename  = $project->id.'_'.$project->name. '.' . $image->getClientOriginalExtension();
                    $path = "uploads/project";
                    if (file_exists($path.$filename)) {
                        unlink($path.$filename);
                    }
                    $test = $image->move($path,$filename);
                    $project->logo = $filename;
                    $project->image = $filename;
                    $success = $project->save();
                }
            break;

            case 'companies':
                $comp_proj_map = new CompanyProjectMapping;
                $obj_cur_proj_comps = $comp_proj_map->select('company_id')->where('project_id', '=', $project_id)->where('is_deleted', '=', 0)->get();
                $cur_proj_comps = array();
                foreach($obj_cur_proj_comps as $comp) {
                    $cur_proj_comps[] = $comp->company_id;
                }
                $obj_already_deleted_project_comps = $comp_proj_map->select("company_id")->where('project_id', '=', $project_id)->where('is_deleted', '=', 1)->get();
                $already_deleted_project_comps = array();
                foreach($obj_already_deleted_project_comps as $comp) {
                    $already_deleted_project_comps[] = $comp->company_id;
                }
                $new_project_comps = Input::get('companies');
                $new_deleted_project_comps = array_diff($cur_proj_comps,$new_project_comps);
                $to_delete_comps = $comp_proj_map->where('project_id', '=', $project_id)->whereIn('company_id', $new_deleted_project_comps);
                if (!empty($to_delete_comps)) {
                    $to_delete_comps->update(array('is_deleted'=>1));
                }
                $new_added_project_comps = array_diff($new_project_comps,$cur_proj_comps);
                foreach($new_added_project_comps as $comp) {
                    if (in_array($comp, $already_deleted_project_comps)) {
                        $to_add_comps = $comp_proj_map->where('project_id', '=', $project_id)->where('company_id', '=', $comp);
                        if (!empty($to_add_comps)) {
                            $to_add_comps->update(array('is_deleted'=>0));
                        }
                    } else {
                        $comp_proj_map->project_id = $project_id;
                        $comp_proj_map->company_id = $comp;
                        $success = $comp_proj_map->save();
                    }
                }
                return "Companies updated successfully";
            break;

            default:
                return redirect('superadmin/project')->with('flash_alert_notice', 'There was nothing to update.');    
            break;
        }
       
    }

    /**
     * Soft remove the specified Project and all its mapping.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, $id)
    {   
        if (!empty($id) && $id != 0) {
            $cur_proj = Project::where('id', $id);
            if (!empty($cur_proj)) {
                $cur_proj->update(array('is_deleted' => 1));
                $companyProjectMapping = CompanyProjectMapping::where('project_id','=',$id);
                if (!empty($companyProjectMapping)) {
                    $companyProjectMapping->update(array('is_deleted' => 1));
                }
            }
        }
        return redirect('superadmin/project')->with('flash_alert_notice', 'Project deleted successfully.');
    }

    /**
     * Check if the Email or Name input already exists.
     *
     */
    public function check_isexist(){            
        $username = Input::get('username');
        $email = Input::get('email');
        $id = Input::get('id');
        if(isset($username) && !empty($username)){
            if(!empty($id)){
                $is_current_name = User::where('name',"=",$username)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if(isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $username)
                {
                    echo "true";
                    die;
                }
            }            
            $is_username = User::where('name',"=",$username)->where('is_deleted',"=",0)->get();            
            if(isset($is_username[0]) && !empty($is_username[0])){
                echo 'false';
            }else{
                echo 'true';
            }
        }
        if(isset($email) && !empty($email)){
            if(!empty($id)){
                $is_current_email = User::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if(isset($is_current_email[0]) && !empty($is_current_email[0]) && $is_current_email[0]->email == $email)
                {
                    echo "true";
                    die;
                }
            }
            $is_email = User::where('email',"=",$email)->where('is_deleted',"=",0)->get();
            if(isset($is_email[0]) && !empty($is_email[0])){
                echo 'false';
            }else{
                echo 'true';
            }
        }
        die;
    }

    /**
     * Get Project list using DataTables.
     *
     */
    public function getData(){
        $project1 = Project::select(['id', 'name', 'code', 'type', 'description'])->where('is_deleted', '=', 0)->get(); 
        $project = Datatables::of($project1)->addColumn('action', function ($prj) {
                return '<a href="project-details/'.$prj->id.'" class="btn btn-warning btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="delete-project/'.$prj->id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure you want to delete this Project?'".');"><i class="fa fa-trash-o"></i></a>';
           })->make(true);
        return $project;
        exit;        
    }

    /**
     * Get associated Company list using DataTables.
     */
    public function getProjectCompanyData($id){        
        $project_comps = Helper::getCompanyProjectMapping($id);
        $project_company1 = Company::select([ 'name', 'type'])->where('is_deleted', '=', 0)->whereIn('id', $project_comps)->get();
        $project_company = Datatables::of($project_company1)->make(true);                          
        return $project_company;
        exit;        
    }
}
