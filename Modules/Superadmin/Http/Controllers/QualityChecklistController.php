<?php

namespace Modules\Superadmin\Http\Controllers;

use App\QualityChecklist;
use App\QualityChecklistTask;
use App\Company;
use App\CompanyChecklistMapping;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreQualityChecklistPost;
use Modules\Superadmin\Http\Requests\StoreQualityChecklistTaskPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;



class QualityChecklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //Load Quality Checklist view 
        $page_title     = 'Quality Checklist'; 
        $page_action    = 'Quality Checklist Details';
        $viewPage       = 'quality-checklist';
        $viewPage1       = '';

        Breadcrumbs::addBreadcrumb('Quality Checklist',  url('superadmin/quality-checklist'));
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('quality_checklist');
        $page_data['obj_companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['checklist_comps'] = array();
        $page_data['cur_checklist'] = '';
        $issueto = QualityChecklist::where('is_deleted', '=', 0)->paginate(10);
        return view('superadmin::quality_checklist.quality_checklist',compact('data','page_title','page_action','viewPage','viewPage1'))->with($page_data);
    }    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    
        
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_checklist(StoreQualityChecklistPost $request)
    {
        $checklist = new QualityChecklist;
        $checklist->checklist_name = Input::get('checklist_name');
        $insert_checklist = $checklist->save();

        if (!empty($checklist->id)) {
            $comps = Input::get('company');
            foreach($comps as $comp) {
                $arr = array('checklist_id' => $checklist->id, 'company_id' => $comp);
                CompanyChecklistMapping::insert($arr);
            }
            return redirect('superadmin/quality-checklist')->with('flash_alert_notice', 'Checklist created succesfully.');
        } else {
            return redirect('superadmin/quality-checklist');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(QualityChecklist $checklist,$id=0)
    {
        Breadcrumbs::addBreadcrumb('Quality Checklist',  url('superadmin/quality-checklist'));
        Breadcrumbs::addBreadcrumb('Quality Checklist Details', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        //Get specific user details according to id
        $page_title     = 'Quality Checklist'; 
        $page_action    = '';
        $viewPage       = 'quality-checklist';
        $viewPage1      = '';
        $data['scripts'] = array('quality_checklist');

        $page_data['checklist_comps'] =  Helper::getCompanyChecklistMapping($id);
        $page_data['obj_companies'] =  Company::select('id', 'name')->where('is_deleted', '=', 0)->orderBy('name', 'asc')->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['checklist'] = $checklist->where('is_deleted','=',0)->find($id);
        $page_data['checklist_id'] = $id;
        
        if (!empty($page_data['checklist'])) {
            //$page_data['issueto_users'] = IssuetoUsers::where('issueto_id', '=', $id)->first();
            
            return view('superadmin::quality_checklist.show_quality_checklist',compact('data','page_title','page_action','viewPage','viewPage1'))->with($page_data);
           
        } else {
            return redirect('superadmin/quality-checklist');
        }
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Issueto $isueto,$id=0)
    {                  
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(StoreQualityChecklistPost $request)
    {
        
        $checklist_id = Input::get('checklist_id');
        $checklist = QualityChecklist::firstOrNew(array('id' => $checklist_id));
        $checklist->checklist_name = Input::get('checklist_name');
        
        $success = $checklist->save();

        if ($success) {
             
            $comp_chk_map = new CompanyChecklistMapping;

            $obj_checklist_comps = $comp_chk_map->select('company_id')->where('checklist_id', '=', $checklist_id)->where('is_deleted', '=', 0)->get();
            $checklist_comps = array();
            foreach($obj_checklist_comps as $comp) {
                $checklist_comps[] = $comp->company_id;
            }

            $obj_checklist_comps_del = $comp_chk_map->select("company_id")->where('checklist_id', '=', $checklist_id)->where('is_deleted', '=', 1)->get();
            $checklist_comps_del = array();
            foreach($obj_checklist_comps_del as $comp) {
                $checklist_comps_del[] = $comp->company_id;
            }
            $comps = Input::get('company');

            $delete_checklist_comps = array_diff($checklist_comps,$comps);
            
            $to_del_comps = $comp_chk_map->where('checklist_id', '=', $checklist_id)->whereIn('company_id', $delete_checklist_comps);
            if(!empty($to_del_comps)){
                $to_del_comps->update(array('is_deleted'=>1));
            }
            
            $add_checklist_comps = array_diff($comps,$checklist_comps);
            foreach($add_checklist_comps as $comp) {
                if(in_array($comp, $checklist_comps_del)) {
                    
                    $to_add_comps = $comp_chk_map->where('checklist_id', '=', $checklist_id)->where('company_id', '=', $comp);
                    if (!empty($to_add_comps)) {
                        $to_add_comps->update(array('is_deleted'=>0));
                    }
                } else {
                    $comp_chk_map->checklist_id = $checklist_id;
                    $comp_chk_map->company_id = $comp;
                    $comp_chk_map->save();
                }
            }
            //$user_details->user_id = $user->id; 
            return redirect("superadmin/quality-checklist-details/$checklist_id")->with('flash_alert_notice', 'Checklist details updated successfully.');        
            
        }else{
             return redirect('superadmin/quality-checklist');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(QualityChecklist $checklist, $id)
    {
       if (!empty($id)) {
            
            $cur_checklist = $checklist->where('id', $id);
            if (!empty($cur_checklist)) {
                $cur_checklist->update(array('is_deleted' => 1));
                
                $companyChecklistMapping = CompanyChecklistMapping::where('checklist_id','=',$id);
                if (!empty($companyChecklistMapping)) {
                    $companyChecklistMapping->update(array('is_deleted' => 1));
                }
                $checklistTask = QualityChecklistTask::where('checklist_id','=',$id);
                if(!empty($checklistTask)) {
                    $checklistTask->update(array('is_deleted' => 1));
                }
            }

        }
        return redirect('superadmin/quality-checklist')->with('flash_alert_notice', 'Checklist deleted successfully.');
    }

    /**
    * Check the specified checklist name is exist
    * And return as true and false
    */
    public function checklist_isexist()
    {
        $name = Input::get('checklist_name');
        $id = Input::get('id');
        if (isset($name) && !empty($name)) {
            if (!empty($id)) {
                $is_current_name = QualityChecklist::where('checklist_name',"=",$name)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if(isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->checklist_name == $name) {
                    echo "true";
                    die;
                }
            }            
            $is_name = QualityChecklist::where('checklist_name',"=",$name)->where('is_deleted',"=",0)->get();            
            if (isset($is_name[0]) && !empty($name[0])) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
        die;
    }

    /**
    * Check the specified checklist task is exist
    * And return as true and false
    */
    public function checklist_task_isexist()
    {
        $name = Input::get('chk_task_name');
        $id = Input::get('id');
        $checklist_id = Input::get('checklist_id');
        if (isset($name) && !empty($name)) {
            if (!empty($id)) {
                $is_current_name = QualityChecklistTask::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"=",$id)->where('checklist_id',"=",$checklist_id)->get();   
                if(isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $name) {
                    echo "true";
                    die;
                }
            }            
            $is_name = QualityChecklistTask::where('name',"=",$name)->where('is_deleted',"=",0)->where('checklist_id',"=",$checklist_id)->get();            
            if (isset($is_name[0]) && !empty($is_name[0])) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
        die;
    }

    /**
    * List view by Data table for checklist data
    */
    public function getChecklistData(){
        $checklist1 = QualityChecklist::select(['id', 'checklist_name'])->where('is_deleted', '=', 0)->get();        
        $checklist = Datatables::of($checklist1)->addColumn('action', function ($it) {
                return '<a href="quality-checklist-details/'.$it->id.'" class="btn btn-xs default"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="'.url('/').'/superadmin/delete-quality-checklist/'.$it->id.'" title="Delete" class="btn btn-xs default"  onclick="return confirm('."'Are you sure you want to delete this Checklist?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $checklist;die;        
    }

    /**
     * Store a newly created checklist task in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_checklist_task(StoreQualityChecklistTaskPost $request)
    {

        $checklist_id = Input::get('task_checklist_id');
        $checklistTask = new QualityChecklistTask;
        $checklistTask->name = Input::get('chk_task_name');
        $checklistTask->checklist_id = $checklist_id;
        $checklistTask->comment = Input::get('comment');
        $checklistTask->status = Input::get('status');
        $checklistTask->created_at = date('Y-m-d h:i:s');
        $checklistTask->created_by = Auth::user()->id;
        $insert_checklist_task = $checklistTask->save();
        
        if (!empty($checklistTask->id)) {
            
            return redirect("superadmin/quality-checklist-details/$checklist_id")->with('flash_alert_notice','checklist task created succesfully.');
        } else {
            return redirect('superadmin/quality-checklist');
        }
        
    }

    /**
    * Update the specified checklist task in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Company  $company
    * @return \Illuminate\Http\Response
    */
    public function update_checklist_task(StoreQualityChecklistTaskPost $request)
    {
        $checklist_id = Input::get('task_checklist_id');
        $task_id = Input::get('task_id');
        $checklistTask = QualityChecklistTask::firstOrNew(array('id' => $task_id));
        $checklistTask->name = Input::get('chk_task_name');
        $checklistTask->checklist_id = $checklist_id;
        $checklistTask->comment = Input::get('comment');
        $checklistTask->status = Input::get('status');
        //print_r($issuetoUser); die;
        
        $success = $checklistTask->save();

        if ($success) {   
             
            
            return redirect("superadmin/quality-checklist-details/$checklist_id")->with('flash_alert_notice','Checklist task created succesfully.');      
            
        } else {
            return redirect('superadmin/quality-checklist');
       }
    }

    /**
    * Remove the specified checklist task from storage.
    *
    * @param  \App\Company  $company
    * @return \Illuminate\Http\Response
    */
    public function destroy_checklist_task(QualityChecklistTask $checklistTask, $id)
    {
       if (!empty($id)) {
            
            $cur_task = $checklistTask->where('id', $id);
            if (!empty($cur_task)) {
                $cur_task->update(array('is_deleted' => 1));
                
            }

        }
        return redirect('superadmin/quality-checklist')->with('flash_alert_notice', 'Checklist task deleted successfully.');
    }

    /**
    * List view by Data table for check list task data
    */
    public function getChecklistTaskData($checklist_id)
    {
        $checklistTask1 = QualityChecklistTask::select(['id', 'name', 'status', 'comment'])->where('is_deleted', '=', 0)->where('checklist_id', '=', $checklist_id)->get();        
        $checklistTask = Datatables::of($checklistTask1)->addColumn('action', function ($itu) {
                return '<a href="javascript:;" class="btn btn-xs default" onclick="edit_checklist_task('.$itu->id.');"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="'.url('/').'/superadmin/delete-quality-checklist-task/'.$itu->id.'" title="Delete" class="btn btn-xs default"  onclick="return confirm('."'Are you sure you want to delete this Task?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $checklistTask;die;        
    }

    /**
    * Get the specified checklist task details from storage.
    *
    * @param  \App\Company  $company
    * @return \Illuminate\Http\Response
    */
    public function get_checklist_task_details()
    {   
        $id = Input::get('id');
        if (!empty($id)) {
            $checklistTask = QualityChecklistTask::select(['id', 'checklist_id', 'name', 'status', 'comment'])->where('is_deleted', '=', 0)->where('id', '=', $id)->get();
        }
        echo $checklistTask;
        exit();
    }
}
