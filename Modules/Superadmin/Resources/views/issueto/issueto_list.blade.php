@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper">
		<div class="page-content">
			
			@include('superadmin::partials.breadcrumb')

			<h3 class="page-title">
			<!--Add User --> <!--<small>reports & statistics</small>-->
			</h3>
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue-hoki">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Issueto List
							</div>
							<div class="tools"></div>
						</div>
						<div class="portlet-body">
							@if(Session::has('flash_alert_notice'))
		                        <div class="alert alert-success alert-dismissable">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           	{{ Session::get('flash_alert_notice') }} 
		                        </div>
	                       	@endif
							
							<div class="table-toolbar">
								<div class="edit_btn">
									<div class="row">
										<div class="col-md-12 text-right">
											<div class="row">
												<div class="col-md-12">
													<a class="btn green" id="" href="javascript:;" onclick="show_issueto_form();"> <i class="fa fa-plus"></i> Add New </a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<!-- BEGIN FORM-->
										<form action="{{ url('superadmin/save_issueto') }}" id="issueto_form" class="horizontal-form" method="post" style="display:none;">
											
											<div class="portlet light">
												<div class="portlet-title">
													<div class="caption">
														<i class="icon-equalizer font-blue-hoki"></i>
														<span class="caption-subject font-blue-hoki bold uppercase">Add Issueto</span>
														<span class="caption-helper"></span>
													</div>
												</div>

												{{ csrf_field() }}
												@include('superadmin::issueto.issueto_form')
											</div>
											

										</form>
										<!-- END FORM-->
									</div>
								</div>

							</div>
							<div class="row">
							<div class="col-md-12">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th> Name </th>
										<th> Description </th>
										<th> Action </th>
										
									</tr>
								</thead>														

							</table>
							</div></div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>

		</div>
	</div>
	
<!-- END CONTAINER -->
@stop