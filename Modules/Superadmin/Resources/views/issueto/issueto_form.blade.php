
<div class="form-body">
	<!--<h3 class="form-section">Person Info</h3>-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group col-md-12">
				<label class="control-label">Name <span class="red">*</span></label>
				
				<?php $name = (isset($issueto->name)) ? $issueto->name : ''; ?>
				{{ Form::text('name', $name, $attributes = array('class'=>'form-control', 'id' => 'name', )) }} 
				@if ($errors->has('name'))
				    <span class=error>{{ $errors->first('name') }}</span>
				@endif
				<!--<span class="help-block">
				This is inline help </span>-->
			</div>

			<div class="form-group col-md-12">
				<label class="control-label">Description <span class="red">*</span></label>
				
				<?php $descr = (isset($issueto->description)) ? $issueto->description : ''; ?>
				{{ Form::textarea('description', $descr, $attributes = array('class'=>'form-control', 'id' => 'description', 'rows' => '3' )) }} 
				@if ($errors->has('description'))
				    <span class=error>{{ $errors->first('description') }}</span>
				@endif
				<!--<span class="help-block">
				This field has error. </span>-->


			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group col-md-12">
				<label class="control-label">Company <span class="red">*</span></label>
				<select name="company[]" id="company" class="select2_category form-control" data-placeholder="Choose Companies" tabindex="1" multiple>
					<option value="">Select Atleast One</option>
					
					@foreach($obj_companies as $company)
						<option value="{{$company->id}}" "@if(in_array($company->id, $issueto_comps)) selected @endif" >{{$company->name}}</option>
					@endforeach
					
				</select>
				@if ($errors->has('company'))
				    <span class=error>{{ $errors->first('company') }}</span>
				@endif
			</div>

			<div class="form-group col-md-12">
				<!-- <div class="form-actions"> -->
					<a href="javascript:;"><button type="reset" class="btn default" onClick="cancel_issueto();"><i class="fa fa-close"></i> Cancel</button></a>
					<button type="submit" class="btn green"><i class="fa fa-check"></i> Save</button>
				<!-- </div> -->
			</div>
		</div>
		<!--/span-->
	</div>
	

</div>


