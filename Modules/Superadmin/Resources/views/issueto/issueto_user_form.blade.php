
<div class="form-body">
	<!--<h3 class="form-section">Person Info</h3>-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group col-md-12">
				<label class="control-label">Name <span class="red">*</span></label>
				{{ Form::text('name', '', $attributes = array('class'=>'form-control', 'id' => 'iu_name', )) }} 
				@if ($errors->has('name'))
				    <span class=error>{{ $errors->first('name') }}</span>
				@endif
				<!--<span class="help-block">
				This is inline help </span>-->
			</div>
		</div>
		<div class="col-md-6">			
			<div class="form-group col-md-12">
				<label class="control-label">Email <span class="red">*</span></label>
				{{ Form::email('email', '', $attributes = array('class'=>'form-control', 'id' => 'email', )) }}
				@if ($errors->has('email'))
				    <span class=error>{{ $errors->first('email') }}</span>
				@endif
				<!--<span class="help-block">
				This field has error. </span>-->
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">			
			<div class="form-group col-md-12">
				<label class="control-label">Activity <span class="red">*</span></label>
				<!-- {{ Form::textarea('activity', '', $attributes = array('class'=>'form-control', 'id' => 'activity', 'rows' => '3' )) }}
				@if ($errors->has('activity'))
				    <span class=error>{{ $errors->first('activity') }}</span>
				@endif -->

				{{ Form::select('activity', $option_activity, null,  ['class' => 'select2_category form-control','id'=>'activity']) }}
				@if ($errors->has('activity'))
				    <span class="error">{{ $errors->first('activity') }}</span>
				@endif
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group col-md-12">
				<label class="control-label">Phone <span class="red">*</span></label>
				{{ Form::text('phone', '', $attributes = array('class'=>'form-control', 'id' => 'phone', )) }}
				@if ($errors->has('phone'))
				    <span class=error>{{ $errors->first('phone') }}</span>
				@endif
				<!--<span class="help-block">
				This is inline help </span>-->
			</div>
		</div>
		
	</div>

	<div class="row">
		
		<div class="form-group col-md-12 text-right">
			<!-- <div class="form-actions"> -->
				<a href="javascript:;"><button type="reset" class="btn default cancel_reset" onClick="cancel_issueto_user();"><i class="fa fa-close"></i> Cancel</button></a>
				<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
			<!-- </div> -->
		</div>
		
	</div>
	

</div>


