
<div class="form-body">
	<!--<h3 class="form-section">Person Info</h3>-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group col-md-12">
				<label class="control-label">Name <span class="red">*</span></label>
				{{ Form::text('chk_task_name', '', $attributes = array('class'=>'form-control', 'id' => 'chk_task_name', )) }} 
				@if ($errors->has('chk_task_name'))
				    <span class=error>{{ $errors->first('chk_task_name') }}</span>
				@endif
				<!--<span class="help-block">
				This is inline help </span>-->
			</div>

			<div class="form-group col-md-12">
				<label class="control-label">Status <span class="red"></span></label>
				<!-- {{ Form::text('status', '', $attributes = array('class'=>'form-control', 'id' => 'status', )) }} -->
					<input type="radio" name="status" id="Yes" value="Yes"><label for="Yes">Yes</label>
				  
					<input type="radio" name="status" id="No" value="No"><label for="No">No</label>
				  
				  	<input type="radio" name="status" id="NA" value="NA"><label for="NA">NA</label><br>
				@if ($errors->has('status'))
				    <span class=error>{{ $errors->first('status') }}</span>
				@endif
				<!--<span class="help-block">
				This field has error. </span>-->
			</div>
		</div>
	
		<div class="col-md-6">			
			<div class="form-group col-md-12">
				<label class="control-label">Comment </label>
				{{ Form::textarea('comment', '', $attributes = array('class'=>'form-control', 'id' => 'comment', 'rows' => '3' )) }}
				@if ($errors->has('comment'))
				    <span class=error>{{ $errors->first('comment') }}</span>
				@endif
				<!--<span class="help-block">
				This field has error. </span>-->
			</div>
			
		</div>
		
	</div>

	<div class="row">
		<div class="col-md-12  text-right">
			<a href="javascript:;"><button type="reset" class="btn default cancel_reset" onClick="cancel_checklist_task();"><i class="fa fa-close"></i> Cancel</button></a>
			<button type="submit" class="btn green"><i class="fa fa-check"></i> Save</button>
		</div>
	</div>

</div>


