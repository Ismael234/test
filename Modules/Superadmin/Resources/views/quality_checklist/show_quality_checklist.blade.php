@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			@include('superadmin::partials.breadcrumb')
			<!-- {!! $breadcrumbs !!} -->
			<h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>--> </h3>
			<!-- END PAGE HEADER-->
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Quality Checklist Details
							</div>
						</div>
						<div class="portlet-body form">
							@if(Session::has('flash_alert_notice'))
		                        <div class="alert alert-success alert-dismissable">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           	{{ Session::get('flash_alert_notice') }} 
		                        </div>
	                       	@endif
							<div class="edit_btn">
								<div class="row">
									<div class="col-md-12 text-right">
										<div class="row">
											<div class="col-md-12">
												<!-- <a href="#"><button type="button" onClick="history.go(-1); return false;" class="btn default"><i class="fa fa-chevron-left "></i> Back</button></a> -->
												<a href="javascript:;" id="" onclick="show_checklist_form();" class="btn green"><i class="fa fa-pencil"></i> Edit</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-horizontal">
								<div class="form-body">	
									<!-- BEGIN FORM-->
									<form action="{{ url('superadmin/update_quality_checklist') }}" id="checklist_form" class="horizontal-form" method="post" style="display:none;">
										<div class="portlet light">
											<div class="portlet-title">
												<div class="caption">
													<i class="icon-equalizer font-blue-hoki"></i>
													<span class="caption-subject font-blue-hoki bold uppercase">Edit Checklist Details</span>
													<span class="caption-helper"></span>
												</div>
											</div>
											{{ csrf_field() }}
											@include('superadmin::quality_checklist.quality_checklist_form')
										</div>
										<input type="hidden" name="checklist_id" id="checklist_id" value="{{$checklist_id}}" />
									</form>
									<!-- END FORM-->	
									<h3 class="form-section">Checklist Info</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"><strong>Name :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$checklist->checklist_name}}
													</p>
												</div>
												<label class="control-label col-md-3"><strong>Company :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														@foreach($checklist_comps as $comp)
															{{$companies[$comp]}}<br>
														@endforeach
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="edit_btn">
								<div class="row">
									<div class="col-md-12 text-right">
										<div class="row">
											<div class="col-md-12">
												<!-- <a href="#"><button type="button" onClick="history.go(-1); return false;" class="btn default"><i class="fa fa-chevron-left "></i> Back</button></a> -->
												<a href="javascript:;" id="" onclick="show_checklist_task_form();" class="btn green"><i class="fa fa-plus"></i> Add New Task</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-horizontal">
								<div class="form-body">	
									<h3 class="form-section">Checklist Tasks</h3>
									<!-- BEGIN FORM-->
									<form action="{{ url('superadmin/save_checklist_task') }}" id="checklist_task_form" class="horizontal-form" method="post" style="display:none;">

										<div class="portlet light">
											<div class="portlet-title">
												<div class="caption">
													<i class="icon-equalizer font-blue-hoki"></i>
													<span class="caption-subject font-blue-hoki bold uppercase">Add Task</span>
													<span class="caption-helper"></span>
												</div>
											</div>
											{{ csrf_field() }}
											@include('superadmin::quality_checklist.quality_checklist_task_form')
										</div>
										<input type="hidden" name="task_checklist_id" id="task_checklist_id" value="{{$checklist_id}}" />
										<input type="hidden" name="task_id" id="task_id" value="" />
									</form>
									<!-- END FORM-->
									<table class="table table-striped table-bordered table-hover" id="sample_2">
										<thead>
											<tr>
												<th> Name </th>
												<th> Status </th>
												<th> Comment </th>
												<th> Action </th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
	
<!-- END CONTAINER -->
@stop