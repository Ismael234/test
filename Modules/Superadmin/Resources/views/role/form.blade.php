<div class="form-group">
	<label class="control-label">Role Name <span class="red">*</span></label>
	@if(!empty($role->name))
		{{Form::text('name',$role->name,array('class'=>'form-control','id'=>'name','ng-model'=>'name','required'=>'required','ng-change'=>'check_role()'))}}
		<span style="color:red" ng-show="role.name.$dirty && role.name.$invalid">
		<span ng-show="role.name.$error.required">Role Name is required.</span>
		</span>

		<div style="color:red;display: none;" id="error_msg" role="alert">
		    <div ng-message="required">Role Name already exist.</div>		    
	  	</div>
	@else
		{{Form::text('name','',array('class'=>'form-control','id'=>'name','ng-model'=>'name','required'=>'required','ng-change'=>'check_role()'))}}
		<span style="color:red" ng-show="role.name.$dirty && role.name.$invalid">
		<span ng-show="role.name.$error.required">Role Name is required.</span>
		</span>
	@endif
</div>
  <div id="treeview-checkbox-demo">							            
        <ul>
        	<li data-value="selected">Permission:-
        		<ul>
	            @foreach($data_tree as $key => $tree)
	            	<li  data-value="{{$key}}">
	            		{{$tree['parent']}}
	            		@if(!empty($tree['child']))
	            		<ul>
	            			@foreach($tree['child'] as $key => $tree_child)
	            				<li data-value="{{$key}}">{{$tree_child}}</li>
	            			@endforeach
	            		</ul>
	            		@endif
	            	</li>
	            @endforeach
        	</ul>
        	</li>
        </ul>
    </div>
    