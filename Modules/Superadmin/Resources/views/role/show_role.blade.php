@extends('superadmin::layouts.master')

<!-- BEGIN CONTAINER -->
	<!-- BEGIN SIDEBAR -->
	@section('content')

	<!-- END SIDEBAR -->

	<div class="page-content-wrapper" ng-app="roleModule">
		<div class="page-content" ng-controller="roleController">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		
			<!-- BEGIN PAGE HEADER-->			
			@include('superadmin::partials.breadcrumb')
			
			<h3 class="page-title">
			<!--Add User --> <!--<small>reports & statistics</small>-->
			</h3>
			<!-- END PAGE HEADER-->
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="tab-pane active" id="tab_3">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Authentication Details
							</div>
							<!-- <div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
								</a>
								<a href="javascript:;" class="reload" data-original-title="" title="">
								</a>											
							</div> -->
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->

							<div class="edit_btn">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">						
													<!-- <a href="#"><button type="button" class="btn default" onclick="history.go(-1); return false;"><i class="fa fa-chevron-left "></i> Back</button></a> -->
													<a href="javascript:;" ng-click="toggle('edit',{{$role->id}},'{{$role->name}}')" class="btn green"><i class="fa fa-pencil"></i> Edit</a>
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>

							<form class="form-horizontal" role="form">
								<div class="form-body">									
									<h3 class="form-section"><strong>{{$role->name}}</strong></h3>
									@if(!empty($parent))
									@foreach($parent as $key=>$p)
										<div class="list-group">
											<a href="javascript:;" class="list-group-item active">
											<h4 class="list-group-item-heading">{{$p}}</h4>
											<p class="list-group-item-text">
												 
											</p>
											</a>
											@php
												$sub=array();
												$int_p=array();
												$sub_key = '';
												$int_key = '';
											@endphp
											@if(!empty($child[$key]) && isset($child[$key]))
												@foreach($child[$key] as $key1=>$c)
														@if($type[$key1] == "Submenu")
															@php $sub_key = $type[$key1] @endphp
															@php $sub[] =$c @endphp
														@else
															@php $int_key = $type[$key1] @endphp
															@php $int_p[] =$c @endphp
														@endif
													@endforeach		
												@if($sub_key == "Submenu" && !empty($sub))
												<a href="javascript:;" class="list-group-item">
												<h4 class="list-group-item-heading"><strong>Submenu:-</strong>		
													{{implode(",",$sub)}}
												</h4>
												</a>
												@endif
												@if($int_key == "Internal permission" && !empty($int_p))
												<a href="javascript:;" class="list-group-item">
												<h4 class="list-group-item-heading"><strong>Internal permission:-</strong>	
													{{implode(",",$int_p)}}
												</h4>
												</a>
												@endif
											@endif								
										</div>
									@endforeach
									@endif
								</div>								
							</form>
							<!-- END FORM-->
							<!-- Confirmation Dialog -->
		    <div class="modal" id="showModal" close="cancel()" style="display:none;">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-header">
		          	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		            <h4 class="modal-title">Role Details</h4>
		          </div>
		          <div class="modal-body">
		          	<!-- BEGIN FORM-->
	          		<div id="perm_hidevalue" style="display: none;">{{json_encode($allPermission)}}</div>
					<form method="GET" action="{{url('/')}}" accept-charset="UTF-8" name="role" id="role" novalidate="novalidate">
						{{ csrf_field() }}
						@include('superadmin::role.form')   
						<input type="hidden" id="hid" name="hid" value="{{$role->id}}">
							</div>
				          <div class="modal-footer">
				            <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
				            <button type="button" ng-disabled="role.name.$dirty && role.name.$invalid" class="btn btn-primary angfrm" ng-click="save({{$role->id}})">Save</button>
				          </div>        	
	            	</form>
					<!-- END FORM-->
		          
		        </div>
		      </div>
		    </div>
    	<!-- End of Confirmation Dialog -->
						</div>
					</div>																
				</div>
			</div>
		</div>

		
	</div>
	<!-- END CONTENT -->	
<!-- END CONTAINER -->
@stop