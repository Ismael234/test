<div class="modal fade" id="detailsModal" close="cancel()" style="display:none;">
	<!-- <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> -->
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- BEGIN FORM-->
			<form id="add_project_form" name="projectInfo" class="horizontal-form" ng-submit="update_details({{$project->id}},'details')" novalidate>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Project Details</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" id="project_id" name="project_id" value="@if(isset($project->id)){{$project->id}}@endif" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Project Name <span class="red">*</span></label>
									<input type="text" id="name" class="form-control" name="name" placeholder="" ng-model="project.name" ng-required="true"/>
									<span class="error" ng-show="projectInfo.name.$invalid && projectInfo.name.$touched">project name is required</span>
									<!--<span class="help-block">
									This is inline help </span>-->
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Project Code <span class="red">*</span></label>
									<input type="text" id="code" name="code" class="form-control" placeholder="" ng-model="project.code" ng-required="true"/>
									<span class="error" ng-show="projectInfo.code.$invalid && projectInfo.code.$touched">project code is required</span>
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Project Type <span class="red">*</span></label>
									@if(!empty($project))
										{{ Form::select('type', $option_type, $project->type,  ['class' => 'select2_category form-control','id'=>'type']) }}
									@else
										{{ Form::select('type', $option_type, null,  ['class' => 'select2_category form-control','id'=>'type']) }}
									@endif
									<span class="error" ng-show="project.type.$invalid && project.type.$touched">
								        <span ng-show="projectInfo.email.$error.required">Project type is required.</span>
								    </span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Description <span class="red"></span></label>
									<textarea id="description" name="description" class="form-control" placeholder="" ng-model="project.description" ng-required="true" rows="4"/></textarea>
									<!-- <span class="error" ng-show="userInfo.description.$invalid && userInfo.description.$touched">password is required</span> -->
								</div>
							</div>
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn blue" id="profile_form" >Update</button>
				</div>
			</form>
			<!-- END FORM-->
	    </div>
	</div>
</div>