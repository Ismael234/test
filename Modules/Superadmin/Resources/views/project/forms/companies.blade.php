<div class="modal fade" id="companiesModal" close="cancel()" style="display:none;">
	<!-- <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> -->
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- BEGIN FORM-->
			<form id="projectCompany" name="projectCompany" class="horizontal-form" ng-submit="update_details({{$project->id}},'companies')" novalidate>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Associated Companies</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" id="project_id" name="project_id" value="@if(isset($project->id)){{$project->id}}@endif" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Company <span class="red">*</span></label>
									@php $proj_comps = array($proj_comps) @endphp
									<select name="company[]" id="company" class="select2_category form-control" data-placeholder="Choose Companies" tabindex="1" multiple>
										<option value="" disabled>Select Atleast One</option>
										
										@foreach($companies as $company)
											<option value="{{$company->id}}" @if(in_array($company->id, $project_companies)) selected @endif >{{$company->name}}</option>
										@endforeach
										
									</select>
									@if ($errors->has('company'))
									    <span class=error>{{ $errors->first('company') }}</span>
									@endif
								</div>
							</div>
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel('projectCompany')">Cancel</button>
					<button type="submit" class="btn blue" id="companies_form">Update</button>
				</div>
			</form>
			<!-- END FORM-->
	    </div>
	</div>
</div>