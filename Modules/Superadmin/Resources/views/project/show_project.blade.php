
@extends('superadmin::layouts.master')

@section('content')

	<!-- BEGIN CONTAINER -->
	<div class="page-content-wrapper"  ng-app="projectModule">
		<div class="page-content" ng-controller="projectController">
			<!-- BEGIN PAGE HEADER-->
			@include('superadmin::partials.breadcrumb')
			<!-- {!! $breadcrumbs !!} -->
			<h3 class="page-title">	 </h3>
			<!-- END PAGE HEADER-->
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i> Project Details
						</div>
					</div>
					<div class="portlet-body form">
						<div class="alert alert-success" style="display:none;">
							<strong>Success!</strong> Project info has been updated.
						</div>
						<div class="edit_btn">
							<div class="row">
								<div class="col-md-12 text-right">
									<div class="row">
										<div class="col-md-12">
											<!-- <a href="#"><button type="button" onClick="history.go(-1); return false;" class="btn default"><i class="fa fa-chevron-left "></i> Back</button></a> -->
											<button ng-click="show_details({{$project->id}}, 'details')" class="btn green"><i class="fa fa-pencil"></i> Update Details </button>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
						@include('superadmin::project.forms.details') <!-- Update Info Form Modal -->
						<!-- BEGIN FORM-->
						<div class="form-horizontal">
							<div class="form-body">
								<h3 class="form-section">Project Info</h3>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Project Name :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 {{$project->name}}
												</p>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Project Code :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 {{$project->code}}
												</p>
											</div>
										</div>
									</div>
								</div>
								<!--/row-->
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Project Type :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 {{$project->type}}
												</p>
											</div>

										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Description :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 {{$project->description}}
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="edit_btn">
							<div class="row">
								<div class="col-md-12 text-right">
									<div class="row">
										<div class="col-md-12">
											<!-- <a href="#"><button type="button" onClick="history.go(-1); return false;" class="btn default"><i class="fa fa-chevron-left "></i> Back</button></a> -->
											<button ng-click="show_details({{$project->id}}, 'address')" class="btn green"><i class="fa fa-pencil"></i> Update Address </button>
										</div>
									</div>
								</div>
							</div>
						</div>
						@include('superadmin::project.forms.address') <!-- Update Address Form Modal -->
						<div class="form-horizontal">
							<div class="form-body">
								<h3 class="form-section">Project Address</h3>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Address 1 :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 {{$project->address1}}
												</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Address 2 :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 {{$project->address2}}
												</p>
											</div>
										</div>
									</div>
								</div>
								<!--/row-->
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Suburb :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 {{$project->suburb}}
												</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Postal Code :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													{{$project->postal_code}}
												</p>
											</div>
										</div>
									</div>
								</div>
								<!--/row-->
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>State :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													{{$project->state}}
												</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Country :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													{{$project->country}}
												</p>
											</div>
										</div>
									</div>
								</div>
								<!--/row-->
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>GPS Coordinates :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 {{$project->geo_code}}
												</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Logo :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													@if(!empty($project->logo))
													@php $logo_image = $project->logo; @endphp
												    	<img class="logo_img" src="{{url('/')}}/uploads/project/{{$logo_image}}">
												    @endif
												</p>
											</div>
										</div>
									</div>
								</div>
								<!--/row-->
							</div>
						</div>
						<!-- END FORM-->
						<div class="edit_btn">
							<div class="row">
								<div class="col-md-12 text-right">
									<div class="row">
										<div class="col-md-12">
											<button ng-click="show_details({{$project->id}}, 'companies')" class="btn green"><i class="fa fa-plus"></i></i> Add More Companies </button>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
						@include('superadmin::project.forms.companies') <!-- Update Compan Form Modal -->
						<div class="form-horizontal" >
							<div class="form-body">
								<h3 class="form-section">Associated Companies</h3>									
								<table class="table table-striped table-bordered table-hover" id="assoc_comp_list">
									<thead>
										<tr>
											<th> Company Name </th>
											<th> Company Type </th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
	<!-- END CONTAINER -->
@stop