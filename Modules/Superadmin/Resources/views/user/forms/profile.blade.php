<div class="modal fade" id="profileModal" close="cancel()"> 
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="userInfo" name="userInfo" class="horizontal-form" ng-submit="submitForm({{$user->id}},'profile')" novalidate>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">User Personal Details</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" id="user_id" name="user_id" value="@if(isset($user->id)){{$user->id}}@endif" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Username <span class="red">*</span></label>
									<input type="text" id="username" class="form-control" name="username" placeholder="" ng-model="user.username" ng-readonly="true" />
									<span class="error" ng-show="userInfo.username.$invalid && userInfo.username.$touched">username is required</span>
									<!--<span class="help-block">
									This is inline help </span>-->
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Full Name <span class="red">*</span></label>
									<input type="text" id="fullname" name="fullname" class="form-control" placeholder="" ng-model="user.fullname" ng-required="true"/>
									<span class="error" ng-show="userInfo.fullname.$invalid && userInfo.fullname.$touched">fullname is required</span>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Email id <span class="red">*</span></label>
									<input type="email" id="email" name="email" class="form-control" placeholder="" ng-model="user.email" ng-change="check_company_email(@if(isset($user->id)){{$user->id}}@endif)"ng-required="true"/>
									<span class="error" ng-show="userInfo.email.$dirty && userInfo.email.$invalid">
								        <span ng-show="userInfo.email.$error.required">Email is required.</span>
								        <span ng-show="userInfo.email.$error.email">Invalid email address.</span>
								    </span>
								    <div style="color:red;display:none;" id="error_msg_email" role="alert">
									    <div ng-message="required">Email already exist.</div>		    
								  	</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Password <span class="red">*</span></label>
									<input type="text" id="password" name="password" class="form-control" ng-model="user.password" ng-minlength="6" required />
									<span class="error" ng-show="userInfo.password.$error.required">Password is required.</span>
								    <span class="error" ng-show="userInfo.password.$error.minlength">Password should be minimum 6 charactors.
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Phone <span class="red">*</span></label>
									<input type="number" id="phone" name="phone" class="form-control" placeholder="" ng-model="user.phone"  ng-minlength="8" ng-maxlength="15" required/>
									<span class="error" ng-show="userInfo.phone.$error.required">Number is required.</span>
									<span class="error" ng-show="userInfo.phone.$error.number">Please enter only numeric value.</span>
								    <span class="error" ng-show="userInfo.phone.$error.minlength || userInfo.phone.$error.maxlength">Number must be in b/w 8 to 15.</span>
								    
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">User Type <span class="red">*</span></label>

									@if(!empty($user->role_id))						
										{{ Form::select('usertype', (object)$roles, $user->role_id,  ['class' => 'select2_category form-control','id'=>'usertype']) }}
									@else
										{{ Form::select('usertype', $roles, null,  ['class' => 'select2_category form-control','id'=>'usertype']) }}
									@endif
									@if ($errors->has('user_type'))
									    <span class="error">{{ $errors->first('user_type') }}</span>
									@endif
								</div>
							</div> 
							<!--/span-->
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn btn-primary" id="profile_form" ng-disabled="userInfo.$invalid">Update</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
