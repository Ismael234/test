<div class="modal fade" id="addressModal" close="cancel()">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="addressInfo" name="addressInfo" class="horizontal-form" ng-submit="submitForm({{$user->id}},'address')">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">User Personal Details</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" id="user_id" name="user_id" value="@if(isset($user->id)){{$user->id}}@endif" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Address 1</label>
								<input type="text" id="busi_address1" name="busi_address1" class="form-control" placeholder="" ng-model="user.busi_address1">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Address 2</label>
								<input type="text" id="busi_address2" name="busi_address2" class="form-control" placeholder="" ng-model="user.busi_address2">
								</div>
							</div>
							<!--/span-->
						</div>

						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Suburb</label>
								<input type="text" id="busi_suburb" name="busi_suburb" class="form-control" placeholder="" ng-model="user.busi_suburb">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business State</label>
								@if(!empty($udetail[0]))						
									{{ Form::select('bus_state', $option_state, $udetail[0]->busi_state,  ['class' => 'select2_category form-control','id'=>'bus_state']) }}
								@else
									{{ Form::select('bus_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'bus_state']) }}
								@endif
								
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Post</label>
								<input type="text" id="busi_post" name="busi_post" class="form-control" placeholder="" ng-model="user.busi_post">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Country</label>
								<input type="text" id="busi_country" name="busi_country" class="form-control" placeholder="" ng-model="user.busi_country">
								</div>
							</div>
							<!--/span-->
						</div>

						<!-- <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label"><input name="chck1" id="chck1" class="chck1" type="checkbox"> Business Address is same as billing user.</label>
								</div>
							</div>
						</div> -->

						<!--/row for billing-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Address 1</label>
								<input type="text" id="bill_address1" name="bill_address1" class="form-control" placeholder="" ng-model="user.bill_address1">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Address 2</label>
								<input type="text" id="bill_address2" name="bill_address2" class="form-control" placeholder="" ng-model="user.bill_address2">
								</div>
							</div>
							<!--/span-->
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Suburb</label>
								<input type="text" id="bill_suburb" name="bill_suburb" class="form-control" placeholder="" ng-model="user.bill_suburb">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing State</label>
								@if(!empty($udetail[0]))
									{{ Form::select('buld_state', $option_state, $udetail[0]->bill_state,  ['class' => 'select2_category form-control','id'=>'buld_state']) }}
								@else
									{{ Form::select('buld_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'buld_state']) }}
								@endif
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Post</label>
								<input type="text" id="bill_post" name="bill_post" class="form-control" placeholder="" ng-model="user.bill_post">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Country</label>
								<input type="text" id="bill_country" name="bill_country" class="form-control" placeholder="" ng-model="user.bill_country">
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn btn-primary" id="address_form">Update</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
