@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper" ng-app="adminModule">
		<div class="page-content" ng-controller="superadminController">
			<!-- BEGIN PAGE HEADER-->
			@include('superadmin::partials.breadcrumb')
			<!-- {!! $breadcrumbs !!} -->
			<h3 class="page-title">	 </h3>
			<!-- END PAGE HEADER-->

			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Superadmin Details
						</div>
						
					</div>
					<div class="portlet-body form">
						@if(Session::has('flash_alert_notice'))
	                        <div class="alert alert-success alert-dismissable">
	                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                          	<i class="icon fa fa-check"></i>  
	                           	{{ Session::get('flash_alert_notice') }} 
	                        </div>
	                   	@endif
						<div class="edit_btn">
							<div class="row">
								<div class="col-md-12 text-right">
									<div class="row">
										<div class="col-md-12">
											<button class="btn green" ng-click="showModal({{$admin->id}})"><i class="fa fa-pencil"></i> Update Profile</button>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
						<!-- Admin Profile Dialog -->
						@include('superadmin::user.forms.admin-profile')
						<!-- BEGIN FORM-->
						<div class="form-horizontal" role="form">
							<div class="form-body">									
								<h3 class="form-section">Superadmin Info</h3>
								<div class="row">
									<div class="col-md-6 text-center">
										<form id="adminLogoFrm" runat="server">
										<input type="hidden" name="username" value="@if(isset($admin->name)){{$admin->name}}@endif" />
											<div class="form-group">
												<span class="btn green fileinput-button">
								                    <i class="glyphicon glyphicon-plus"></i>

								                    @if(!empty($udetail[0]) && !empty($udetail[0]->logo_image))
								                    	<span>Change Photo</span>
								                    @else
								                    	<span>Add Photo</span>
								                    @endif
													 	<input type='file' name="logo" id="userImg"/>
												</span><br><br>
												@if(!empty($udetail[0]) && !empty($udetail[0]->logo_image))
													<img id="logo_image"  width="200" height="200"  src="{{url('/')}}/uploads/user/{{$udetail[0]->logo_image}}" alt="your image" />
												@else
													<img id="logo_image"  width="200" height="200"  src="{{url('/')}}/uploads/user_icon.jpg" alt="user image" />
												@endif
											</div>	
											<p class="form-control-static">
												<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success start" ng-click="submitForm({{$admin->id}},'logo')">
								                    <i class="glyphicon glyphicon-upload"></i>
								                    <span>Start upload</span>
								                </button>
								                <span ng-show="adminLoader">
								                	<img src="{{url('/')}}/assets/img/ajax-loader.gif">
								                </span>
											</p>
										</form>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Username :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$admin->name}}
													</p>
												</div>
											</div>
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Password :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$admin->plain_password}}
													</p>
												</div>
											</div>
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Name :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$admin->full_name}}
													</p>
												</div>
											</div>
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Email :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$admin->email}}
													</p>
												</div>
											</div>
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Mobile :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$admin->phone}}
													</p>
												</div>
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
							</div>							
						</div>
					</div>
				</div>																
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
<!-- END CONTAINER -->
@stop