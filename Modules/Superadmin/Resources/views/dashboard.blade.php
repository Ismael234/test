@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		
		@include('superadmin::partials.breadcrumb')
		<h3 class="page-title">
		Dashboard <small>company & user statistics</small>
		</h3>
		<!-- END PAGE HEADER-->
		<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual">
						<i class="fa fa-comments"></i>
					</div>
					<div class="details">
						<div class="number">
							 {{$total_companies}}
						</div>
						<div class="desc">
							 Companies
						</div>
					</div>
					<a class="more" href="{{url('superadmin/company')}}">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat red-intense">
					<div class="visual">
						<i class="fa fa-bar-chart-o"></i>
					</div>
					<div class="details">
						<div class="number">
							 {{$total_users}}
						</div>
						<div class="desc">
							 Users
						</div>
					</div>
					<a class="more" href="{{url('superadmin/user')}}">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat green-haze">
					<div class="visual">
						<i class="fa fa-shopping-cart"></i>
					</div>
					<div class="details">
						<div class="number">
							 {{$project}}
						</div>
						<div class="desc">
							 Projects
						</div>
					</div>
					<a class="more" href="{{url('superadmin/project')}}">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat purple-plum">
					<div class="visual">
						<i class="fa fa-globe"></i>
					</div>
					<div class="details">
						<div class="number">
							 {{$total_issueto}}
						</div>
						<div class="desc">
							 Issue To
						</div>
					</div>
					<a class="more" href="{{url('superadmin/issueto')}}">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
		</div>
		<!-- END DASHBOARD STATS -->
		<div class="clearfix">
		</div>
	</div>
</div>
<!-- END CONTENT -->
@stop