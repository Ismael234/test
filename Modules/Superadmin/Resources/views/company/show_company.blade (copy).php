@extends('superadmin::layouts.master')

<!-- BEGIN CONTAINER -->
	<!-- BEGIN SIDEBAR -->
	@section('content')

	<!-- END SIDEBAR -->

	<div class="page-content-wrapper" ng-app="companyModule">
		<div class="page-content" ng-controller="companyController">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		
			<!-- BEGIN PAGE HEADER-->			
			@include('superadmin::partials.breadcrumb')
			
			<h3 class="page-title">
			<!--Add User --> <!--<small>reports & statistics</small>-->
			</h3>
			<!-- END PAGE HEADER-->
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="tab-pane active" id="tab_3">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Show Company Details
							</div>
							<!-- <div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
								</a>
								<a href="javascript:;" class="reload" data-original-title="" title="">
								</a>											
							</div> -->
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->

							<div class="edit_btn">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">						
													<!-- <a href="#"><button type="button" class="btn default" onclick="history.go(-1); return false;"><i class="fa fa-chevron-left "></i> Back</button></a> -->
													<!-- <a href="/superadmin/edit-company-details/{{ $cmp->id }}" class="btn green"><i class="fa fa-pencil"></i> Edit</a> -->
													<a class="btn green" ng-click="showModelFrm({{$cmp->id}}, 'company')"><i class="fa fa-pencil"></i> Update Profile </a>
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>
							

								<!-- Address Dialog -->
				    
						@include('superadmin::company.forms.address')
				    
	    		<!-- End of Address Dialog -->

							<form class="form-horizontal" role="form">
								<div class="form-body">									
									<h3 class="form-section">Company Info</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Company Name:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{ucfirst($cmp->name)}}
													</p>																										
													<input type="hidden" name="hid" id="hid" value="{{$cmp->id}}" />
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Company Type:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp->type}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Company Registration Number:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp->registration_number}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Email-id:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp->email}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Phone:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp->phone}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="edit_btn">
										<div class="row">
											<div class="col-md-12 text-right">
												<div class="row">
													<div class="col-md-12">
														<!-- <a href="#"><button type="button" onClick="history.go(-1); return false;" class="btn default"><i class="fa fa-chevron-left "></i> Back</button></a> -->
														<button class="btn green" ng-click="showModelFrm({{$cmp->id}},'address')"><i class="fa fa-pencil"></i> Update Address
													</button></div>
												</div>
											</div>
											<div class="col-md-6">
											</div>
										</div>
									</div>
									<h3 class="form-section">Business Details</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Address 1:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->busi_address1))
														 {{$cmp_details[0]->busi_address1}}
													@endif
													</p>
												</div>
											</div>
										</div>
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Address 2:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->busi_address2))
														 {{$cmp_details[0]->busi_address2}}
													@endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Suburb:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->busi_suburb))
														 {{$cmp_details[0]->busi_suburb}}
													@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business State:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->busi_state))
														{{$cmp_details[0]->busi_state}}
													@endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<!--/row-->
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Post:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->busi_post))
														{{$cmp_details[0]->busi_post}}
													@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Country:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->busi_country))
														{{$cmp_details[0]->busi_country}}
													@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--Billing addr-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Address 1:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->bill_address1))
														 {{$cmp_details[0]->bill_address1}}
													 @endif
													</p>
												</div>
											</div>
										</div>
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Address 2:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->bill_address2))
														 {{$cmp_details[0]->bill_address2}}
													 @endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Suburb:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->bill_suburb))
														 {{$cmp_details[0]->bill_suburb}}
													 @endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing State:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->bill_state))
														{{$cmp_details[0]->bill_state}}
													@endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<!--/row-->
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Post:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->bill_post))
														{{$cmp_details[0]->bill_post}}
													@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Country:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
													@if(!empty($cmp_details[0]->bill_country))
														{{$cmp_details[0]->bill_country}}
													@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<form id="form1" runat="server">
												<div class="form-group">
													<label class="control-label col-md-5"><strong>Logo:</strong></label>
													<div class="col-md-7">
													<span class="btn green fileinput-button">
									                    <i class="glyphicon glyphicon-plus"></i>
									                    @if(!empty($cmp_details[0]->logo_image))
									                    	<span>Change logo</span>
									                    @else
									                    	<span>Add logo</span>
									                    @endif
														 	<input type='file' name="logo" id="imgInp" ng-files="getTheFiles($files)"/>
    													</span><br><br>
    													@if(!empty($cmp_details[0]->logo_image))
    														<img id="logo_image" width="250" height="150" src="{{url('/')}}/uploads/company/thumbnail_250x150/{{$cmp_details[0]->logo_image}}" alt="your image" />
    													@else
    														<img id="logo_image" width="250" height="150" src="{{url('/')}}/uploads/company_icon.png" alt="your image" />
    													@endif
													</div>
												</div>						
											</form>
										</div>
										<!--/span-->										
									</div>	
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong></strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success start" ng-click="uploadLogo({{$cmp->id}},'logo','{{$cmp->name}}')">
										                    <i class="glyphicon glyphicon-upload"></i>
										                    <span>Start upload</span>
										                </button>
										                <span ng-show="showLoader">
										                	<img src="{{url('/')}}/assets/img/ajax-loader.gif">
										                </span>
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>								
								</div>								
							</form>
							<!-- END FORM-->

							<div class="form-horizontal" >
							<div class="form-body">									
								<h3 class="form-section">Associated Users</h3>

								<!-- <div class="table-toolbar">
									<div class="row">
										
										<div class="col-md-12 text-right">
											<div class="btn-group">
												<a class="btn green" id="" href="{{url('/')}}/superadmin/edit-user-details/{{$cmp->id}}"> <i class="fa fa-plus"></i> Add Users </a>
											</div>
										</div>
									</div>
								</div> -->
								
								<table class="table table-striped table-bordered table-hover" id="assoc_user_list">
									<thead>
										<tr>
											<th> Name </th>
											<th> Email </th>
											<th> Phone </th>
										</tr>
									</thead>														

								</table>
							</div>
						</div>
						</div>
					</div>																
					<!-- Company Dialog -->
				    <div class="modal fade" id="companyModal" close="cancel()">
						@include('superadmin::company.forms.form')
				    </div>
	    		<!-- End of Company Dialog -->
	    		
				</div>				
			</div>			
		</div>
		
	</div>
	<!-- END CONTENT -->	
<!-- END CONTAINER -->
@stop