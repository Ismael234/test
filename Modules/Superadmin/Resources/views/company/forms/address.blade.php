<div class="modal fade" id="addressModal" close="cancel()">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="addressInfo" name="addressInfo" class="horizontal-form" ng-submit="submitForm(,'address')">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">User Personal Details</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" id="company_id" name="company_id" ng-model="cmpdetails.company_id"/>
					<div class="form-body">
							<!--/row for buisness-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Business Address 1</label>
				<input type="text" id="bus_add1" name="bus_add1" class="form-control" ng-model="cmpdetails.bus_add1" placeholder="">
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Business Address 2</label>
				<input type="text" id="bus_add2" name="bus_add2" class="form-control" placeholder="" ng-model="cmpdetails.bus_add2">
				</div>
			</div>
			<!--/span-->
		</div>

		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Business Suburb</label>
				<input type="text" id="bus_suburb" name="bus_suburb" ng-model="cmpdetails.bus_suburb" class="form-control" placeholder="" >
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Business State</label>
				{{ Form::select('bus_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'bus_state', 'ng-model'=>"cmpdetails.bus_state"]) }}
				
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Business Post</label>
				<input type="text" id="bus_post" name="bus_post" ng-model="cmpdetails.bus_post"  class="form-control" placeholder="">
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Business Country</label>
				<input type="text" id="bus_country" name="bus_country"  ng-model="cmpdetails.bus_country" class="form-control" placeholder="" >
				</div>
			</div>
			<!--/span-->
		</div>

		<!-- <div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label"><input name="chck1" id="chck1" class="chck1" type="checkbox"> Business Address is same as billing address.</label>
				</div>
			</div>
		</div>
 -->
		<!--/row for billing-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Billing Address 1</label>
				<input type="text" id="billing_add1"  ng-model="cmpdetails.billing_add1" name="billing_add1" class="form-control" placeholder="">
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Billing Address 2</label>
				<input type="text" id="billing_add2"  ng-model="cmpdetails.billing_add2" name="billing_add2" class="form-control" placeholder="">
				</div>
			</div>
			<!--/span-->
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Billing Suburb</label>
				<input type="text" id="billing_suburb"  ng-model="cmpdetails.billing_suburb"  name="billing_suburb" class="form-control" placeholder="">
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Billing State</label>
				{{ Form::select('billing_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'billing_state','ng-model'=>"cmpdetails.billing_state"]) }}
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Billing Post</label>
				<input type="text" id="billing_post"  ng-model="cmpdetails.billing_post" name="billing_post" class="form-control" placeholder="">
				</div>
			</div>
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
				<label class="control-label">Billing Country</label>
				<input type="text" id="billing_country"   ng-model="cmpdetails.billing_country" name="billing_country" class="form-control" placeholder="">
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->		
	<!--/row-->

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
		<button type="submit" class="btn blue" ng-click="saveCompanyDetail()">Update</button>
	</div>
	</div>
	</form>
	</div>
</div>
</div>