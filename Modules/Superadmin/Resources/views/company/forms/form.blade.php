<div class="modal-dialog">
    <div class="modal-content">
		<form id="cmpInfo" name="cmpInfo" class="horizontal-form" ng-submit="submitForm({{$cmp->id}},'profile')" novalidate>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Company Personal Details</h4>
			</div>
			<div class="modal-body">
				{{ csrf_field() }}
			<input type="hidden" id="cmp_id" name="cmp_id" ng-model="cmp_id" value="@if(isset($cmp->id)){{$cmp->id}}@endif" />
				<div class="form-body">

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Company Name <span class="red">*</span></label>
								<input type="text" id="cmpname" class="form-control" name="cmpname" placeholder="" ng-model="cmp.cmpname" readonly="" ng-required="true" ng-blur="check_company_name(@if(isset($cmp->id)){{$cmp->id}}@endif)" />
								<span class="error" ng-show="cmpInfo.cmpname.$invalid && cmpInfo.cmpname.$touched">Company Name is required</span>
								<span class="error" ng-show="cmpInfo.cmpname.$invalid && cmpInfo.cmpname.$exist">Company Name already exist</span>
								@if ($errors->has('cmpname'))
								    <span class=error>{{ $errors->first('cmpname') }}</span>
								@endif

								<div style="color:red;display: none;" id="error_msg" role="alert">
								    <div ng-message="required">Company Name already exist.</div>		    
							  	</div>
								<!--<span class="help-block">
								This is inline help </span>-->
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Company Type <span class="red">*</span></label>
								<input type="text" id="ctype" name="ctype" class="form-control" placeholder="" ng-model="cmp.ctype" ng-required="true"/>
								<span class="error" ng-show="cmpInfo.ctype.$invalid && cmpInfo.ctype.$touched">Company Type is required</span>
								@if ($errors->has('ctype'))
								    <span class=error>{{ $errors->first('ctype') }}</span>
								@endif
							</div>
						</div>
						<!--/span-->
					</div>	
					<div class="row">
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Company Registration Number <span class="red"></span></label>
								<input type="text" id="cregis" name="cregis" class="form-control" placeholder="" ng-model="cmp.cregis"/>								
							</div>
						</div>
						<!--/span-->

						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Email id <span class="red">*</span></label>	
								<input type="email" id="email" name="email" class="form-control" placeholder=""  ng-model="cmp.email" ng-blur="check_company_email(@if(isset($cmp->id)){{$cmp->id}}@endif)" required>
								<span style="color:red" ng-show="cmpInfo.email.$dirty && cmpInfo.email.$invalid">
									<span ng-show="cmpInfo.email.$error.required">Email is required.</span>
									<span ng-show="cmpInfo.email.$error.email">Invalid email address.</span>
								</span>
								<div class="error" style="display: none;" id="error_msg_email" role="alert">
								    <div ng-message="required">Company email already exist.</div>		    
							  	</div>
								@if ($errors->has('email'))
								    <span class=error>{{ $errors->first('email') }}</span>
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Phone <span class="red">*</span></label>
									
									<input type="number" id="phone" name="phone" ng-minlength="8" ng-maxlength="15"  class="form-control" placeholder=""  ng-model="cmp.phone" required>

									<span style="color:red" ng-show="cmpInfo.phone.$dirty && cmpInfo.phone.$invalid">
										<span ng-show="cmpInfo.phone.$error.required || cmpInfo.phone.$error.number">Valid phone number is required.</span>
										<span ng-show="((cmpInfo.phone.$error.minlength ||
                           	cmpInfo.phone.$error.maxlength) && cmpInfo.phone.$dirty)">phone number between 8 to 15 digits.</span>
									</span>
									@if ($errors->has('phone'))
									    <span class=error>{{ $errors->first('phone') }}</span>
									@endif
								</div>
							</div>							
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
				<button type="submit" class="btn blue" ng-disabled="cmpInfo.cmpname.$dirty && cmpInfo.cmpname.$invalid ||  
				cmpInfo.email.$dirty && cmpInfo.email.$invalid ||  
				cmpInfo.ctype.$dirty && cmpInfo.ctype.$invalid ||  
				cmpInfo.phone.$dirty && cmpInfo.phone.$invalid" id="company_form" ng-click="saveComapny(@if(isset($cmp->id)){{$cmp->id}}@endif)">Update</button>
			</div>
		</form>
	</div>
</div>