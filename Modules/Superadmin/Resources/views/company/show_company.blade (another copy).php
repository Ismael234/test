@extends('superadmin::layouts.master')
<!-- BEGIN CONTAINER -->	
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" ng-app="companyModule">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" ng-controller="companyController">
        <!-- BEGIN PAGE HEADER-->
        @include('superadmin::partials.breadcrumb')
        <h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>-->	</h3>
        <!-- END PAGE HEADER-->
        <!-- BEGIN DASHBOARD STATS 1-->
       
        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
        	<div class="row">
			    <div class="col-lg-6 col-xs-12 col-sm-12">
			        <div class="portlet light bordered">
			            <div class="portlet-title tabbable-line">
			                <div class="caption col-md-9">
			                    <i class="icon-bubbles font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">Company Details</span>
			                </div>
			  
							<div class="caption col-md-3">
			                    <i class="icon-bubbles font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">
			                    	<a class="btn green" ng-click="showModelFrm({{$cmp->id}}, 'company')"><i class="fa fa-pencil"></i> Update Profile </a>
			                    </span>
			                </div>                                       
			            </div>
			            <div class="portlet-body">
			                <div class="tab-content">
			                    <div class="tab-pane active" id="portlet_comments_1">
			                        <!-- BEGIN: Comments -->
			                        <div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Company Name:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{ucfirst($cmp->name)}}
													</p>																										
													<input type="hidden" name="hid" id="hid" value="{{$cmp->id}}" />
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-7">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Company Type:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp->type}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>

									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Company Registration Number:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp->registration_number}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-7">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Email-id:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp->email}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>

									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Phone:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp->phone}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Logo:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														<span class="btn green fileinput-button">
									                    <i class="glyphicon glyphicon-plus"></i>
									                    @if(!empty($cmp_details[0]->logo_image))
									                    	<span>Change logo</span>
									                    @else
									                    	<span>Add logo</span>
									                    @endif
														 	<input type='file' name="logo" id="imgInp" ng-files="getTheFiles($files)"/>
    													</span><br><br>
    													@if(!empty($cmp_details[0]->logo_image))
    														<img id="logo_image" width="250" height="150" src="{{url('/')}}/uploads/company/thumbnail_250x150/{{$cmp_details[0]->logo_image}}" alt="your image" />
    													@else
    														<img id="logo_image" width="250" height="150" src="{{url('/')}}/uploads/company_icon.png" alt="your image" />
    													@endif 
    													<br><br>
    													<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success start" ng-click="uploadLogo({{$cmp->id}},'logo','{{$cmp->name}}')">
										                    <i class="glyphicon glyphicon-upload"></i>
										                    <span>Start upload</span>
										                </button>
										                <span ng-show="showLoader">
										                	<img src="{{url('/')}}/assets/img/ajax-loader.gif">
										                </span>
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>

									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label col-md-5"><strong></strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-7">
											<div class="form-group">
												<label class="control-label col-md-5"><strong></strong></label>
												<div class="col-md-7">													
														
													
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
			                        <!-- END: Comments -->
			                    </div>
			                   
			                </div>
			            </div>
			        </div>
			    </div>
			    <div class="col-lg-6 col-xs-12 col-sm-12">
			        <div class="portlet light bordered">
			            <div class="portlet-title tabbable-line">
			                <div class="caption">
			                    <i class=" icon-social-twitter font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">Companies</span>
			                </div>                                        
			            </div>
			            <div class="portlet-body">
			                <div class="tab-content">
			                    <div class="tab-pane active" id="tab_actions_pending">
			                        <!-- BEGIN: Actions -->
			                        Test
			                        <!-- END: Actions -->
			                    </div>                                            
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<!-- Company Dialog -->
		    <div class="modal fade" id="companyModal" close="cancel()">
				@include('superadmin::company.forms.form')
		    </div>
			<!-- End of Company Dialog -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->                         
@stop
<!-- END CONTAINER -->