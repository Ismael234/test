@extends('superadmin::layouts.master')
<!-- BEGIN CONTAINER -->	
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" ng-app="companyModule">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" ng-controller="companyController">
        <!-- BEGIN PAGE HEADER-->
        @include('superadmin::partials.breadcrumb')
        <h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>-->	</h3>
        <!-- END PAGE HEADER-->
        @if(Session::has('flash_alert_notice'))
            <div class="alert alert-success alert-dismissable">
              	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              	<i class="icon fa fa-check"></i>  
               	{{ Session::get('flash_alert_notice') }} 
            </div>
       	@endif
       
        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
        	<div class="row">
			    <div class="col-lg-6 col-xs-12 col-sm-12">
			        <div class="portlet light bordered box-min-height ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-dark hide"></i>
                                <span class="caption-subject bold font-dark uppercase"> Company Details</span>
                                <span class="caption-helper"></span>
                            </div>
                            <div class="actions">
                                <i class="icon-bubbles font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">
			                    	<a class="btn btn-icon-only green" data-toggle="tooltip" title="Edit Details" ng-click="showModelFrm({{$cmp->id}}, 'company')"><i class="fa fa-edit"></i> </a>
			                    </span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                            	<input type="hidden" name="hid" id="hid" value="{{$cmp->id}}" />
                            	<div class="col-md-12">
                                	<div class="col-md-6 pull-right top-buffer">
                                        <div class="btn green fileinput-button">
						                    <i class="glyphicon glyphicon-plus"></i>
						                    @if(!empty($cmp_details[0]->logo_image))
						                    	<span>Change logo</span>
						                    @else
						                    	<span>Add Logo</span>
						                    @endif   
						                    <input type='file' name="logo" id="imgInp" ng-files="getTheFiles($files)"/>
										</div>
										<br><br>
										<div class="submit-button">
											<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success start" ng-click="uploadLogo({{$cmp->id}},'logo','{{$cmp->name}}')">
							                    <i class="glyphicon glyphicon-upload"></i>
							                    <span>Start upload</span>
							                </button>
							                <span ng-show="showLoader">
							                	<img src="{{url('/')}}/assets/img/ajax-loader.gif">
							                </span>
						                </div>
									</div>
									<div class="col-md-6">
										@if(!empty($cmp_details[0]->logo_image))
											<img id="logo_image" width="250" height="150" src="{{url('/')}}/uploads/company/{{$cmp_details[0]->logo_image}}" alt="Company Logo" />
										@else
											<img id="logo_image" width="250" height="150" src="{{url('/')}}/uploads/company_icon.png" alt="Company Logo" />
										@endif 
										<br><br>
					                </div>                                                
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
										<label class="control-label col-md-5"><strong>Name:</strong></label>
										<div class="col-md-7">
											<p class="form-control-static">
												{{ucfirst($cmp->name)}}
											</p>
										</div>
									</div>                                                  
                                    <div class="row">														
                                    	<label class="control-label col-md-5"><strong>Type:</strong></label>
										<div class="col-md-7">
											<p class="form-control-static">
										 		{{ucfirst($cmp->type)}}
										 	</p>
										</div>
									</div>                                                  
                                    <div class="row">
										<label class="control-label col-md-5"><strong>Reg Number:</strong></label>
										<div class="col-md-7">
											<p class="form-control-static">
												{{$cmp->registration_number}}
											</p>
										</div>
									</div>                                                  
                                    <div class="row">
										<label class="control-label col-md-5"><strong>Email-id:</strong></label>
										<div class="col-md-7">
										 	<p class="form-control-static">
										 		{{$cmp->email}}
									 		</p>
										</div>
									</div>                                                  
                                    <div class="row">
										<label class="control-label col-md-5"><strong>Phone:</strong></label>
										<div class="col-md-7">
											<p class="form-control-static">
										 		{{$cmp->phone}}
									 		</p>
										</div>
									</div>                                                  
                                </div>
                            </div>
                        </div>
                    </div>
			    </div>
			    <div class="col-lg-6 col-xs-12 col-sm-12">
			        <div class="portlet light bordered box-min-height">
			            <div class="portlet-title tabbable-line">
			                <div class="caption">
			                    <i class=" icon-social-twitter font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">Comapny Address</span>
			                </div> 
			                <div class="actions">
                                <i class="icon-bubbles font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">
			                    	<a class="btn btn-icon-only green" data-toggle="tooltip" title="Edit Address" ng-click="showModelFrm({{$cmp->id}},'address')"><i class="fa fa-edit"></i> </a>
			                    </span>
                            </div>                                       
			            </div>
			            <!-- Address Dialog -->				    
							@include('superadmin::company.forms.address')
	    				<!-- End of Address Dialog -->
			            <div class="portlet-body">
			                <div class="tab-content">
			                    <div class="tab-pane active" id="tab_actions_pending">
			                        <!-- BEGIN: Actions -->
			                        <h4 class="caption-subject font-blue-steel uppercase text-right">Business Address</h4>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Address 1:</strong></label>
												<div class="col-md-7">												
													@if(!empty($cmp_details[0]->busi_address1))
														 {{$cmp_details[0]->busi_address1}}
													@endif													
												</div>
											</div>
										</div>
									
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Address 2:</strong></label>
												<div class="col-md-7">													
													@if(!empty($cmp_details[0]->busi_address2))
														 {{$cmp_details[0]->busi_address2}}
													@endif													
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Suburb:</strong></label>
												<div class="col-md-7">												
													@if(!empty($cmp_details[0]->busi_suburb))
														 {{$cmp_details[0]->busi_suburb}}
													@endif													
												</div>
											</div>
										</div>
									
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business State:</strong></label>
												<div class="col-md-7">													
													@if(!empty($cmp_details[0]->busi_state))
														 {{$cmp_details[0]->busi_state}}
													@endif													
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Post:</strong></label>
												<div class="col-md-7">												
													@if(!empty($cmp_details[0]->busi_post))
														 {{$cmp_details[0]->busi_post}}
													@endif													
												</div>
											</div>
										</div>
									
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Country:</strong></label>
												<div class="col-md-7">													
													@if(!empty($cmp_details[0]->busi_country))
														 {{$cmp_details[0]->busi_country}}
													@endif													
												</div>
											</div>
										</div>
									</div>
									<h4 class="caption-subject font-blue-steel uppercase text-right">Billing Address</h4>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Address 1:</strong></label>
												<div class="col-md-7">												
													@if(!empty($cmp_details[0]->bill_address1))
														 {{$cmp_details[0]->bill_address1}}
													@endif													
												</div>
											</div>
										</div>
									
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Address 2:</strong></label>
												<div class="col-md-7">													
													@if(!empty($cmp_details[0]->bill_address2))
														 {{$cmp_details[0]->bill_address2}}
													@endif													
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Suburb:</strong></label>
												<div class="col-md-7">												
													@if(!empty($cmp_details[0]->bill_suburb))
														 {{$cmp_details[0]->bill_suburb}}
													@endif													
												</div>
											</div>
										</div>
									
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing State:</strong></label>
												<div class="col-md-7">													
													@if(!empty($cmp_details[0]->bill_state))
														 {{$cmp_details[0]->bill_state}}
													@endif													
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Post:</strong></label>
												<div class="col-md-7">												
													@if(!empty($cmp_details[0]->bill_post))
														 {{$cmp_details[0]->bill_post}}
													@endif													
												</div>
											</div>
										</div>
									
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Country:</strong></label>
												<div class="col-md-7">													
													@if(!empty($cmp_details[0]->bill_country))
														 {{$cmp_details[0]->bill_country}}
													@endif													
												</div>
											</div>
										</div>
									</div>
			                        <!-- END: Actions -->
			                    </div> 			                                                               
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<!--Assoicative users-->
			<div class="row">
			    <div class="col-lg-6 col-xs-12 col-sm-12">
			        <div class="portlet light bordered box-min-height">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-dark hide"></i>
                                <span class="caption-subject bold font-dark uppercase">Users</span>
                                <span class="caption-helper"></span>
                            </div>                                       
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                            	<div class="col-md-12">
									<div class="form-group">
                                        <table class="table" id="assoc_user_list">
										<thead>
											<tr>
												<th> Name </th>													
												<th> Email ID </th>	
												<th> Phone </th>												
											</tr>
										</thead>
									</table>
									</div>
								</div>
                        	</div>
                        </div>
                    </div>
			    </div>
			    <div class="col-lg-6 col-xs-12 col-sm-12">
			        <div class="portlet light bordered box-min-height">
			            <div class="portlet-title tabbable-line">
			                <div class="caption">
			                    <i class=" icon-social-twitter font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">Projects</span>
			                </div> 
			                <!-- <div class="actions">
                                <i class="icon-bubbles font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">
			                    	<a class="btn btn-icon-only green"  ng-click="showModelFrm({{$cmp->id}},'address')"><i class="fa fa-edit"></i> </a>
			                    </span>
                            </div> -->                                       
			            </div>
			            <!-- Address Dialog -->				    
							@include('superadmin::company.forms.address')
	    				<!-- End of Address Dialog -->
			            <div class="portlet-body">
			                <div class="tab-content">
			                    <div class="row">
	                            	<div class="col-md-12">
										<div class="form-group">
	                                        <table class="table" id="assoc_project_list">
											<thead>
												<tr>
													<th> Name </th>													
													<th> Type </th>	
													<th> Action </th>												
												</tr>
											</thead>
										</table>
										</div>
									</div>
	                        	</div>		                                                               
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<!-- Company Dialog -->
		    <div class="modal fade" id="companyModal" close="cancel()">
				@include('superadmin::company.forms.form')
		    </div>
			<!-- End of Company Dialog -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->                         
@stop
<!-- END CONTAINER -->