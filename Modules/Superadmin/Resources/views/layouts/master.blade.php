@if(Session::get('projectConfig') != 0)
	@include('superadmin::layouts.project_header')
	@include('superadmin::layouts.project_sidebar')	
@else
	@include('superadmin::layouts.header')
	@include('superadmin::layouts.sidebar')	
@endif
@yield('content')
@include('superadmin::layouts.footer')