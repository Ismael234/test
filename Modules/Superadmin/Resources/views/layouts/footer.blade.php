</div>
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 Copyright © 2017 All rights reserved - WiseWorking
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="{{url('/')}}/assets/js/jquery.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/js/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

<script src="{{url('/')}}/assets/js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="{{url('/')}}/assets/js/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/js/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

<!-- <script src="{{url('/')}}/assets/js/jquery.blockui.min.js" type="text/javascript"></script> -->
<script src="{{url('/')}}/assets/js/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/js/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('/')}}/assets/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js" type="text/javascript"></script>

<script src="{{url('/')}}/assets/scripts/metronic.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/js/layout.js" type="text/javascript"></script>
<!-- <script src="{{url('/')}}/assets/js/layout/scripts/demo.js" type="text/javascript"></script> -->
<script src="{{url('/')}}/assets/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<!-- custom js -->
<script type="text/javascript">
var baseUrl='{{url('/')}}'
</script>
@if(!empty($data['scripts']))
	@foreach ($data['scripts'] as $js)
		<script src="{{url('/')}}/js/{{$js}}.js"></script>
	@endforeach
@endif
<script>
jQuery(document).ready(function() {          
Layout.init(); // init current layout
$('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>