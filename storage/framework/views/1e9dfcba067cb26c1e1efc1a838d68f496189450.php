<div class="modal fade" id="companiesModal" close="cancel()" style="display:none;">
	<!-- <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> -->
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- BEGIN FORM-->
			<form id="userCompany" name="userCompany" class="horizontal-form" ng-submit="submitForm(<?php echo e($user->id); ?>,'companies')" novalidate>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Associated Companies</h4>
				</div>
				<div class="modal-body">
					<?php echo e(csrf_field()); ?>

					<input type="hidden" id="user_id" name="user_id" value="<?php if(isset($user->id)): ?><?php echo e($user->id); ?><?php endif; ?>" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Company <span class="red">*</span></label>
									<select name="company[]" id="company" class="select2_category form-control" data-placeholder="Choose Companies" tabindex="1" multiple>
										<option value="">Select Atleast One</option>
										<?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
											<option value="<?php echo e($company->id); ?>" "<?php if(!empty($company->id) && in_array($company->id, $uc)): ?> selected <?php endif; ?>" ><?php echo e($company->name); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
									</select>
									<?php if($errors->has('company')): ?>
									    <span class=error><?php echo e($errors->first('company')); ?></span>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel('userCompany')">Cancel</button>
					<button type="submit" class="btn btn-success" id="companies_form">Update</button>
				</div>
			</form>
			<!-- END FORM-->
	    </div>
	</div>
</div>