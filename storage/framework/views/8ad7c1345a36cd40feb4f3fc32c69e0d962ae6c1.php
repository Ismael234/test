<div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
    <div class="alert alert-success display-hide">
        <button class="close" data-close="alert"></button> Your form validation is successful! </div>    
     <div class="form-group">
        <label class="control-label col-md-3">Name
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-4">
            <div class="input-icon right">
                <i class="fa"></i>
                <input type="text" class="form-control" name="name" id="name" value="<?php if(isset($permission->name)): ?><?php echo e($permission->name); ?><?php endif; ?>"> 
                <?php if($errors->has('name')): ?>
                    <span class=error>The name field is required.</span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">Type
            <span class="required" aria-required="true">  </span>
        </label>
        <div class="col-md-4">
            <div class="input-icon right">
                <i class="fa"></i>     
                <?php if(isset($permission->type) && !empty($permission->type)): ?>
                    <?php  $selected_type = $permission->type  ?>
                <?php else: ?>
                    <?php  $selected_type = ""  ?>
                <?php endif; ?>

                 <?php  $type = array('Menu'=>'Menu','Submenu'=>'Submenu','Internal permission'=>'Internal permission')  ?>                                
                <?php echo e(Form::select('type', $type, $selected_type,  ['class' => 'select2_category form-control','id'=>'type','onchange'=>'checkRule(this)'])); ?>

            </div>            
        </div>
    </div>
    <?php if($selected_type == "Menu" || $selected_type == ""): ?>
        <div class="form-group margin-top-20 module" style="display: none;">
    <?php else: ?>
        <div class="form-group  margin-top-20 module">
    <?php endif; ?>

        <label class="control-label col-md-3">
            <span class="required" aria-required="true">  </span>
        </label>
        <div class="col-md-4">
            <div class="input-icon right">
                <i class="fa"></i>                        

                <?php if(isset($permission->module_id) && !empty($permission->module_id)): ?>
                    <?php  $selected_module = $permission->module_id  ?>
                <?php else: ?>
                    <?php  $selected_module = ""  ?>
                <?php endif; ?>
                <?php echo e(Form::select('module_name', $data['module_list'], $selected_module,  ['class' => 'form-control','id'=>'module_name'])); ?>

                <?php if($errors->has('module_name')): ?>
                    <span class=error>The Module name field is required.</span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">Description
            <span class="required" aria-required="true">  </span>
        </label>
        <div class="col-md-4">
            <div class="input-icon right">
                <i class="fa"></i>
                <textarea class="form-control" rows="3" name="description" id="description"> <?php if(isset($permission->description)): ?><?php echo e($permission->description); ?><?php endif; ?></textarea>
            </div>
        </div>
    </div>  
</div>
<div class="form-actions text-center">
    <button type="button" class="btn btn-default" onClick="history.go(-1); return false;"> Cancel </button>
    <button type="submit" class="btn blue"><i class="fa fa-check"></i> Submit</button>
</div>