<?php $__env->startSection('content'); ?>
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper" ng-app="projectIssuetoModule">
		<div class="page-content" ng-controller="projectIssuetoController">
			
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

			<h3 class="page-title">
			<!--Add User --> <!--<small>reports & statistics</small>-->
			</h3>

			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue-hoki">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Issuedto List
							</div>
							<div class="tools"></div>
						</div>
						<div class="portlet-body" >
							<?php if(isset($_REQUEST['i']) && !empty($_REQUEST['i']) && empty($j)): ?>
		                        <div class="alert alert-success alert-dismissable">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           		<?php  echo $_REQUEST['i'];  ?>;
		                        </div>
                       		<?php endif; ?>
							<div class="row">
								<div class="col-md-4 pull-right">
									<div class="form-group">
										<!-- <label class="control-label col-md-3">Project</label> -->
										<?php echo e(Form::select('project', $project, $project,  ['class' => 'select2_category form-control','id'=>'project'])); ?>

									</div>
								</div>
								<div class="col-md-2 pull-right text-right">
									<strong>Project :</strong>
								</div>
							</div>
							<div class="hide" id="project_issueto_section">
								<div class="edit_btn">
									<div class="row">
										<div class="col-md-12 text-right">
											<div class="row">
												<div class="col-md-12">
													<a class="btn green" id="" href="javascript:;" ng-click="showIssueto('add')"> <i class="fa fa-plus"></i> Add New </a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php echo $__env->make('projectconfiguration::issueto.issueto_form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
								<div class="row">
									<div class="col-md-12">
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th> Name </th>
													<th> Company </th>
													<th> Email </th>
													<th> Phone </th>
													<th> Activity </th>
													<th> Action </th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>

		</div>
	</div>
	
<!-- END CONTAINER -->           																									
<?php $__env->stopSection(); ?>
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>