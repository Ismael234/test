<!-- BEGIN COPYRIGHT -->
<div class="copyright">
     Copyright © 2017 All rights reserved - WiseWorking
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo e(url('/')); ?>/assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/js/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- <script src="<?php echo e(url('/')); ?>/assets/js/jquery.blockui.min.js" type="text/javascript"></script> -->
<script src="<?php echo e(url('/')); ?>/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/js/jquery.cokie.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo e(url('/')); ?>/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- <script src="<?php echo e(url('/')); ?>/assets/plugins/select2/select2.min.js" type="text/javascript"></script> -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo e(url('/')); ?>/assets/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/js/layout.js" type="text/javascript"></script>
<script src="<?php echo e(url('/')); ?>/assets/js/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
var baseUrl='<?php echo e(url('/')); ?>'
</script>
<script>
jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
  Layout.init(); // init current layout
  Login.init();
});
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>