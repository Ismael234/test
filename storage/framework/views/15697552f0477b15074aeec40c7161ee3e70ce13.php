
<div class="form-body">
	<!--<h3 class="form-section">Person Info</h3>-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Project Name <span class="red">*</span></label>
				<input type="text" id="name" class="form-control" name="name" placeholder="" value="<?php if(isset($project->name)): ?><?php echo e($project->name); ?><?php endif; ?>" />
				<?php if($errors->has('name')): ?>
				    <span class=error><?php echo e($errors->first('name')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Project Code <span class="red">*</span></label>
				<input type="text" id="code" name="code" class="form-control" placeholder=""  value="<?php if(isset($project->code)): ?><?php echo e($project->code); ?><?php endif; ?>">
				<?php if($errors->has('code')): ?>
				    <span class=error><?php echo e($errors->first('code')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Company <span class="red">*</span></label>
				<?php  $proj_comps = array($proj_comps)  ?>
				<select name="company[]" id="company" class="select2_category form-control" data-placeholder="Choose Companies" tabindex="1" multiple>
					<option value="" disabled>Select Atleast One</option>
					
					<?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
						<option value="<?php echo e($company->id); ?>" <?php if(in_array($company->id, $proj_comps)): ?> selected <?php endif; ?> ><?php echo e($company->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
					
				</select>
				<?php if($errors->has('company')): ?>
				    <span class=error><?php echo e($errors->first('company')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
		
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Description </label>
				<textarea type="text" id="description" name="description" class="form-control" rows = "3" ><?php if(isset($project->description)): ?><?php echo e($project->description); ?><?php endif; ?></textarea>
				<?php if($errors->has('description')): ?>
				    <span class=error><?php echo e($errors->first('description')); ?></span>
				<?php endif; ?>
			</div>
			
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Project Type <span class="red">*</span></label>
				<?php if(!empty($project)): ?>						
					<?php echo e(Form::select('type', $option_type, $project->type,  ['class' => 'select2_category form-control','id'=>'type'])); ?>

				<?php else: ?>
					<?php echo e(Form::select('type', $option_type, null,  ['class' => 'select2_category form-control','id'=>'type'])); ?>

				<?php endif; ?>

				<?php if($errors->has('type')): ?>
				    <span class=error><?php echo e($errors->first('type')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="btn btn-default btn-file">Project Logo <span class="red"></span>
				<input type="file" id="project_logo" name="project_logo" class=""></label>
				<?php if(!empty($project)): ?>
					<?php if(!empty($project->logo)): ?>
						<?php $logo_image = $project->logo; ?>
					<?php endif; ?>
					<img class="logo_img" src="<?php echo e(url('/')); ?>/uploads/project/<?php echo e($logo_image); ?>">
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	

	<hr>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label"><u><i><strong>Address Details:</strong><i></u></label>
			</div>
		</div>
	</div>
	<!--/row for buisness-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Address 1 <span class="red">*</span></label>
				<input type="text" id="address1" name="address1" class="form-control" placeholder="" value="<?php if(isset($project->address1)): ?><?php echo e($project->address1); ?><?php endif; ?>">
				<?php if($errors->has('address1')): ?>
				    <span class=error><?php echo e($errors->first('address1')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Address 2 </label>
				<input type="text" id="address2" name="address2" class="form-control" placeholder="" value="<?php if(isset($project->address2)): ?><?php echo e($project->address2); ?><?php endif; ?>">
				<?php if($errors->has('address2')): ?>
				    <span class=error><?php echo e($errors->first('address2')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>

	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Suburb <span class="red">*</span></label>
				<input type="text" id="suburb" name="suburb" class="form-control" placeholder="" value="<?php if(isset($project->suburb)): ?><?php echo e($project->suburb); ?><?php endif; ?>" />
				<?php if($errors->has('suburb')): ?>
				    <span class=error><?php echo e($errors->first('suburb')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Post Code <span class="red">*</span></label>
				<input type="text" id="postal_code" name="postal_code" class="form-control" placeholder="" value="<?php if(isset($project->postal_code)): ?><?php echo e($project->postal_code); ?><?php endif; ?>" />
				<?php if($errors->has('postal_code')): ?>
				    <span class=error><?php echo e($errors->first('postal_code')); ?></span>
				<?php endif; ?>
			
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">State <span class="red">*</span></label>
				<?php if(!empty($project)): ?>						
					<?php echo e(Form::select('state', $option_state, $project->state,  ['class' => 'select2_category form-control','id'=>'state'])); ?>

				<?php else: ?>
					<?php echo e(Form::select('state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'state'])); ?>

				<?php endif; ?>
				<?php if($errors->has('state')): ?>
				    <span class=error><?php echo e($errors->first('state')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Country <span class="red">*</span></label>
				<?php if(!empty($project)): ?>						
					<?php echo e(Form::select('country', $option_country, $project->country,  ['class' => 'select2_category form-control','id'=>'country'])); ?>

				<?php else: ?>
					<?php echo e(Form::select('country', $option_country, null,  ['class' => 'select2_category form-control','id'=>'country'])); ?>

				<?php endif; ?>
				<?php if($errors->has('country')): ?>
				    <span class=error><?php echo e($errors->first('country')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">GPS Coordinates </label>
				<input type="text" id="geo_code" name="geo_code" class="form-control" placeholder="" value="<?php if(isset($project->geo_code)): ?><?php echo e($project->geo_code); ?><?php endif; ?>" />
				<?php if($errors->has('geo_code')): ?>
				    <span class=error><?php echo e($errors->first('geo_code')); ?></span>
				<?php endif; ?>
			</div>
		</div>
	</div>

</div>


<div class="form-actions">
	<button type="button" class="btn btn-default" onClick="history.go(-1); return false;"> Cancel </button>
	<button type="submit" class="btn blue pull-right"><i class="fa fa-check"></i> Submit</button>
</div>

