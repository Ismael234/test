<!-- BEGIN CONTAINER -->
	
<?php $__env->startSection('content'); ?>
	
	
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->			
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>-->	</h3>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Edit Permission</span>
								<span class="caption-helper"></span>
							</div>
						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							
							<form action="<?php echo e(url('superadmin/update-permission')); ?>" method="post" class="form-horizontal" id="add_permission" autocomplete="on|off" enctype="multipart/form-data" accept-charset="utf-8" novalidate="novalidate">
								<?php echo e(csrf_field()); ?>

								<input type="hidden" name="hid" id="hid" value="<?php if(!empty($permission->id)): ?><?php echo e($permission->id); ?><?php endif; ?>">
                                <?php echo $__env->make('superadmin::permission.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </form>
							<!-- END FORM-->
						</div>
					</div>	
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
			
		</div>
	</div>
<?php $__env->stopSection(); ?>
<!-- END CONTAINER -->
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>