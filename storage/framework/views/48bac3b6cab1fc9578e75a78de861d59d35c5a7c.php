<!-- BEGIN CONTAINER -->
	<!-- BEGIN SIDEBAR -->
	<?php $__env->startSection('content'); ?>

	<!-- END SIDEBAR -->

	<div class="page-content-wrapper" ng-app="roleModule">
		<div class="page-content" ng-controller="roleController">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		
			<!-- BEGIN PAGE HEADER-->			
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			
			<h3 class="page-title">
			<!--Add User --> <!--<small>reports & statistics</small>-->
			</h3>
			<!-- END PAGE HEADER-->
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="tab-pane active" id="tab_3">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Authentication Details
							</div>
							<!-- <div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
								</a>
								<a href="javascript:;" class="reload" data-original-title="" title="">
								</a>											
							</div> -->
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->

							<div class="edit_btn">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">						
													<!-- <a href="#"><button type="button" class="btn default" onclick="history.go(-1); return false;"><i class="fa fa-chevron-left "></i> Back</button></a> -->
													<a href="javascript:;" ng-click="toggle('edit',<?php echo e($role->id); ?>,'<?php echo e($role->name); ?>')" class="btn green"><i class="fa fa-pencil"></i> Edit</a>
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>

							<form class="form-horizontal" role="form">
								<div class="form-body">									
									<h3 class="form-section"><strong><?php echo e($role->name); ?></strong></h3>
									<?php if(!empty($parent)): ?>
									<?php $__currentLoopData = $parent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$p): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
										<div class="list-group">
											<a href="javascript:;" class="list-group-item active">
											<h4 class="list-group-item-heading"><?php echo e($p); ?></h4>
											<p class="list-group-item-text">
												 
											</p>
											</a>
											<?php 
												$sub=array();
												$int_p=array();
												$sub_key = '';
												$int_key = '';
											 ?>
											<?php if(!empty($child[$key]) && isset($child[$key])): ?>
												<?php $__currentLoopData = $child[$key]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1=>$c): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
														<?php if($type[$key1] == "Submenu"): ?>
															<?php  $sub_key = $type[$key1]  ?>
															<?php  $sub[] =$c  ?>
														<?php else: ?>
															<?php  $int_key = $type[$key1]  ?>
															<?php  $int_p[] =$c  ?>
														<?php endif; ?>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>		
												<?php if($sub_key == "Submenu" && !empty($sub)): ?>
												<a href="javascript:;" class="list-group-item">
												<h4 class="list-group-item-heading"><strong>Submenu:-</strong>		
													<?php echo e(implode(",",$sub)); ?>

												</h4>
												</a>
												<?php endif; ?>
												<?php if($int_key == "Internal permission" && !empty($int_p)): ?>
												<a href="javascript:;" class="list-group-item">
												<h4 class="list-group-item-heading"><strong>Internal permission:-</strong>	
													<?php echo e(implode(",",$int_p)); ?>

												</h4>
												</a>
												<?php endif; ?>
											<?php endif; ?>								
										</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
									<?php endif; ?>
								</div>								
							</form>
							<!-- END FORM-->
							<!-- Confirmation Dialog -->
		    <div class="modal" id="showModal" close="cancel()" style="display:none;">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-header">
		          	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		            <h4 class="modal-title">Role Details</h4>
		          </div>
		          <div class="modal-body">
		          	<!-- BEGIN FORM-->
	          		<div id="perm_hidevalue" style="display: none;"><?php echo e(json_encode($allPermission)); ?></div>
					<form method="GET" action="<?php echo e(url('/')); ?>" accept-charset="UTF-8" name="role" id="role" novalidate="novalidate">
						<?php echo e(csrf_field()); ?>

						<?php echo $__env->make('superadmin::role.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>   
						<input type="hidden" id="hid" name="hid" value="<?php echo e($role->id); ?>">
							</div>
				          <div class="modal-footer">
				            <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
				            <button type="button" ng-disabled="role.name.$dirty && role.name.$invalid" class="btn btn-primary angfrm" ng-click="save(<?php echo e($role->id); ?>)">Save</button>
				          </div>        	
	            	</form>
					<!-- END FORM-->
		          
		        </div>
		      </div>
		    </div>
    	<!-- End of Confirmation Dialog -->
						</div>
					</div>																
				</div>
			</div>
		</div>

		
	</div>
	<!-- END CONTENT -->	
<!-- END CONTAINER -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>