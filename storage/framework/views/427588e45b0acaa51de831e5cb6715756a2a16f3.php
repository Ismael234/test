<!-- BEGIN CONTAINER -->	
<?php $__env->startSection('content'); ?>
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>-->	</h3>
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                       
                        <div class="clearfix"></div>
                        <!-- END DASHBOARD STATS 1-->
                        <?php echo $__env->make('projectconfiguration::location.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->                         
<?php $__env->stopSection(); ?>
<!-- END CONTAINER -->
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>