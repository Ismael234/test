<?php $__env->startSection('content'); ?>
<!-- BEGIN CONTAINER -->
	<div class="page-content-wrapper" ng-app="userModule">
		<div class="page-content" ng-controller="userController">
			<!-- BEGIN PAGE HEADER-->
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<!-- <?php echo $breadcrumbs; ?> -->
			<h3 class="page-title">	 </h3>
			<!-- END PAGE HEADER-->

			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>User Details
						</div>
						
					</div>
					<div class="portlet-body form">
					<?php if(Session::has('flash_alert_notice')): ?>
                        <div class="alert alert-success alert-dismissable">
                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          	<i class="icon fa fa-check"></i>  
                           	<?php echo e(Session::get('flash_alert_notice')); ?> 
                        </div>
                   	<?php endif; ?>

						<div class="edit_btn">
							<div class="row">
								<div class="col-md-12 text-right">
									<div class="row">
										<div class="col-md-12">
											<!-- <a href="#"><button type="button" onClick="history.go(-1); return false;" class="btn default"><i class="fa fa-chevron-left "></i> Back</button></a> -->
											<button class="btn green" ng-click="showModelFrm(<?php echo e($user->id); ?>, 'profile')"><i class="fa fa-pencil"></i> Update Profile</button>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
						<!-- Profile Dialog -->
							<?php echo $__env->make('superadmin::user.forms.profile', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				    	<!-- End of Profile Dialog -->
						<!-- BEGIN FORM-->
						<div class="form-horizontal">
							<div class="form-body">									
								<h3 class="form-section">User Info</h3>								
								<div class="row">
									<div class="col-md-6 text-center">
										<form id="userLogoFrm" runat="server">
											<div class="form-group">
												<span class="btn green fileinput-button">
								                    <i class="glyphicon glyphicon-plus"></i>

								                    <?php if(!empty($udetail[0]->logo_image)): ?>
								                    	<span>Change Photo</span>
								                    <?php else: ?>
								                    	<span>Add Photo</span>
								                    <?php endif; ?>
													 	<input type='file' name="logo" id="userImg"/>
												</span><br><br>
												<?php if(!empty($udetail[0]->logo_image)): ?>
													<img id="logo_image"  width="200" height="200"  src="<?php echo e(url('/')); ?>/uploads/user/<?php echo e($udetail[0]->logo_image); ?>" alt="your image" />
												<?php else: ?>
													<img id="logo_image"  width="200" height="200"  src="<?php echo e(url('/')); ?>/uploads/user_icon.jpg" alt="user image" />
												<?php endif; ?>
											</div>	
											<p class="form-control-static">
												<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success start" ng-click="submitForm(<?php echo e($user->id); ?>,'logo','<?php echo e($user->name); ?>')">
								                    <i class="glyphicon glyphicon-upload"></i>
								                    <span>Start upload</span>
								                </button>
								                <span ng-show="showLoader">
								                	<img src="<?php echo e(url('/')); ?>/assets/img/ajax-loader.gif">
								                </span>
											</p>
										</form>
											

									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-3"><strong>Username :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php echo e($user->name); ?>

												</p>
											</div>

											<label class="control-label col-md-3"><strong>Password :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php echo e($user->plain_password); ?>

												</p>
											</div>

											<label class="control-label col-md-3"><strong>User Role :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													<?php echo e(((isset($roles[$user->role_id])) ? $roles[$user->role_id] : 'NA')); ?>

												</p>
											</div>

											<label class="control-label col-md-3"><strong>Name :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php echo e($user->full_name); ?>

												</p>
											</div>

											<label class="control-label col-md-3"><strong>Email :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php echo e($user->email); ?>

												</p>
											</div>

											<label class="control-label col-md-3"><strong>Mobile :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													<?php echo e($user->phone); ?>

												</p>
											</div>
										
											
										<?php /*
											<label class="control-label col-md-3"><strong>Company :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													@foreach($user_compnies as $uc)
													{{$compnies[$uc]}}<br>
													@endforeach
												</p>
											</div> */?>
										</div>
									</div>
									<!--/span-->
								</div>
							</div>							
						</div>
						<!-- END USERINFO FORM-->
						<div class="edit_btn">
							<div class="row">
								<div class="col-md-12 text-right">
									<div class="row">
										<div class="col-md-12">
											<button class="btn green" ng-click="showModelFrm(<?php echo e($user->id); ?>, 'address')"><i class="fa fa-pencil"></i> Update Address</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Address Dialog -->
							<?php echo $__env->make('superadmin::user.forms.address', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				    	<!-- End of Address Dialog -->
						<div class="form-horizontal">
							<div class="form-body">									
								<h3 class="form-section">User Address</h3>		
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Business Address 1 :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php if(!empty($user_details)): ?><?php echo e($user_details->busi_address1); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
								
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Business Address 2 :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php if(!empty($user_details)): ?><?php echo e($user_details->busi_address2); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<!--/span-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Business Suburb :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php if(!empty($user_details)): ?><?php echo e($user_details->busi_suburb); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
									<!--/span-->
								
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Business State :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													<?php if(!empty($user_details)): ?><?php echo e($user_details->busi_state); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
								</div>
								<!--/row-->
								<div class="row">
									<!--/span-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Business Post :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													<?php if(!empty($user_details)): ?><?php echo e($user_details->busi_post); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>

									<!--/span-->
									<!--/span-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Business Country :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													<?php if(!empty($user_details)): ?><?php echo e($user_details->busi_country); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
								<!--Billing addr-->
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Billing Address 1 :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php if(!empty($user_details)): ?><?php echo e($user_details->bill_address1); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
								
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Billing Address 2 :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php if(!empty($user_details)): ?><?php echo e($user_details->bill_address2); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<!--/span-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Billing Suburb :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													 <?php if(!empty($user_details)): ?><?php echo e($user_details->bill_suburb); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
									<!--/span-->
								
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Billing State :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													<?php if(!empty($user_details)): ?><?php echo e($user_details->bill_state); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
								</div>
								<!--/row-->
								<div class="row">
									<!--/span-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Billing Post :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													<?php if(!empty($user_details)): ?><?php echo e($user_details->bill_post); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
									<!--/span-->
									<!--/span-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-5"><strong>Billing Country :</strong></label>
											<div class="col-md-7">
												<p class="form-control-static">
													<?php if(!empty($user_details)): ?><?php echo e($user_details->bill_country); ?><?php endif; ?>
												</p>
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
							</div>							
						</div>
						
						<div class="edit_btn">
							<div class="row">
								<div class="col-md-12 text-right">
									<div class="row">
										<div class="col-md-12">
											<button class="btn green" ng-click="showModelFrm(<?php echo e($user->id); ?>, 'companies')"> <i class="fa fa-plus"></i> Add More Companies </button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Address Dialog -->
							<?php echo $__env->make('superadmin::user.forms.companies', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				    	<!-- End of Address Dialog -->
						<div class="form-horizontal" >
							<div class="form-body">									
								<h3 class="form-section">Associated Companies</h3>
								<table class="table table-striped table-bordered table-hover" id="assoc_comp_list">
									<thead>
										<tr>
											<th> Company Name </th>
											<th> Company Type </th>
										</tr>
									</thead>														
								</table>
							</div>
						</div>
					</div>
				</div>																
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
<!-- END CONTAINER -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>