<div class="form-body">
<!--<h3 class="form-section">Person Info</h3>-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Name <span class="red">*</span></label>
			<input type="text" id="name" class="form-control" name="name" placeholder="" value="<?php if(!empty($company->name)): ?><?php echo e($company->name); ?><?php endif; ?>">
			<?php if($errors->has('name')): ?>
			    <span class=error>The company name field is required.</span>
			<?php endif; ?>
			<!--<span class="help-block">
			This is inline help </span>-->
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Type <span class="red">*</span></label>
			<input type="text" id="ctype" name="ctype" class="form-control" placeholder="" value="<?php if(!empty($company->type)): ?><?php echo e($company->type); ?><?php endif; ?>">
			<?php if($errors->has('ctype')): ?>
			    <span class=error>The type field is required</span>
			<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Registration Number</label>
			<input type="text" id="cregis" name="cregis" class="form-control" placeholder="" value="<?php if(!empty($company->registration_number)): ?><?php echo e($company->registration_number); ?><?php endif; ?>">
			<!--<span class="help-block">
			Select your gender </span>-->
			</div>
		</div>
	<!--/span-->
	<div class="col-md-6">
		<div class="form-group">
		<label class="control-label">Email id <span class="red">*</span></label>
		<input type="text" id="email" name="email" class="form-control" placeholder="" value="<?php if(!empty($company->email)): ?><?php echo e($company->email); ?><?php endif; ?>">
		<?php if($errors->has('email')): ?>
			    <span class=error><?php echo e($errors->first('email')); ?></span>
			<?php endif; ?>
		</div>
	</div>
	<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Phone <span class="red">*</span></label>
				<input type="text" id="phone" name="phone" class="form-control" placeholder="" value="<?php if(!empty($company->phone)): ?><?php echo e($company->phone); ?><?php endif; ?>">
				<?php if($errors->has('phone')): ?>
				    <span class=error><?php echo e($errors->first('phone')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		
	</div>
	<hr>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label"><u><i><strong>Address Details:</strong><i></u></label>
			</div>
		</div>
	</div>
	<!--/row for buisness-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Address 1</label>
			<input type="text" id="bus_add1" name="bus_add1" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->busi_address1)): ?><?php echo e($cmp_details[0]->busi_address1); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Address 2</label>
			<input type="text" id="bus_add2" name="bus_add2" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->busi_address2)): ?><?php echo e($cmp_details[0]->busi_address2); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
	</div>

	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Suburb</label>
			<input type="text" id="bus_suburb" name="bus_suburb" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->busi_suburb)): ?><?php echo e($cmp_details[0]->busi_suburb); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business State</label>
			<?php if(!empty($cmp_details[0]->busi_state)): ?>						
				<?php echo e(Form::select('bus_state', $option_state, $cmp_details[0]->busi_state,  ['class' => 'select2_category form-control','id'=>'bus_state'])); ?>

			<?php else: ?>
				<?php echo e(Form::select('bus_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'bus_state'])); ?>

			<?php endif; ?>
			
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Post</label>
			<input type="text" id="bus_post" name="bus_post" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->busi_post)): ?><?php echo e($cmp_details[0]->busi_post); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Country</label>
			<input type="text" id="bus_country" name="bus_country" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->busi_country)): ?><?php echo e($cmp_details[0]->busi_country); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label checkboxes">					
					<input name="chck1" id="chck1" class="chck1" type="checkbox"> Business Address is same as billing address.
				</label>
			</div>
		</div>
	</div>

	<!--/row for billing-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Address 1</label>
			<input type="text" id="buld_add1" name="buld_add1" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->bill_address1)): ?><?php echo e($cmp_details[0]->bill_address1); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Address 2</label>
			<input type="text" id="buld_add2" name="buld_add2" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->bill_address2)): ?><?php echo e($cmp_details[0]->bill_address2); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Suburb</label>
			<input type="text" id="buld_suburb" name="buld_suburb" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->bill_suburb)): ?><?php echo e($cmp_details[0]->bill_suburb); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing State</label>
			<?php if(!empty($cmp_details[0]->bill_state)): ?>
				<?php echo e(Form::select('buld_state', $option_state, $cmp_details[0]->bill_state,  ['class' => 'select2_category form-control','id'=>'buld_state'])); ?>

			<?php else: ?>
				<?php echo e(Form::select('buld_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'buld_state'])); ?>

			<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Post</label>
			<input type="text" id="buld_post" name="buld_post" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->bill_post)): ?><?php echo e($cmp_details[0]->bill_post); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Country</label>
			<input type="text" id="buld_country" name="buld_country" class="form-control" placeholder="" value="<?php if(!empty($cmp_details[0]->bill_country)): ?><?php echo e($cmp_details[0]->bill_country); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<?php /*
	<div class="row">		
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="btn btn-default btn-file">
				    Logo <input type="file" name="logo" id="logo">				    
				</label>
				@if(!empty($cmp_details[0]->logo_image))
				    <img class="logo_img" src="{{url('/')}}/uploads/company/{{$cmp_details[0]->logo_image}}">
				    @endif
			</div>
		</div>
		<!--/span-->
	</div>
	*/ ?>
<!--/row-->

</div>
<div class="form-actions">
	<button type="button" class="btn btn-default" onClick="history.go(-1); return false;"> Cancel </button>
	<button type="submit" class="btn blue pull-right"><i class="fa fa-check"></i> Submit</button>
</div>
