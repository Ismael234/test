
<div class="form-body">
	<!--<h3 class="form-section">Person Info</h3>-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group col-md-12">
				<label class="control-label">Name <span class="red">*</span></label>
				
				<?php $checklist_name = (isset($checklist->checklist_name)) ? $checklist->checklist_name : ''; ?>
				<?php echo e(Form::text('checklist_name', $checklist_name, $attributes = array('class'=>'form-control', 'id' => 'checklist_name', ))); ?> 
				<?php if($errors->has('checklist_name')): ?>
				    <span class=error><?php echo e($errors->first('checklist_name')); ?></span>
				<?php endif; ?>
			</div>

		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group col-md-12">
				<label class="control-label">Company <span class="red">*</span></label>
				<select name="company[]" id="company" class="select2_category form-control" data-placeholder="Choose Companies" tabindex="1" multiple>
					<option value="" disabled>Select Atleast One</option>
					
					<?php $__currentLoopData = $obj_companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
						<option value="<?php echo e($company->id); ?>" <?php if(in_array($company->id, $checklist_comps)): ?> selected <?php endif; ?> ><?php echo e($company->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
					
				</select>
				<?php if($errors->has('company')): ?>
				    <span class=error><?php echo e($errors->first('company')); ?></span>
				<?php endif; ?>
			</div>

			<div class="form-group col-md-12">
				<!-- <div class="form-actions"> -->
					<a href="javascript:;"><button type="reset" class="btn default" onClick="cancel_checklist();"><i class="fa fa-close"></i> Cancel</button></a>
					<button type="submit" class="btn green"><i class="fa fa-check"></i> Save</button>
				<!-- </div> -->
			</div>
		</div>
		<!--/span-->
	</div>
	

</div>


