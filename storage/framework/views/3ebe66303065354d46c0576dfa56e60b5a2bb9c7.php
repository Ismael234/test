<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<!--<link href="<?php echo e(url('/')); ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>-->
<link href="<?php echo e(url('/')); ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<!-- <link href="<?php echo e(url('/')); ?>/assets/plugins/select2/select2.css" rel="stylesheet" type="text/css"/> -->
<link href="<?php echo e(url('/')); ?>/assets/css/login3.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo e(url('/')); ?>/assets/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<!-- <link href="<?php echo e(url('/')); ?>/assets/css/layout.css" rel="stylesheet" type="text/css"/> -->
<!-- <link href="<?php echo e(url('/')); ?>/assets/css/custom.css" rel="stylesheet" type="text/css"/> -->
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>