<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title> Wiseworking | Superadmin</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/js/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/css/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo e(url('/')); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo e(url('/')); ?>/assets/css/custom.css" rel="stylesheet" type="text/css"/>



<!-- END GLOBAL MANDATORY STYLES -->
<link rel="shortcut icon" href="<?php echo e(url('/')); ?>/assets/img/favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid">
<!-- BEGIN HEADER -->
<?php  $admin = \Session::get('adminData');  ?>
<div class="page-header -i navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="<?php echo e(url('/')); ?>">
			<img src="<?php echo e(url('/')); ?>/assets/img/logo.png" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<?php if(!empty($admin['logo_image'])): ?>
						<img class="img-circle" src="<?php echo e(url('/')); ?>/uploads/user/thumbnail_29x29/<?php echo e($admin['logo_image']); ?>" alt="your image" />
					<?php else: ?>
						<img alt="default" class="img-circle" src="<?php echo e(url('/')); ?>/assets/img/avatar.png"/>
					<?php endif; ?>
					<span class="username username-hide-on-mobile">
					<?php if(!empty($admin['fullname'])): ?>
						<?php echo e($admin['fullname']); ?>

					<?php else: ?>
						Superadmin
					<?php endif; ?>
					</span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="<?php echo e(url('/superadmin/profile')); ?>">
							<i class="icon-user"></i> My Profile </a>
						</li>						
						<li>
							<a href="<?php echo e(url('/logout')); ?>" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
							<i class="icon-key"></i> Log Out </a>
							<form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
                                <?php echo e(csrf_field()); ?>

                            </form>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<div class="page-container">