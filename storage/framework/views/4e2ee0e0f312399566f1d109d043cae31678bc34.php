<div class="form-group">
	<label class="control-label">Role Name <span class="red">*</span></label>
	<?php if(!empty($role->name)): ?>
		<?php echo e(Form::text('name',$role->name,array('class'=>'form-control','id'=>'name','ng-model'=>'name','required'=>'required'))); ?>		
	<?php else: ?>
		<?php echo e(Form::text('name','',array('class'=>'form-control','id'=>'name','ng-model'=>'name','required'=>'required'))); ?>		
	<?php endif; ?>
</div>
  <div id="treeview-checkbox-demo">							            
        <ul>
        	<li data-value="selected">Permission:-
        		<ul>
	            <?php $__currentLoopData = $data_tree; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $tree): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	            	<li  data-value="<?php echo e($key); ?>">
	            		<?php echo e($tree['parent']); ?>

	            		<?php if(!empty($tree['child'])): ?>
	            		<ul>
	            			<?php $__currentLoopData = $tree['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $tree_child): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	            				<li data-value="<?php echo e($key); ?>"><?php echo e($tree_child); ?></li>
	            			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
	            		</ul>
	            		<?php endif; ?>
	            	</li>
	            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        	</ul>
        	</li>
        </ul>
    </div>
    