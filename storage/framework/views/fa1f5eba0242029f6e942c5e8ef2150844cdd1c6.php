<?php $__env->startSection('content'); ?>
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper">
		<div class="page-content">
			
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

			<h3 class="page-title">
			<!--Add User --> <!--<small>reports & statistics</small>-->
			</h3>
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue-hoki">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Quality Checklist
							</div>
							<div class="tools"></div>
						</div>
						<div class="portlet-body">
							<?php if(Session::has('flash_alert_notice')): ?>
		                        <div class="alert alert-success alert-dismissable">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           	<?php echo e(Session::get('flash_alert_notice')); ?> 
		                        </div>
	                       	<?php endif; ?>
							
							<div class="table-toolbar">
								<div class="edit_btn">
									<div class="row">
										<div class="col-md-12 text-right">
											<div class="row">
												<div class="col-md-12">
													<a class="btn green" id="" href="javascript:;" onclick="show_checklist_form();"> <i class="fa fa-plus"></i> Add Checklist </a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<!-- BEGIN FORM-->
										<form action="<?php echo e(url('superadmin/save_quality_checklist')); ?>" id="checklist_form" class="horizontal-form" method="post" style="display:none;">
											
											<div class="portlet light">
												<div class="portlet-title">
													<div class="caption">
														<i class="icon-equalizer font-blue-hoki"></i>
														<span class="caption-subject font-blue-hoki bold uppercase">Add Checklist</span>
														<span class="caption-helper"></span>
													</div>
												</div>

												<?php echo e(csrf_field()); ?>

												<?php echo $__env->make('superadmin::quality_checklist.quality_checklist_form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
											</div>
											

										</form>
										<!-- END FORM-->
									</div>
								</div>

							</div>
							<div class="row">
							<div class="col-md-12">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th> Name </th>
										<th> Action </th>
										
									</tr>
								</thead>														

							</table>
							</div></div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>

		</div>
	</div>
	
<!-- END CONTAINER -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>