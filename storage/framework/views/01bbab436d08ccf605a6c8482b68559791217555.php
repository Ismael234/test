<?php $__env->startSection('content'); ?>
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper aaaaaaq">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>-->	</h3>
			<!-- END PAGE HEADER-->

			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Add User</span>
								<span class="caption-helper"></span>
							</div>

							<div class="tools"></div>
						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="<?php echo e(url('superadmin/save_user')); ?>" id="add_user_form" class="horizontal-form" method="post" enctype="multipart/form-data">
								<?php echo e(csrf_field()); ?>

								<?php echo $__env->make('superadmin::user.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
			
		</div>
	</div>

<!-- END CONTAINER -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>