<!-- BEGIN CONTAINER -->
	
<?php $__env->startSection('content'); ?>
	
	
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->			
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>-->	</h3>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Add Role</span>
								<span class="caption-helper"></span>
							</div>
						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<?php echo e(Form::open(array('url' => '','name'=>"role" ,'id'=>"role" ,'method' => 'get'))); ?>

							<div class="row">								
							    <div class="col-md-6">							    
							    <?php echo $__env->make('superadmin::role.form1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
							    <button type="submit" class="btn blue" id="show-values">Save Permission</button>
							        <!-- <pre id="values"></pre> -->
							   </div>
  							</div>
  							<?php echo e(Form::close()); ?>

							<!-- END FORM-->
						</div>
					</div>	
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
			
		</div>
	</div>
<?php $__env->stopSection(); ?>
<!-- END CONTAINER -->
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>