 <div class="modal fade" id="adminModelFrm" close="cancel()">
	 <div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="adminInfo" name="adminInfo" class="horizontal-form" ng-submit="submitForm(<?php echo e($admin->id); ?>,'profile')" novalidate>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Superadmin Details</h4>
				</div>
				<div class="modal-body">
					<?php echo e(csrf_field()); ?>

					<input type="hidden" id="user_id" name="user_id" value="<?php if(isset($admin->id)): ?><?php echo e($admin->id); ?><?php endif; ?>" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Username <span class="red">*</span></label>
									<input type="text" id="username" class="form-control" name="username" placeholder="" ng-model="admin.username" ng-required="true"/>
									<span class="error" ng-show="adminInfo.username.$invalid && adminInfo.username.$touched">username is required</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Full Name <span class="red">*</span></label>
									<input type="text" id="fullname" name="fullname" class="form-control" placeholder="" ng-model="admin.fullname" ng-required="true"/>
									<span class="error" ng-show="adminInfo.fullname.$invalid && adminInfo.fullname.$touched">fullname is required</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Email id <span class="red">*</span></label>
									<input type="email" id="email" name="email" class="form-control" placeholder="" ng-model="admin.email" ng-required="true"/>
									<span class="error" ng-show="adminInfo.email.$dirty && adminInfo.email.$invalid">
								        <span ng-show="adminInfo.email.$error.required">Email is required.</span>
								        <span ng-show="adminInfo.email.$error.email">Invalid email address.</span>
								    </span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Password <span class="red">*</span></label>
									<input type="text" id="password" name="password" class="form-control" placeholder="" ng-model="admin.password" ng-required="true"/>
									<span class="error" ng-show="adminInfo.password.$invalid && adminInfo.password.$touched">password is required</span>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Mobile <span class="red">*</span></label>
									<input type="text" id="phone" name="phone" class="form-control" placeholder="" ng-model="admin.phone" ng-required="true"/>
									<span class="error" ng-show="adminInfo.phone.$invalid && adminInfo.phone.$touched">phone is required</span>
								</div>
							</div>
						</div> 
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn btn-primary" id="admin_profile_form">Update</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
