
<div class="form-body">
	<!--<h3 class="form-section">Person Info</h3>-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Username <span class="red">*</span></label>
				<input type="text" id="username" class="form-control" name="username" placeholder="" value="<?php if(isset($user->name)): ?><?php echo e($user->name); ?><?php endif; ?>" />
				<?php if($errors->has('username')): ?>
				    <span class=error><?php echo e($errors->first('username')); ?></span>
				<?php endif; ?>
				<!--<span class="help-block">
				This is inline help </span>-->
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Password <span class="red">*</span></label>
				<input type="text" id="password" name="password" class="form-control" placeholder=""  value="<?php if(isset($user->plain_password)): ?><?php echo e($user->plain_password); ?><?php endif; ?>">
				<?php if($errors->has('password')): ?>
				    <span class=error><?php echo e($errors->first('password')); ?></span>
				<?php endif; ?>
				<!--<span class="help-block">
				This field has error. </span>-->
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Confirm Password </label>
				<input type="text" id="CPassword" name="CPassword" class="form-control" placeholder="" value="<?php if(isset($user->plain_password)): ?><?php echo e($user->plain_password); ?><?php endif; ?>">
				<?php if($errors->has('CPassword')): ?>
				    <span class=error><?php echo e($errors->first('CPassword')); ?></span>
				<?php endif; ?>
				<!--<span class="help-block">
				Select your gender </span>-->
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Full Name <span class="red">*</span></label>
				<input type="text" id="fullname" name="fullname" class="form-control" placeholder="" value="<?php if(isset($user->full_name)): ?><?php echo e($user->full_name); ?><?php endif; ?>">
				<?php if($errors->has('fullname')): ?>
				    <span class=error><?php echo e($errors->first('fullname')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Company <span class="red">*</span></label>
				<?php  $uc = array($uc)  ?>
				<select name="company[]" id="company" class="select2_category form-control" data-placeholder="Choose Companies" tabindex="1" multiple>
					<option value="">Select Atleast One</option>
					<?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
						<option value="<?php echo e($company->id); ?>" "<?php if(!empty($company->id) && in_array($company->id, $uc)): ?> selected <?php endif; ?>" ><?php echo e($company->name); ?></option>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
				</select>
				<?php if($errors->has('company')): ?>
				    <span class=error><?php echo e($errors->first('company')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Email id <span class="red">*</span></label>
				<input type="text" id="email" name="email" class="form-control" placeholder="" value="<?php if(isset($user->email)): ?><?php echo e($user->email); ?><?php endif; ?>">
				<?php if($errors->has('email')): ?>
				    <span class=error><?php echo e($errors->first('email')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Mobile <span class="red">*</span></label>
				<input type="text" id="phone" name="phone" class="form-control" placeholder="" value="<?php if(isset($user->phone)): ?><?php echo e($user->phone); ?><?php endif; ?>" />
				<?php if($errors->has('phone')): ?>
				    <span class=error><?php echo e($errors->first('phone')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">User Type <span class="red">*</span></label>

				<?php if(!empty($user->role_id)): ?>						
					<?php echo e(Form::select('usertype', (object)$roles, $user->role_id,  ['class' => 'select2_category form-control','id'=>'usertype'])); ?>

				<?php else: ?>
					<?php echo e(Form::select('usertype', $roles, null,  ['class' => 'select2_category form-control','id'=>'usertype'])); ?>

				<?php endif; ?>
				<?php if($errors->has('user_type')): ?>
				    <span class=error><?php echo e($errors->first('user_type')); ?></span>
				<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->

	<hr>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label"><u><i><strong>Business Details:</strong><i></u></label>
			</div>
		</div>
	</div>
	<!--/row for buisness-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Address 1</label>
			<input type="text" id="bus_add1" name="bus_add1" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->busi_address1); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Address 2</label>
			<input type="text" id="bus_add2" name="bus_add2" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->busi_address2); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
	</div>

	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Suburb</label>
			<input type="text" id="bus_suburb" name="bus_suburb" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->busi_suburb); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business State</label>
			<?php if(!empty($udetail[0])): ?>						
				<?php echo e(Form::select('bus_state', $option_state, $udetail[0]->busi_state,  ['class' => 'select2_category form-control','id'=>'bus_state'])); ?>

			<?php else: ?>
				<?php echo e(Form::select('bus_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'bus_state'])); ?>

			<?php endif; ?>
			
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Post</label>
			<input type="text" id="bus_post" name="bus_post" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->busi_post); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Business Country</label>
			<input type="text" id="bus_country" name="bus_country" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->busi_country); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label"><input name="chck1" id="chck1" class="chck1" type="checkbox"> Business Address is same as billing address.</label>
			</div>
		</div>
	</div>

	<!--/row for billing-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Address 1</label>
			<input type="text" id="buld_add1" name="buld_add1" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->bill_address1); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Address 2</label>
			<input type="text" id="buld_add2" name="buld_add2" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->bill_address2); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Suburb</label>
			<input type="text" id="buld_suburb" name="buld_suburb" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->bill_suburb); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing State</label>
			<?php if(!empty($udetail[0])): ?>
				<?php echo e(Form::select('buld_state', $option_state, $udetail[0]->bill_state,  ['class' => 'select2_category form-control','id'=>'buld_state'])); ?>

			<?php else: ?>
				<?php echo e(Form::select('buld_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'buld_state'])); ?>

			<?php endif; ?>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Post</label>
			<input type="text" id="buld_post" name="buld_post" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->bill_post); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
			<label class="control-label">Billing Country</label>
			<input type="text" id="buld_country" name="buld_country" class="form-control" placeholder="" value="<?php if(!empty($udetail[0])): ?><?php echo e($udetail[0]->bill_country); ?><?php endif; ?>">
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<?php /*
	<div class="row">
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="btn btn-default btn-file">Photo
				<input type="file" id="user_logo" name="user_logo" class=""></label>
				@if(!empty($udetail[0]))
					@if(!empty($udetail[0]->logo_image))
						<?php $logo_image = $udetail[0]->logo_image; ?>
					@endif
					<img class="logo_img" src="{{url('/')}}/uploads/user/{{$logo_image}}">
				@endif
				    	
				    
			</div>
		</div>
	</div>
	*/ ?>
		

</div>


<div class="form-actions">
	<button type="button" class="btn btn-default" onClick="history.go(-1); return false;"> Cancel </button>
	<button type="submit" class="btn blue pull-right"><i class="fa fa-check"></i> Submit</button>
</div>
