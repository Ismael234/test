<?php $__env->startSection('content'); ?>
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper" ng-app="adminModule">
		<div class="page-content" ng-controller="superadminController">
			<!-- BEGIN PAGE HEADER-->
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<!-- <?php echo $breadcrumbs; ?> -->
			<h3 class="page-title">	 </h3>
			<!-- END PAGE HEADER-->

			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Superadmin Details
						</div>
						
					</div>
					<div class="portlet-body form">
						<?php if(isset($_REQUEST['i']) && !empty($_REQUEST['i']) && empty($j)): ?>							 	
		                        <div class="alert alert-success alert-dismissable">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           		<?php  echo $_REQUEST['i'];  ?>;
		                        </div>
                       	<?php endif; ?>
						<div class="edit_btn">
							<div class="row">
								<div class="col-md-12 text-right">
									<div class="row">
										<div class="col-md-12">
											<button class="btn green" ng-click="showModal(<?php echo e($admin->id); ?>)"><i class="fa fa-pencil"></i> Update Profile</button>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
						<!-- Admin Profile Dialog -->
						<?php echo $__env->make('superadmin::user.forms.admin-profile', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						<!-- BEGIN FORM-->
						<div class="form-horizontal" role="form">
							<div class="form-body">									
								<h3 class="form-section">Superadmin Info</h3>
								<div class="row">
									<div class="col-md-6 text-center">
										<form id="adminLogoFrm" runat="server">
										<input type="hidden" name="username" value="<?php if(isset($admin->name)): ?><?php echo e($admin->name); ?><?php endif; ?>" />
											<div class="form-group">
												<span class="btn green fileinput-button">
								                    <i class="glyphicon glyphicon-plus"></i>

								                    <?php if(!empty($udetail[0]) && !empty($udetail[0]->logo_image)): ?>
								                    	<span>Change Photo</span>
								                    <?php else: ?>
								                    	<span>Add Photo</span>
								                    <?php endif; ?>
													 	<input type='file' name="logo" id="userImg"/>
												</span><br><br>
												<?php if(!empty($udetail[0]) && !empty($udetail[0]->logo_image)): ?>
													<img id="logo_image"  width="200" height="200"  src="<?php echo e(url('/')); ?>/uploads/user/<?php echo e($udetail[0]->logo_image); ?>" alt="your image" />
												<?php else: ?>
													<img id="logo_image"  width="200" height="200"  src="<?php echo e(url('/')); ?>/uploads/user_icon.jpg" alt="user image" />
												<?php endif; ?>
											</div>	
											<p class="form-control-static">
												<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success start" ng-click="submitForm(<?php echo e($admin->id); ?>,'logo')">
								                    <i class="glyphicon glyphicon-upload"></i>
								                    <span>Start upload</span>
								                </button>
								                <span ng-show="adminLoader">
								                	<img src="<?php echo e(url('/')); ?>/assets/img/ajax-loader.gif">
								                </span>
											</p>
										</form>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Username :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 <?php echo e($admin->name); ?>

													</p>
												</div>
											</div>
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Password :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 <?php echo e($admin->plain_password); ?>

													</p>
												</div>
											</div>
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Name :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 <?php echo e($admin->full_name); ?>

													</p>
												</div>
											</div>
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Email :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 <?php echo e($admin->email); ?>

													</p>
												</div>
											</div>
											<div class="col-md-12">
												<label class="control-label col-md-3"><strong>Mobile :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														<?php echo e($admin->phone); ?>

													</p>
												</div>
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
							</div>							
						</div>
					</div>
				</div>																
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
<!-- END CONTAINER -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>