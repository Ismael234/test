<?php $__env->startSection('content'); ?>
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper">
		<div class="page-content">
			
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

			<h3 class="page-title">
			<!--Add User --> <!--<small>reports & statistics</small>-->
			</h3>
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue-hoki">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Role List
							</div>
							<div class="tools"></div>
						</div>
						<div class="portlet-body">
							<?php if(Session::has('flash_alert_notice')): ?>
		                        <div class="alert alert-success alert-dismissable">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           	<?php echo e(Session::get('flash_alert_notice')); ?> 
		                        </div>
	                       	<?php endif; ?>
							<?php if(!empty($_REQUEST['i']) && $_REQUEST['i']==1 && empty($j)): ?>							 	
		                        <div class="alert alert-success alert-dismissable">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           		Role has been updated successfuly.
		                        </div>
	                       	<?php endif; ?>
							<div class="table-toolbar">
								<div class="row">
									
									<div class="col-md-12 text-right">
										<div class="btn-group">
											<a class="btn green" id="" href="<?php echo e(url('/')); ?>/superadmin/add-role"> <i class="fa fa-plus"></i> Add New </a>
										</div>
									</div>
								</div>
							</div>
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th> Role </th>										
										<th> Action </th>
									</tr>
								</thead>														
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>

		</div>
	</div>
	
<!-- END CONTAINER -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>