<!-- BEGIN CONTAINER -->
	
<?php $__env->startSection('content'); ?>
	
	
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->			
			<?php echo $__env->make('superadmin::partials.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<h3 class="page-title">	<!--Add User --> <!--<small>reports & statistics</small>-->	</h3>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Add Company</span>
								<span class="caption-helper"></span>
							</div>
						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->


							<form action="<?php echo e(url('superadmin/save_company')); ?>" method="post" class="horizontal-form" id="cmp_section" autocomplete="on|off" enctype="multipart/form-data" accept-charset="utf-8">


								<?php echo e(csrf_field()); ?>

								<?php echo $__env->make('superadmin::company.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
							</form>
							<!-- END FORM-->
						</div>
					</div>	
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
			
		</div>
	</div>
<?php $__env->stopSection(); ?>
<!-- END CONTAINER -->
<?php echo $__env->make('superadmin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>