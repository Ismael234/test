<?php if(Session::get('projectConfig') != 0): ?>
	<?php echo $__env->make('superadmin::layouts.project_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php echo $__env->make('superadmin::layouts.project_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>	
<?php else: ?>
	<?php echo $__env->make('superadmin::layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php echo $__env->make('superadmin::layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>	
<?php endif; ?>
<?php echo $__env->yieldContent('content'); ?>
<?php echo $__env->make('superadmin::layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>