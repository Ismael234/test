<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUserRole extends Model
{
    //
    protected $table = 'project_user_role';
    protected $fillable = ['project_id', 'user_id'];
    public $timestamps = false;
}
