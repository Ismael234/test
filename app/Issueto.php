<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issueto extends Model
{
    //
    protected $table = 'issueTo';
    protected $fillable = ['id','name','description'];
    public $timestamps = false;

}
