<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table = 'role';
    protected $fillable = ['name','created_at','created_by'];
    public $timestamps = false;    

    public function roleAuthDetails()
    {
        return $this->hasMany('App\RoleAuthMap');        
    }
}