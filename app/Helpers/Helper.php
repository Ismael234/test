<?php

namespace App\Helpers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;
use Auth;
use Config;
use View;
use Input;
use session;
use Crypt;
use Hash;
use Menu;
use Html;
use App\User;
use App\Role;
use App\Company;
use App\CompanyUserMapping;
use App\CompanyIssuetoMapping;
use App\CompanyProjectMapping;
use App\CompanyChecklistMapping;

 

class Helper {

    /**
     * function used to check stock in kit
     *
     * @param = null
     */
   
    public static function test(){
        
        return "Hey I am a Helper";
    }

    public static function getUserRoles(){
    	$data = array();
    	$roles = Role::where('is_deleted', '=', 0)->where('id', '!=', 0)->select('id','name')->get();
    	foreach($roles as $role){
    		$data[$role->id] = $role->name;
    	}
        asort($data);
    	return $data;
    }

    public static function getCompanyList(){
        $data = array();
        $comps = Company::where('is_deleted', '=', 0)->select('id','name')->get();
        foreach($comps as $comp){
            $data[$comp->id] = $comp->name;
        }
        asort($data);
        return $data;
    }

    public static function getCompanyUserMapping($user_id){
        $data = array();
        $usr_comps = CompanyUserMapping::select('company_id')->where('user_id', '=', $user_id)->where('is_deleted', '=', 0)->get();
            $data = array();
            foreach($usr_comps as $comp){
                $data[] = $comp->company_id;
            }
        asort($data);
        return $data;
    }

    public static function getCompanyIssuetoMapping($issueto_id){
        $data = array();
        $issueto_comps = CompanyIssuetoMapping::select('company_id')->where('issueto_id', '=', $issueto_id)->where('is_deleted', '=', 0)->get();
            $data = array();
            foreach($issueto_comps as $comp){
                $data[] = $comp->company_id;
            }
        asort($data);
        return $data;
    }

    public static function getCompanyChecklistMapping($checklist_id){
        $data = array();
        $checklist_comps = CompanyChecklistMapping::select('company_id')->where('checklist_id', '=', $checklist_id)->where('is_deleted', '=', 0)->get();
            $data = array();
            foreach($checklist_comps as $comp){
                $data[] = $comp->company_id;
            }
        asort($data);
        return $data;
    }

    public static function getCompanyProjectMapping($project_id){
        $data = array();
        $prj_comps = CompanyProjectMapping::select('company_id')->where('project_id', '=', $project_id)->where('is_deleted', '=', 0)->get();
            $data = array();
            foreach($prj_comps as $comp){
                $data[] = $comp->company_id;
            }
        asort($data);
        return $data;
    }


    public static function sendMessage($template,$data){
         Mail::send($template, $data, function($message) use ($data) {
             $message->to($data['email'], $data['name'])->subject
                ($data['subject']);
             $message->from('test@fxbytes.com','GendefId');             
        });
        return "1";
    }

    public static function getCompanyUsers($company_id){
        $data = array();
        $comp_users = CompanyUserMapping::select('user_id')->where('company_id', '=', $company_id)->where('is_deleted', '=', 0)->get();
            $data = array();
            foreach($comp_users as $user){
                $data[] = $user->user_id;
            }
        asort($data);
        return $data;
    }

    public static function getCompanyProject($company_id){
        $data = array();
        $prj_comps = CompanyProjectMapping::select('project_id')->where('company_id', '=', $company_id)->where('is_deleted', '=', 0)->get();        
            $data = array();
            foreach($prj_comps as $pro){
                $data[] = $pro->project_id;
            }
        asort($data);
        return $data;
    }

    public static function getLoginUserDetails(){
        $data = array();
        $id = Auth::user()->id;
        $admin = User::find($id);
        $userdetail = $admin-> UserDetails;
        $adminData = array();
        $adminData['username'] = $admin->name;
        $adminData['fullname'] = $admin->full_name;
        $adminData['email'] = $admin->email; 
        $adminData['phone'] = $admin->phone;
        $adminData['logo_image'] = !empty($userdetail[0])?$userdetail[0]->logo_image:'';
        //print_r($adminData); die;
        \Session::set('adminData', $adminData);
    }
    
    public static function generatePageTree($datas, $parent = 0, $depth=0){
        if($depth > 100) return ''; // Make sure not to have an endless recursion
        $tree = '<ul class="fa-ul">';
        for($i=0, $ni=count($datas); $i < $ni; $i++){
            if($datas[$i]['parent_id'] == $parent){
                $tree .= '<li data-value="'.$datas[$i]['id'].'"><i class="fa fa-plus ckeckedT" data-id="'.$datas[$i]['id'].'">&nbsp;&nbsp;</i>';
                $tree .= $datas[$i]['name'];
                $tree .= Helper::generatePageTree($datas, $datas[$i]['id'], $depth+1);
                $tree .= '</li>';
            }
        }
        $tree .= '</ul>';
        return $tree;
    }    
}