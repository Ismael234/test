<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QualityChecklist extends Model
{
    //
    protected $table = 'quality_checklist';
    protected $fillable = ['id','name'];
    public $timestamps = false;

}
