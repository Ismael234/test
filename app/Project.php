<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $table = 'project';
    protected $fillable = ['name', 'code', 'type','description', 'address1', 'suburb', 'postal_code ','state', 'country',];
    public $timestamps = false;

    public function projectCompany()
    {
        return $this->hasMany('App\CompanyProjectMapping');        
    }

}
