<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = 'company';
    protected $fillable = ['name','code','registration_number','type','email','phone','created_at'];
    public $timestamps = false;

 	public function CompanyDetails()
    {
        return $this->hasMany('App\CompanyDetails');        
    }
}
