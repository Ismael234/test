<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleAuthMap extends Model
{
    //
    protected $table = 'role_authentication_mapping';
    protected $fillable = ['role_id','authorisation_id','status'];
    public $timestamps = false;
        
}
