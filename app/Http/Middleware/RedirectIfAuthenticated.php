<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Helpers as Helper;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {                
        // if(Auth::Check())
        // {
        //     if(Auth::User()->role_id == 0)
        //     {
        //         return redirect('/superadmin');
        //     }
        //     else{
        //         return redirect('/');   
        //     }            
        // }    
        if (Auth::guard($guard)->check()) {
            if(Auth::User()->role_id == 0)
            {
                $admin = Helper::getLoginUserDetails();
                return redirect('/superadmin');
            }
            else{
                return redirect('/');   
            }      
        }        
        return $next($request);
    }
}
