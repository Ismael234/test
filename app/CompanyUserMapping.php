<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyUserMapping extends Model
{
    //
    protected $table = 'company_user_mapping';
    protected $fillable = ['user_id'];
    public $timestamps = false;
}
